<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOlympiadZonesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('olympiad_zones', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('min_id')->unsigned()->nullable();
            $table->integer('max_id')->unsigned()->nullable();
            $table->integer('zone_type_id')->unsigned()->nullable();
            $table->integer('gender_id')->unsigned()->nullable();
            $table->integer('participant_limit');
            $table->dateTime('start_date');
            $table->string('problem_pdf')->nullable();
            $table->string('solution_pdf')->nullable();
            $table->timestamps();
            $table->integer('user_id')->unsigned();
            $table->integer('edited_user_id')->unsigned()->nullable();
            $table->integer('olympiad_id')->unsigned();
            $table->integer('olympiad_detail_id')->unsigned();
            $table->integer('status_id')->unsigned();

            $table->foreign('min_id')->references('id')->on('zones');
            $table->foreign('max_id')->references('id')->on('zones');
            $table->foreign('zone_type_id')->references('id')->on('zone_types');
            $table->foreign('gender_id')->references('id')->on('zones');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('edited_user_id')->references('id')->on('users');
            $table->foreign('olympiad_id')->references('id')->on('olympiads')->onDelete('cascade');
            $table->foreign('olympiad_detail_id')->references('id')->on('olympiad_details')->onDelete('cascade');
            $table->foreign('status_id')->references('id')->on('zone_status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('olympiad_zones');
    }
}
