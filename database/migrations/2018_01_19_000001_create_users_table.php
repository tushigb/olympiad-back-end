<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

//Хэрэглэгч
class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('register_number')->unique();
            $table->date('dob')->nullable();
            $table->boolean('sex')->nullable();
            $table->string('first_name');
            $table->string('last_name')->nullable();
            $table->string('email')->unique();
            $table->string('mobile_phone');
            $table->string('home_phone')->nullable();
            $table->string('hurry_phone');
            $table->string('student_code')->nullable();
            $table->string('image_path')->unique()->nullable();
            $table->string('profession')->nullable();
            $table->text('description')->nullable();
            $table->string('web_site')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
            $table->integer('role_id')->unsigned();
            $table->integer('ad_city_province_id')->unsigned();
            $table->integer('ad_sum_district_id')->unsigned();
            $table->string('ad_team_section');
            $table->string('ad_apart_room')->nullable();
            $table->integer('school_id')->unsigned()->nullable();
            $table->integer('class')->nullable();
            $table->char('class_group')->nullable();
            $table->boolean('is_verified');

            $table->foreign('role_id')->references('id')->on('roles');
            $table->foreign('ad_city_province_id')->references('id')->on('city_provinces');
            $table->foreign('ad_sum_district_id')->references('id')->on('sum_districts');
            $table->foreign('school_id')->references('id')->on('schools');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
