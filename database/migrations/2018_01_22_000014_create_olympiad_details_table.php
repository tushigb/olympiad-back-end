<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOlympiadDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('olympiad_details', function (Blueprint $table) {
            $table->increments('id');
            $table->dateTime('start_date');
            $table->dateTime('end_date');
            $table->dateTime('register_end_date');
            $table->timestamps();
            $table->integer('user_id')->unsigned();
            $table->integer('edited_user_id')->unsigned()->nullable();
            $table->integer('olympiad_id')->unsigned();
            $table->integer('olympiad_scope_id')->unsigned();
            $table->integer('lesson_id')->unsigned();
            $table->double('mandate_cost');
            $table->boolean('show_mobile');

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('edited_user_id')->references('id')->on('users');
            $table->foreign('olympiad_id')->references('id')->on('olympiads')->onDelete('cascade');
            $table->foreign('olympiad_scope_id')->references('id')->on('olympiad_scopes');
            $table->foreign('lesson_id')->references('id')->on('lessons');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('olympiad_details');
    }
}
