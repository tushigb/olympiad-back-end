<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOlympiadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('olympiads', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->text('description');
            $table->text('goal');
            $table->text('address');
            $table->text('reward');
            $table->text('requirement');
            $table->string('logo')->nullable();
            $table->string('cover')->nullable();
            $table->string('guidance')->nullable();
            $table->timestamps();
            $table->integer('user_id')->unsigned();
            $table->integer('edited_user_id')->unsigned()->nullable();
            $table->integer('status_id')->unsigned();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('edited_user_id')->references('id')->on('users');
            $table->foreign('status_id')->references('id')->on('olympiad_status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('olympiads');
    }
}
