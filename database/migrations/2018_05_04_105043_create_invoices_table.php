<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->increments('id');
            $table->text('invoice_id');
//            $table->string('merchant_invoice_number');
            $table->integer('user_id')->unsigned();
            $table->integer('mandate_id')->unsigned()->nullable();
            $table->integer('file_id')->unsigned()->nullable();
            $table->text('invoice_json');
            $table->text('payment_json')->nullable();
            $table->boolean('is_paid');
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('mandate_id')->references('id')->on('mandates');
            $table->foreign('file_id')->references('id')->on('files');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
