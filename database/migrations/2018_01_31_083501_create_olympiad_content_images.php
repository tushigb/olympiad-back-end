<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOlympiadContentImages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('olympiad_content_images', function (Blueprint $table) {
            $table->increments('id');
            $table->string('path');
            $table->boolean('is_guidance')->nullable();
            $table->integer('olympiad_id')->unsigned()->nullable();
            $table->integer('olympiad_detail_id')->unsigned()->nullable();
            $table->integer('content_id')->unsigned()->nullable();
            $table->timestamps();

            $table->foreign('olympiad_id')->references('id')->on('olympiads')->onDelete('cascade');
            $table->foreign('olympiad_detail_id')->references('id')->on('olympiad_details')->onDelete('cascade');
            $table->foreign('content_id')->references('id')->on('contents')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('olympiad_content_images');
    }
}
