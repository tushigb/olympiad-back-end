<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

//Олимпиад_Хүрээ
class CreateOlympiadScopesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('olympiad_scopes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
//            $table->integer('user_id')->unsigned();
//            $table->integer('edited_user_id')->unsigned();
//
//            $table->foreign('user_id')->references('id')->on('users');
//            $table->foreign('edited_user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('olympiad_scopes');
    }
}
