<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProblemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('problems', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('max_score');
            $table->timestamps();

            $table->integer('user_id')->unsigned();
            $table->integer('edited_user_id')->unsigned()->nullable();
            $table->integer('olympiad_zone_id')->unsigned();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('edited_user_id')->references('id')->on('users');
            $table->foreign('olympiad_zone_id')->references('id')->on('olympiad_zones')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('problems');
    }
}
