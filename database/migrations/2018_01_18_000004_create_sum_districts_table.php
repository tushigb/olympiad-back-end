<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

//Сум_Дүүрэг
class CreateSumDistrictsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sum_districts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('code');
            $table->string('name');
            $table->timestamps();
            $table->integer('city_province_id')->unsigned();
//            $table->integer('user_id')->unsigned();
//            $table->integer('edited_user_id')->unsigned();

            $table->foreign('city_province_id')->references('id')->on('city_provinces');
//            $table->foreign('user_id')->references('id')->on('users');
//            $table->foreign('edited_user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sum_districts');
    }
}
