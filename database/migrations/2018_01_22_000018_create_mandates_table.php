<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMandatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mandates', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('is_came');
            $table->boolean('is_qr');
            $table->string('register_number');
            $table->date('dob');
            $table->boolean('sex');
            $table->string('email');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('mobile_phone');
            $table->string('home_phone')->nullable();
            $table->string('hurry_phone');
            $table->string('pupil_class')->nullable();
            $table->string('student_code')->nullable();
            $table->string('profession')->nullable();
            $table->string('image_path')->nullable();
            $table->boolean('is_paid');
            $table->integer('payment');
            $table->timestamps();
            $table->integer('role_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->integer('ad_city_province_id')->unsigned();
            $table->integer('ad_sum_district_id')->unsigned();
            $table->string('ad_team_section');
            $table->string('ad_apart_room');
            $table->integer('school_id')->unsigned()->nullable();
            $table->integer('olympiad_id')->unsigned();
            $table->integer('olympiad_detail_id')->unsigned();
            $table->integer('olympiad_zone_id')->unsigned();
            $table->integer('class');
            $table->char('class_group');

            $table->foreign('role_id')->references('id')->on('roles');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('ad_city_province_id')->references('id')->on('city_provinces');
            $table->foreign('ad_sum_district_id')->references('id')->on('sum_districts');
            $table->foreign('school_id')->references('id')->on('schools');
            $table->foreign('olympiad_id')->references('id')->on('olympiads');
            $table->foreign('olympiad_detail_id')->references('id')->on('olympiad_details');
            $table->foreign('olympiad_zone_id')->references('id')->on('olympiad_zones');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mandates');
    }
}