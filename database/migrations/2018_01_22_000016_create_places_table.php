<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlacesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('places', function (Blueprint $table) {
            $table->increments('id');
            $table->string('place');
            $table->integer('participant_number');
            $table->timestamps();

            $table->integer('user_id')->unsigned();
            $table->integer('edited_user_id')->unsigned()->nullable();
            $table->integer('olympiad_id')->unsigned();
            $table->integer('olympiad_detail_id')->unsigned();
            $table->integer('olympiad_zone_id')->unsigned();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('edited_user_id')->references('id')->on('users');
            $table->foreign('olympiad_id')->references('id')->on('olympiads');
            $table->foreign('olympiad_detail_id')->references('id')->on('olympiad_details');
            $table->foreign('olympiad_zone_id')->references('id')->on('olympiad_zones');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('places');
    }
}
