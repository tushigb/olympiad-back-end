<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMarksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('marks', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('user_id')->unsigned();
            $table->integer('olympiad_id')->unsigned();
            $table->integer('olympiad_detail_id')->unsigned();
            $table->integer('olympiad_zone_id')->unsigned();
            $table->integer('mandate_id')->unsigned();
            $table->integer('problem_id')->unsigned();
            $table->integer('score');

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('olympiad_id')->references('id')->on('olympiads');
            $table->foreign('olympiad_detail_id')->references('id')->on('olympiad_details');
            $table->foreign('olympiad_zone_id')->references('id')->on('olympiad_zones');
            $table->foreign('mandate_id')->references('id')->on('mandates');
            $table->foreign('problem_id')->references('id')->on('problems');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('marks');
    }
}
