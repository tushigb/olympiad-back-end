<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                'register_number' => 'QW12345678',
                'first_name' => 'Tushig',
                'last_name' => 'Battumur',
                'email' => 'aaa@aaa.com',
                'image_path' => 'null',
                'password' => 'tushig',
                'role_id' => 1
            ],
            [
                'register_number' => 'AS87654321',
                'first_name' => 'Bold',
                'last_name' => 'Erdene',
                'email' => 'bbb@bbb.com',
                'image_path' => 'nulll',
                'password' => 'demo',
                'role_id' => 2
            ],
            [
                'register_number' => 'ZX12348765',
                'first_name' => 'Bat',
                'last_name' => 'Dorj',
                'email' => 'ccc@ccc.com',
                'image_path' => 'nullll',
                'password' => 'dorjbat',
                'role_id' => 3
            ],
//            [
//                'register_number' => 'ASDF123456',
//                'first_name' => 'tushig',
//                'last_name' => 'battumur',
//                'email' => 'tushig.0803@gmail.com',
//                'password' => 'helloworld',
//                'role_id' => 3
//            ],
        ];

        foreach ($users as $user) {
            DB::table('users')->insert([
                'register_number' => $user['register_number'],
                'first_name' => $user['first_name'],
                'last_name' => $user['last_name'],
                'email' => $user['email'],
                'image_path' => $user['image_path'],
                'password' => $user['password'],
                'role_id' => $user['role_id']
            ]);
        }
    }
}