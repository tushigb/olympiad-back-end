<?php

use Illuminate\Database\Seeder;

class ZoneStatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $statuses = [
            [
                'name' => 'Баталгаажсан'
            ],
            [
                'name' => 'Ирц бүртгэгдсэн'
            ],
            [
                'name' => 'Дүн бүртгэгдсэн'
            ]
        ];

        foreach ($statuses as $status) {
            DB::table('zone_status')->insert([
                'name' => $status['name']
            ]);
        }
    }
}
