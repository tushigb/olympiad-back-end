<?php

use Illuminate\Database\Seeder;

class OlympiadScopeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $olympiadScopes = [
            [
                'name' => 'Оюутан'
            ],
            [
                'name' => 'Багш'
            ],
            [
                'name' => 'Сурагч'
            ]
        ];

        foreach ($olympiadScopes as $olympiadScope) {
            DB::table('olympiad_scopes')->insert([
                'name' => $olympiadScope['name']
            ]);
        }
    }
}
