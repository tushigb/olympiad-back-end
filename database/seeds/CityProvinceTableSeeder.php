<?php

use Illuminate\Database\Seeder;

class CityProvinceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cityProvinces = DB::table('city_provinces')->get();
        foreach ($cityProvinces as $cityProvince) {
            $sumDistricts = DB::table('sum_districts')
                ->where('city_province_id', '=', $cityProvince->id)
                ->get();
            if ($cityProvince->name != 'Улаанбаатар') {
                foreach ($sumDistricts as $sumDistrict) {
                    for ($i = 0; $i < 15; $i++) {
                        DB::table('team_sections')->insert([
                            'name' => $i + 1 . '-р баг',
                            'city_province_id' => $cityProvince->id,
                            'sum_district_id' => $sumDistrict->id,
                        ]);
                    }
                }
            } else {
                foreach ($sumDistricts as $sumDistrict) {
                    for ($i = 0; $i < 30; $i++) {
                        DB::table('team_sections')->insert([
                            'name' => $i + 1 . '-р хороо',
                            'city_province_id' => $cityProvince->id,
                            'sum_district_id' => $sumDistrict->id,
                        ]);
                    }
                }
            }
        }
    }
}
