<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;


class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            [
                'name' => 'Админ'
            ],
            [
                'name' => 'Зохион байгуулагч'
            ],
            [
                'name' => 'Багш'
            ],
            [
                'name' => 'Оюутан'
            ],
            [
                'name' => 'Сурагч'
            ]
        ];

        foreach ($roles as $role) {
            DB::table('roles')->insert([
                'name' => $role['name']
            ]);
        }
    }
}