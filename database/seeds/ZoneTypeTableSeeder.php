<?php

use Illuminate\Database\Seeder;

class ZoneTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $zoneTypes = [
            [
                'name' => 'анги'
            ],
            [
                'name' => 'нас'
            ],
            [
                'name' => 'gender'
            ]
        ];

        foreach ($zoneTypes as $zoneType) {
            DB::table('zone_types')->insert([
                'name' => $zoneType['name'],
            ]);
        }
    }
}