<?php

use Illuminate\Database\Seeder;

class OlympiadStatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $statuses = [
            [
                'name' => 'Хүлээгдэж буй'
            ],
            [
                'name' => 'Баталгаажсан'
            ],
            [
                'name' => 'Дууссан'
            ],
            [
                'name' => 'Хойшилсон'
            ],
            [
                'name' => 'Цуцлагдсан'
            ]
        ];

        foreach ($statuses as $status) {
            DB::table('olympiad_status')->insert([
                'name' => $status['name']
            ]);
        }
    }
}
