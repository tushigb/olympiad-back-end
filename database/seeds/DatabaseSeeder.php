<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RoleTableSeeder::class);
        $this->call(LessonTableSeeder::class);
        $this->call(OlympiadScopeSeeder::class);
        $this->call(ZoneTypeTableSeeder::class);
        $this->call(ZoneTableSeeder::class);
        $this->call(SchoolTableSeeder::class);
        $this->call(OlympiadStatusTableSeeder::class);
        $this->call(ZoneStatusTableSeeder::class);
//        $this->call(CityProvinceTableSeeder::class);
    }
}