<?php

use Illuminate\Database\Seeder;

class LessonTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $lessons = [
            [
                'name' => 'Математик'
            ],
            [
                'name' => 'Монгол хэл'
            ],
            [
                'name' => 'Физик'
            ],
            [
                'name' => 'Хими'
            ],
            [
                'name' => 'Мэдээлэл зүй'
            ],
            [
                'name' => 'Түүх'
            ],
            [
                'name' => 'Биеийн тамир'
            ]
        ];

        foreach ($lessons as $lesson) {
            DB::table('lessons')->insert([
                'name' => $lesson['name']
            ]);
        }
    }
}