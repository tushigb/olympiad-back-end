<?php

use Illuminate\Database\Seeder;

class SchoolTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $schools = [
            [
                'name' => 'Найдвар'
            ],
            [
                'name' => 'Хөгжлийн гүүр'
            ],
            [
                'name' => 'Олонлог төв сургууль'
            ],
            [
                'name' => 'Технологийн ахлах сургууль'
            ],
            [
                'name' => 'Түшээ гүн ахлах сургууль'
            ],
            [
                'name' => 'Монгол эдийн засгийн ахлах сургууль'
            ],
            [
                'name' => 'Арвист хангай бүрэн дунд сургууль'
            ],
            [
                'name' => 'Шинэ зуун билэг бүрэн дунд сургууль'
            ],
        ];

        foreach ($schools as $school) {
            DB::table('schools')->insert([
                'name' => $school['name']
            ]);
        }
    }
}