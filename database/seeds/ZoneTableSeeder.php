<?php

use Illuminate\Database\Seeder;

class ZoneTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $genders = [
            [
                'name' => 'Хоёул',
                'zone_type_id' => 3
            ],
            [
                'name' => 'Эр',
                'zone_type_id' => 3
            ],
            [
                'name' => 'Эм',
                'zone_type_id' => 3
            ]
        ];

        for ($i = 1; $i <= 12; $i++) {
            DB::table('zones')->insert([
                'name' => $i,
                'zone_type_id' => 1
            ]);
        }

        for ($i = 6; $i <= 18; $i++) {
            DB::table('zones')->insert([
                'name' => $i,
                'zone_type_id' => 2
            ]);
        }

        DB::table('zones')->insert([
            'name' => '18+',
            'zone_type_id' => 2
        ]);

        foreach ($genders as $gender) {
            DB::table('zones')->insert([
                'name' => $gender['name'],
                'zone_type_id' => $gender['zone_type_id']
            ]);
        }
    }
}
