<?php
/*Angular-аар нэвтрэх үеийн Authentication*/

//Нэвтрэх
Route::post('login', 'Auth\LoginController@login')->name('login');

//Баталгаажуулах
Route::get('verify', 'Auth\LoginController@verify');

//Гарах
Route::get('logout', 'Auth\LoginController@logout');