<?php

//Бүх файл авах
Route::get('rest/file/all', 'FileController@getAll');

//Хэрэглэгчийн id-аас шалтгаалж файлууд авах
Route::get('rest/file', 'FileController@getAllByUserId');

//Хадгалах, шинэчлэх
Route::post('rest/file', 'FileController@save');

//Хуулсан файлаа харах - Auth
Route::get('rest/file-pdf/{id}', 'FileController@getFile');

//Төлбөрийн нөхцөл харах
Route::get('rest/file/payment/{file_id}', 'FileController@getPaymentMethod');

Route::any('{path?}', function () {
    return File::get('dist/index.html');
})->where("path", ".+");