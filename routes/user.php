<?php

Route::get('rest/user/all/{first}/{second}/{role_id}/{per_page}', 'UserController@getAll');

Route::get('rest/user/{id}/{value}', 'UserController@setStatus');

Route::post('rest/user', 'UserController@save');

Route::put('rest/user', 'UserController@save');

Route::put('rest/user/password', 'UserController@changePassword');

Route::delete('rest/user/delete/{id}', 'UserController@delete');

Route::get('rest/user/statistic', 'UserController@getSimpleStatistics');