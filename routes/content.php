<?php

// Бүх мэдээ авах
Route::get('rest/contents/{type}', 'ContentController@getAll');

// Хэрэглэгчийн id-аас шалтгаалж бүх мэдээ авах
Route::get('rest/user-contents/{type}', 'ContentController@getAllByUserId');

// Хэрэглэгчийн id-аас шалтгаалж мэдээ авах
Route::get('rest/content/{id}', 'ContentController@getOneByUserId');

// Устгах
Route::delete('rest/content/delete/{id}', 'ContentController@remove');

// Мэдээ хадгалах
Route::post('rest/content', 'ContentController@save');