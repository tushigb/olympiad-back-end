<?php
/*Angular - олимпиадтай холбоотой routing*/

//Infinite scroll - Олимпиадууд авах
Route::get('rest/olympiads', 'OlympiadController@getOlympiads');

//Бүх олимпиад авах
Route::get('rest/olympiads/{per_page}/{lesson}/{status}', 'OlympiadController@getAll');

//Хэрэглэгчийн id-аар авах
Route::get('rest/olympiads/user', 'OlympiadController@getOlympiadsByUserId');

//Хэрэглэгчийн id-аас шалтгаалж олимпиад авах авах
Route::get('rest/olympiad/{id}', 'OlympiadController@getOne');

//Олимпиад нэмэх
Route::post('rest/olympiad', 'OlympiadController@save');

//Олимпиад засах
Route::put('rest/olympiad', 'OlympiadController@save');

//Олимпиад устгах
Route::delete('rest/olympiad/{id}', 'OlympiadController@remove');

//Олимпиадын зоон авах
Route::get('rest/olympiad/zone/{id}', 'OlympiadController@getOlympiadZone');

//Олимпиадын зоонууд авах
Route::get('rest/olympiad/zones/{id}', 'OlympiadController@getOlympiadZones');

//VueJS-ээр дуудах олимпиадын дэлгэрэнгүйн хэрэгтэй өгөгдлүүд
Route::get('rest/olympiad/{id}/data', 'OlympiadController@getOlympiadDetailData');

//Олимпиадын статус өөрчлөх
Route::get('rest/olympiad/status_change/{id}/{status_id}', 'OlympiadController@setOlympiadStatus');

//Олимпиадын зооны статус өөрчлөх
Route::get('rest/olympiad/zone-status/{id}/{status_id}/change', 'OlympiadController@setOlympiadZoneStatus');

//Олимпиадад оролцогчийг авахgetOlympiadZones
Route::get('rest/olympiad/zone-participant/{id}', 'OlympiadController@getParticipant');

//Олимпиадад оролцогчийн мандатыг имэйлээр явуулах
Route::get('rest/olympiad/zone/participant/{id}/email', 'OlympiadController@sendMandateViaEmail');

//Олимпиадын зооны pdf file upload
Route::post('pdf-file/upload/{zone_id}/{column}', 'OlympiadController@uploadPdf');

//Олимпиадын зооны pdf file устгах
Route::post('pdf-file/{zone_id}/{column}/delete', 'OlympiadController@deletePdf');