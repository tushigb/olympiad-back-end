<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

////Нэвтрэх
//Route::post('login', 'Auth\LoginController@login')->name("login");

////Бүртгүүлэх
//Route::post('register', 'UserController@register');
////Зураг upload
//Route::post('image/upload/{id}', 'HelperController@uploadImage');
////Зураг устгах
//Route::post('image/delete', 'HelperController@deleteImage');
//
////Нэвтрэх
//Route::post('login', 'Auth\LoginController@login')->name('login');
////Logout
//Route::get('logout', 'Auth\LoginController@logout');
////Check
//Route::post('verify', 'Auth\LoginController@verify');
//
//
////Token-той хандах
//Route::group(['middleware' => ['web', 'auth:api']], function () {
//
//    //deleteToken
//    Route::post('logout', 'UserController@deleteToken');
//
//    //changePassword
//    Route::put('user/password', 'UserController@changePassword'); // Нууц үг солих
//
//    // Хэрэглэгч
//    Route::get('users', 'UserController@getAll'); // Бүх хэрэглэгч авах
//
//    Route::get('user/{id}', 'UserController@getOne'); // ID-аар нэг хэрэглэгч авах
//
//    Route::delete('user/{id}', 'UserController@remove'); // хэрэглэгч устгах
//
//    Route::put('user', 'UserController@save'); // хэрэглэгч хадгалах
//
//    Route::post('user', 'UserController@save'); // хэрэглэгч нэмэх
//
//    // Олимпиад
////    Route::get('olympiads', 'OlympiadController@getAll'); // Бүх олимпиад авах
//
//    Route::post('olympiad', 'OlympiadController@save'); // олимпиад нэмэх
//
//    //Мэдээ
//    Route::get('contents', 'ContentController@getAll'); // Бүх контент авах
//
//    Route::post('content', 'ContentController@save'); // мэдээ нэмэх
//
//    // Хичээл
//    Route::get('lessons', 'HelperController@getAllLessons'); // Бүх хот_аймаг авах
//
//    //Олимпиад хүрээ
//    Route::get('olympiad_scopes', 'HelperController@getAllOlympiadScopes'); // бүх олимпиад хүрээ авах
//});
//
//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});
//
//Route::get('olympiads', 'OlympiadController@getAll'); // Бүх олимпиад авах
//
//Route::get('olympiads/user/{userId}', 'OlympiadController@getOlympiadsByUserId'); // хэрэглэгчийн id-аар авах
//
//Route::post('olympiad/{id}', 'OlympiadController@getOne'); // хэрэглэгчийн id-аар авах
//
//// Роль
//Route::get('roles', 'HelperController@getAllRoles'); // Бүх роль авах
//
//Route::get('role/{id}', 'HelperController@getOneRole'); // ID-аар нэг роль авах
//
//Route::delete('role/{id}', 'HelperController@removeRole'); // Роль устгах
//
//Route::put('role', 'HelperController@saveRole'); // Роль хадгалах
//
//Route::post('role', 'HelperController@saveRole'); // Роль нэмэх
//
//// Хот_Аймаг
//Route::get('city_provinces', 'HelperController@getAllCityProvinces'); // Бүх хот_аймаг авах
//
//Route::get('city_province/{id}', 'HelperController@getOneCityProvince'); // ID-аар нэг хот_аймаг авах
//
//Route::delete('city_province/{id}', 'HelperController@removeCityProvince'); // хот_аймаг устгах
//
//Route::put('city_province', 'HelperController@saveCityProvince'); // хот_аймаг хадгалах
//
//Route::post('city_province', 'HelperController@saveCityProvince'); // хот_аймаг нэмэх
//
////Зоон
//Route::get('zones', 'HelperController@getAllZones'); // Бүх зоон авах
