<?php

//Mandate хадгалах
Route::post('olympiad/{id}', 'MandateController@save');

//Mandate шинэчлэх
Route::put('olympiad/{id}', 'MandateController@save');

//Mandate устгах
Route::post('mandate/delete/', 'MandateController@delete');

//Олимпиадын зооны ороцогчдыг авах
Route::get('mandates/zone/{id}/{sortKey}/{type}/{sort}/{both?}', 'MandateController@getMandatesByZoneId');

//Хайлт
Route::get('mandates/zone-search/{id}/{sortKey}/{value?}/{sort}/{both?}', 'MandateController@searchMandates');

//Мандатын ирц бүртгэл
Route::post('mandate/attendance', 'MandateController@saveAttendance');

//Мандатын ирц бүртгэл - QR
Route::get('mandate/attendance/{id}', 'MandateController@attendanceQR');

//Мандатын дүн оруулах
Route::post('mandates/zone/mark', 'MandateController@saveMark');

//Мандатын дүн авах - Дүнгийн жагсаалт
Route::get('mandates/zone/{id}/scores', 'MandateController@getScoreList');

//Мандатын дүн - Сургуулийн дундаж оноо
Route::get('mandates/zone/{id}/school/avg', 'MandateController@getSchoolAvgScoreList');

//Төлбөрийн хэсэг түр ашиглаж байгаа
Route::post('mandate', 'MandateController@payment');

//Route::get('mandate/attendance/{id}', 'MandateController@attendanceQR');

//Ерөнхий статистик - Олимпиад
Route::get('mandates/statistics/olympiad/general/{id}', 'StatisticController@generalStatOlympiad');

//Орлогын статистик - Олимпиад
Route::get('mandates/statistics/olympiad/general/benefit/{id}', 'StatisticController@benefitStatOlympiad');

//Дундаж оноо авалт статистик - Олипиад
Route::get('mandates/statistics/olympiad/general-average/{id}', 'StatisticController@averageScoreByZoneIdStatOlympiad');

//Ерөнхий статистик - Зоон
Route::get('mandates/statistics/general/{id}', 'StatisticController@generalStat');

//Дундаж статистик - Зоон
Route::get('mandates/statistics/average/{id}', 'StatisticController@averageStat');


//Route::any('{path?}', function () {
//    return File::get(public_path() . '/dist/index.html');
//})->where("path", ".+");

