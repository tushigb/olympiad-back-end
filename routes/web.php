<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::group(['middleware' => 'web'], function () {

});

Route::get('/', 'ContentController@getIndexPage');

Route::get('/shop', function () {
    return view('shop');
});

// Бидний тухай - Зохион байгуулагч авах
Route::get('/about', 'UserController@getOrganizers');

// Олимпиад дэлгэрэнгүй
Route::get('/olympiad/{id}', 'OlympiadController@getOlympiadDetail');

// Дүнгийн жагсаалт
Route::get('/score/{id}', 'MandateController@getScorePage');

// Зохион байгуулагчийн профайл
Route::get('/organization/{id}', 'UserController@getOrganizationProfile');

// Хэрэглэгчийн профайл
Route::get('/participant/{id}', 'UserController@getParticipantProfile');

Route::get('/participant/{id}/olympiad/{olympiad_id}/mandate/{mandate_id}',
    function ($id, $olympiad_id, $mandate_id) {
        if (auth()->check()) {
            $mandate = DB::table('mandates as m')
                ->join('olympiad_zones as o', 'm.olympiad_zone_id', '=', 'o.id')
                ->join('olympiads as ol', 'm.olympiad_id', '=', 'ol.id')
                ->join('olympiad_details as od', 'm.olympiad_detail_id', '=', 'od.id')
                ->join('zones as z', 'z.id', '=', 'o.min_id')
                ->join('zones as z1', 'z1.id', '=', 'o.max_id')
                ->join('zone_types as zt', 'o.zone_type_id', '=', 'zt.id')
                ->join('schools as s', 'm.school_id', '=', 's.id')
                ->select('m.id', 'm.first_name', 'm.last_name', 'm.sex', 'm.class', 'm.class_group', 'm.image_path',
                    'ol.logo', 'ol.title', 'ol.address', 'od.start_date as olympiad_start_date',
                    'o.start_date as zone_start_date', 'z.name as min',
                    'z1.name as max', 'o.zone_type_id', 'zt.name as zone_type_name', 's.name as school_name')
                ->where('m.id', '=', $mandate_id)
                ->first();

            $pdf = \Illuminate\Support\Facades\App::make('dompdf.wrapper');
            $pdf->loadView('mandate_mail_pdf', ['mandate' => $mandate]);
//            \Illuminate\Support\Facades\Mail::to('tushig.0803@gmail.com')
//                ->send(new \App\Mail\MandateShipped($pdf->output(), $mandate));
            return $pdf->stream();
//            return view('mandate_mail_pdf', ['mandate' => $mandate]);
        } else {
            return view('error');
        }
    });

Route::get('/contact', function () {
    return view('contact');
});

Route::get('/service', function () {
    return view('service');
});

//Route::get('/storage/{folder}/{filename}', function ($folder, $filename) {
//    if ($folder != 'file') {
//        $path = storage_path('app/' . $folder . '/' . $filename);
//
//        $file = File::get($path);
//        $type = File::mimeType($path);
//
//        $response = Response::make($file, 200);
//        $response->header("Content-Type", $type);
//        return $response;
//    }
//});

Route::get('/file/{id}', function ($id) {

});