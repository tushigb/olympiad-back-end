<?php
Route::get('/storage/{folder}/{file_name}', 'HelperController@getFile');

//Зураг upload
Route::post('image/upload/{folder_name}', 'HelperController@uploadImage');

//Зураг устгах
Route::post('image/delete', 'HelperController@deleteImage');

//PDF upload
Route::post('pdf/upload/{folder_name}', 'HelperController@uploadPdf');

//Файл устгах (PDF, IMAGE ETC..)
Route::post('file/remove', 'HelperController@deleteFile');

//Бүх хичээл авах
Route::get('lessons', 'HelperController@getAllLessons');

//Олимпиад хүрээ авах
Route::get('olympiad_scopes', 'HelperController@getAllOlympiadScopes');

//Бүх сургууль авах
Route::get('schools', 'HelperController@getAllSchools');

//Бүх роль авах
Route::get('roles', 'HelperController@getAllRoles');

//Бүх хот_аймаг авах
Route::get('city_provinces', 'HelperController@getAllCityProvinces');

// Зоон
Route::get('zones', 'HelperController@getAllZones'); // Бүх зоон авах

Route::post('zone', 'HelperController@saveZone'); // Зоон хадгалах

Route::get('zone/{id}/problems', 'HelperController@getProblemsByZoneId'); // Зооны бодлого авах