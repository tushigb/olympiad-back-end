<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OlympiadDetail extends Model
{
    protected $fillable = [
        'start_date', 'end_date', 'register_end_date',
        'mandate_cost', 'user_id', 'edited_user_id',
        'olympiad_id', 'olympiad_scope_id', 'lesson_id',
        'show_mobile'
    ];

    public function olympiad()
    {
        return $this->belongsTo('App\Olympiad');
    }
}