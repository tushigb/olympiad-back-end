<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Olympiad extends Model
{
    use Notifiable;

    protected $fillable = [
        'title', 'description', 'goal',
        'address', 'reward', 'requirement',
        'logo', 'cover', 'user_id',
        'edited_user_id', 'status_id'
    ];

    public function olympiadDetail()
    {
        return $this->hasOne('App\OlympiadDetail');
    }

    public function olympiadZone()
    {
        return $this->hasMany('App\OlympiadZone');
    }

    public function olympiadContentImage()
    {
        return $this->hasMany('App\OlympiadContentImage');
    }
}