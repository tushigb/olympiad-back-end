<?php

namespace App\Transformer;

use App\Mandate;
use Illuminate\Support\Facades\DB;
use League\Fractal\TransformerAbstract;

class ZoneTransformer extends TransformerAbstract
{
    public function transform($zone)
    {
        return [
            'id' => $zone->id,
            'min_id' => $zone->min_id,
            'max_id' => $zone->max_id,
            'zone_type_id' => $zone->zone_type_id,
            'gender_id' => $zone->gender_id,
            'start_date' => $zone->start_date,
            'problems' => $zone->problem,
        ];
    }
}