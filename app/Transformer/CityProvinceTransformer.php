<?php namespace App\Transformer;

use App\SumDistrict;
use App\Transformer\SumDistrictTransformer;
use League\Fractal\TransformerAbstract;

class CityProvinceTransformer extends TransformerAbstract
{

    protected $defaultIncludes = [
        'sumDistrict',
    ];

    protected $bool;

    public function __construct()
    {
//        $this->bool = $bool;
    }

    public function transform($cityProvince)
    {
        return [
            'id' => $cityProvince->id,
            'code' => $cityProvince->code,
            'name' => $cityProvince->name,
            'sum_district' => $cityProvince->sumDistrict,
        ];
    }

    public function includeSumDistrict($cityProvince)
    {
        $sumDistrict = $cityProvince->sumDistrict;
        return $this->collection($sumDistrict, new SumDistrictTransformer);
    }
}