<?php namespace App\Transformer;

use League\Fractal\TransformerAbstract;

class ContentTransformer extends TransformerAbstract
{
    public function __construct()
    {

    }

    public function transform($content)
    {
        return [
            'id' => $content->id,
            'title' => $content->title,
            'description' => $content->description,
            'speech' => $content->speech,
            'name' => $content->name,
            'name_description' => $content->name_description,
            'image_path' => $content->image_path,
            'content' => $content->content,
            'is_content' => (int)$content->is_content,
            'created_at' => $content->created_at,
            'images' => $content->olympiadContentImage
        ];
    }
}
