<?php namespace App\Transformer;

use App\OlympiadContentImage;
use App\OlympiadZone;
use Illuminate\Support\Facades\DB;
use League\Fractal\TransformerAbstract;

class OlympiadTransformer extends TransformerAbstract
{

    protected $variable;
    protected $i;

    public function __construct($variable)
    {
        $this->variable = $variable;
    }

    protected $defaultIncludes = [
        'olympiad_zone',
    ];

    public function transform($olympiad)
    {
        if ($this->variable == 1) {
            return [
                'id' => $olympiad->id,
                'title' => $olympiad->title,
                'description' => $olympiad->description,
                'goal' => $olympiad->goal,
                'address' => $olympiad->address,
                'reward' => $olympiad->reward,
                'requirement' => $olympiad->requirement,
                'logo' => $olympiad->logo,
                'cover' => $olympiad->cover,
                'guidance' => $olympiad->guidance,
                'olympiad_detail' => $olympiad->olympiadDetail,
//                'zones' => $olympiad->olympiadZone,
                'images' => OlympiadContentImage::where('olympiad_id', '=', $olympiad->id)
                    ->where('is_guidance', '=', false)
                    ->get(),
                'guidances' => OlympiadContentImage::where('olympiad_id', '=', $olympiad->id)
                    ->where('is_guidance', '=', true)
                    ->get(),
                'zone_count' => OlympiadZone::where('olympiad_id', '=', $olympiad->id)
                    ->count(),
                'zone_verified' => OlympiadZone::where('olympiad_id', '=', $olympiad->id)
                    ->where('status_id', '=', 3)
                    ->count()
            ];
        } else if ($this->variable == 2) {
            return [
                'id' => $olympiad->id,
                'title' => $olympiad->title,
                'description' => $olympiad->description,
                'goal' => $olympiad->goal,
                'address' => $olympiad->address,
                'reward' => $olympiad->reward,
                'requirement' => $olympiad->requirement,
                'logo' => $olympiad->logo,
                'cover' => $olympiad->cover,
                'guidance' => $olympiad->guidance,
                'olympiad_detail' => $olympiad->olympiadDetail,
                'status_id' => $olympiad->status_id,
//                'created_at' => $olympiad->created_at,
//                'updated_at' => $olympiad->updated_at,
//                'asd' => DB::table('olympiad_details')
//                    ->where('olympiad_id', '=', $olympiad->id)
//                    ->get()
            ];
        }
    }


    public function includeOlympiadZone($olympiad)
    {
        $olympiadZones = $olympiad->olympiadZone;
        return $this->collection($olympiadZones, new OlympiadZoneTransformer);
    }
//    public function includeOlympiadDetail($olympiad)
//    {
//        $olympiadDetail = $olympiad->olympiadDetail;
//        return $this->collection($olympiadDetail, new OlympiadDetailTransformer);
//    }
}