<?php

namespace App\Transformer;

use App\Mandate;
use Illuminate\Support\Facades\DB;
use League\Fractal\TransformerAbstract;

class OlympiadZoneTransformer extends TransformerAbstract
{
    public function transform($olympiadZone)
    {
        return [
            'id' => $olympiadZone->id,
            'min_id' => $olympiadZone->min_id,
            'max_id' => $olympiadZone->max_id,
            'zone_type_id' => $olympiadZone->zone_type_id,
            'gender_id' => $olympiadZone->gender_id,
            'start_date' => $olympiadZone->start_date,
            'problems' => $olympiadZone->problem,
            'participant_count' => Mandate::where('olympiad_zone_id', '=', $olympiadZone->id)
                ->count(),
            'verified_count' => Mandate::where('olympiad_zone_id', '=', $olympiadZone->id)
                ->where('is_paid', '=', true)
                ->count(),
            'problem_pdf' => $olympiadZone->problem_pdf,
            'solution_pdf' => $olympiadZone->solution_pdf,
            'status_id' => $olympiadZone->status_id,
            'participant_limit' => $olympiadZone->participant_limit,
        ];
    }
}