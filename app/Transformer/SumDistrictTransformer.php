<?php namespace App\Transformer;

use League\Fractal\TransformerAbstract;

class SumDistrictTransformer extends TransformerAbstract
{
    public function transform($sumDistrict)
    {
        return [
            'id' => $sumDistrict->id,
            'code' => $sumDistrict->code,
            'name' => $sumDistrict->name,
 //            'team_section' => $sumDistrict->teamSection,
        ];
    }
}