<?php namespace App\Transformer;

use App\Mark;
use League\Fractal\TransformerAbstract;

class MandateTransformer extends TransformerAbstract
{

    public function __construct()
    {
    }

    public function transform($mandate)
    {
        return [
            'id' => $mandate->id,
            'email' => $mandate->email,
            'register_number' => $mandate->register_number,
            'first_name' => $mandate->first_name,
            'last_name' => $mandate->last_name,
            'is_came' => (int)$mandate->is_came,
            'is_qr' => (int)$mandate->is_qr,
            'is_paid' => (int)$mandate->is_paid,
            'olympiad_id' => $mandate->olympiad_id,
            'olympiad_detail_id' => $mandate->olympiad_detail_id,
            'user_id' => $mandate->user_id,
            'sex' => (int)$mandate->sex,
            'mobile_phone' => $mandate->mobile_phone,
            'marks' => Mark::where('mandate_id', '=', $mandate->id)
                ->get(),
            'total' => Mark::where('mandate_id', '=', $mandate->id)
                ->sum('score')
//            'total' => Mark::where('mandate_id', '=', $mandate->id)
//                ->sum('score')
//                ->first(),
        ];
    }
}