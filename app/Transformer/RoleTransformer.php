<?php
namespace App\Transformer;

use League\Fractal\TransformerAbstract;

class RoleTransformer extends TransformerAbstract
{
    public function transform($role)
    {
        return [
            'id' => $role->id,
            'name' => $role->name
        ];
    }
}