<?php namespace App\Transformer;

use League\Fractal\TransformerAbstract;

class LessonTransformer extends TransformerAbstract
{
    public function transform($lesson)
    {
        return [
            'id' => $lesson->id,
            'name' => $lesson->name
        ];
    }
}