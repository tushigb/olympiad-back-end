<?php namespace App\Transformer;

use App\Lesson;
use League\Fractal\TransformerAbstract;

class FileTransformer extends TransformerAbstract
{
    public function __construct()
    {

    }

    public function transform($file)
    {
        return [
            'id' => $file->id,
            'name' => $file->name,
            'description' => $file->description,
            'price' => $file->price,
            'path' => $file->path,
            'user_id' => $file->user_id,
            'lesson_id' => $file->lesson_id,
            'lesson' => Lesson::where('id', '=', $file->lesson_id)
                ->select('name')
                ->first()
        ];
    }
}