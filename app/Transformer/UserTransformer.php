<?php namespace App\Transformer;

use App\CityProvince;
use App\Role;
use App\School;
use App\SumDistrict;
use App\TeamSection;
use App\User;
use Illuminate\Support\Facades\DB;
use League\Fractal\TransformerAbstract;

class UserTransformer extends TransformerAbstract
{

    public function __construct()
    {
    }

    public function transform($user)
    {
        if ($user->school_id) {
            $school = School::select('name')
                ->where('id', '=', $user->school_id)
                ->first()->name;
        } else {
            $school = null;
        }
        return [
            'id' => $user->id,
            'register_number' => $user->register_number,
            'dob' => $user->dob,
            'sex' => $user->sex,
            'first_name' => $user->first_name,
            'last_name' => $user->last_name,
            'email' => $user->email,
            'mobile_phone' => $user->mobile_phone,
            'home_phone' => $user->home_phone,
            'hurry_phone' => $user->hurry_phone,
            'student_code' => $user->student_code,
            'profession' => $user->profession,
            'image_path' => $user->image_path,
            'role' => Role::select('name')
                ->where('id', '=', $user->role_id)
                ->first()->name,
            'ad_city_province' => CityProvince::select('name')
                ->where('id', '=', $user->ad_city_province_id)
                ->first()->name,
            'ad_sum_district' => SumDistrict::select('name')
                ->where('id', '=', $user->ad_sum_district_id)
                ->first()->name,
            'ad_team_section' => $user->ad_team_section,
            'ad_apart_room' => $user->ad_apart_room,
            'school' => $school,
            'class' => $user->class,
            'class_group' => $user->class_group,
            'is_verified' => (int)$user->is_verified,
        ];
    }
}