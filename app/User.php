<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
//    use HasApiTokens, Notifiable;

    use \Illuminate\Auth\Authenticatable, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'register_number', 'dob', 'sex',
        'first_name', 'last_name', 'email',
        'mobile_phone', 'home_phone', 'hurry_phone',
        'student_code', 'profession', 'image_path',
        'description', 'password', 'role_id',
        'ad_city_province_id', 'ad_sum_district_id',
        'ad_team_section', 'ad_apart_room', 'school_id',
        'class', 'class_group', 'is_verified', 'web_site'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function cityProvince()
    {
        return $this->belongsTo('App\CityProvince');
    }

    public function sumDistrict()
    {
        return $this->belongsTo('App\SumDistrict');
    }

    public function role()
    {
        return $this->belongsTo('App\User');
    }
}