<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    protected $fillable = [
        'name', 'price', 'path', 'description',
        'user_id', 'edited_user_id', 'lesson_id'
    ];

    protected $hidden = [
        'updated_at', 'created_at',
    ];
}