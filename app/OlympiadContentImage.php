<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OlympiadContentImage extends Model
{
    protected $fillable = [
        'path', 'is_guidance', 'olympiad_id', 'olympiad_detail_id',
        'content_id'
    ];

    public function olympiad()
    {
        return $this->belongsTo('App\Olympiad');
    }

    public function content()
    {
        return $this->belongsTo('App\Content');
    }
}