<?php

namespace App\Mail;

use Barryvdh\DomPDF\PDF;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\App;

class MandateShipped extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public $pdf;
    public $mandate;

    public function __construct($pdf, $mandate)
    {
        $this->pdf = $pdf;
        $this->mandate = $mandate;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Цахим мандат')
            ->view('mandate_mail_text')
            ->attachData($this->pdf, 'mandate.pdf', [
                'mime' => 'application/pdf'
            ]);
    }
}