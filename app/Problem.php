<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Problem extends Model
{

    protected $fillable = [
        'name', 'max_score', 'user_id', 'edited_user_id',
        'olympiad_zone_id'
    ];
//    protected $fillable = [
//        'min_id', 'max_id', 'gender_id', 'zone_type_id',
//        'start_date', 'user_id', 'olympiad_id', 'olympiad_detail_id'
//    ];
    public function zone()
    {
        return $this->belongsTo('App\OlympiadZone')->withDefault([
        ]);
    }
}
