<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OlympiadZone extends Model
{
    protected $fillable = [
        'min_id', 'max_id', 'gender_id', 'zone_type_id',
        'start_date', 'participant_limit', 'user_id',
        'olympiad_id', 'olympiad_detail_id', 'status_id'
    ];

    public function olympiad()
    {
        return $this->belongsTo('App\Olympiad')->withDefault([
        ]);
    }

    public function problem()
    {
        return $this->hasMany('App\Problem');
    }
}
