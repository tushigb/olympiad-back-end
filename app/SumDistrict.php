<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SumDistrict extends Model
{
    public function user()
    {
        return $this->hasMany('App\User');
    }

    public function cityProvince()
    {
        return $this->belongsTo('App\CityProvince')->withDefault([
        ]);
    }
}
