<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mark extends Model
{
    protected $fillable = [
        'user_id', 'olympiad_id', 'olympiad_detail_id',
        'olympiad_zone_id', 'mandate_id', 'problem_id',
        'score'
    ];

//    public function mandate()
//    {
//        return $this->belongsTo('App\Mandate');
//    }
}
