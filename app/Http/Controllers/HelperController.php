<?php

namespace App\Http\Controllers;

use App\CityProvince;
use App\Lesson;
use App\OlympiadDetail;
use App\OlympiadScope;
use App\OlympiadZone;
use App\Problem;
use App\Role;
use App\School;
use App\TeamSection;
use App\Transformer\CityProvinceTransformer;
use App\Transformer\LessonTransformer;
use App\Transformer\RoleTransformer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Validator;
use File;
use Response;
use App\Zone;
use Illuminate\Http\Request;

//Туслах controller - Хэрэгтэй өгөгдлүүдийг солилцох
class HelperController extends Controller
{
    public $successStatus = 200;
    protected $response;
    protected $olympiadZone;

    public function __construct(\EllipseSynergie\ApiResponse\Contracts\Response $response,
                                OlympiadZone $olympiadZone)
    {
        $this->response = $response;
        $this->olympiadZone = $olympiadZone;
    }

    public function getFile($folder, $file_name)
    {
        if ($folder != 'file') {
            $path = storage_path('app/' . $folder . '/' . $file_name);

            $file = File::get($path);
            $type = File::mimeType($path);

            $response = Response::make($file, 200);
            $response->header("Content-Type", $type);
            return $response;
        }
    }

    // Зураг upload
    public function uploadImage(Request $request, $folder_name)
    {
        try {
            $validator = Validator::make($request->all(), [
                'image_file' => 'required|image|mimes:jpeg,png,jpg|max:3072'
            ]);

            if ($validator->fails()) {
                return response()->json($validator->errors(), 401);
            }

            $imageName = $request->file('image_file')->store($folder_name);

            if ($imageName)
                return response()->json(['responseMessage' => 'You have successfully uploaded image',
                    'imagePath' => $imageName], 200);
            else return response()->json(['responseMessage' => 'error'], 500);
        } catch (\Exception $e) {
            return $this->response->errorInternalError($e->getMessage());
        }
    }

    // Зураг устгах
    public function deleteImage(Request $request)
    {
        try {
            Storage::delete($request->image_url);
            return response()->json([
                'responseMessage' => 'You have successfully deleted image'], 200);
        } catch (\Exception $e) {
            return $this->response->errorInternalError($e->getMessage());
        }
    }

    // Файл устгах - (Зураг, pdf)
    public function deleteFile(Request $request)
    {
        try {
            Storage::delete($request->path);
            return response()->json([
                'message' => 'Файл амжилттай хууллаа'], 200);
        } catch (\Exception $e) {
            return $this->response->errorInternalError($e->getMessage());
        }
    }

    // Pdf upload хийх
    public function uploadPdf(Request $request, $folder_name)
    {
        try {
            $validator = Validator::make($request->all(), [
                'pdf_file' => 'required|mimes:pdf|max:10000'
            ]);

            if ($validator->fails()) {
                return response()->json($validator->errors(), 401);
            }

            $path = $request->file("pdf_file")->store($folder_name);

            if ($path) {
                return response()->json(['message' => 'Файл амжилттай хууллаа',
                    'path' => $path], 200);
            } else response()->json(['message' => 'Файл хуулахад ямар нэгэн алдаа гарлаа!'], 500);
        } catch (\Exception $e) {
            return $this->response->errorInternalError($e->getMessage());
        }
    }

    // Pdf устгах
    public function deletePdf(Request $request)
    {
        try {
            Storage::delete($request->path);
            return response()->json(['message', 'Файл амжилттай устлаа'], 200);
        } catch (\Exception $e) {
            return $this->response->errorInternalError($e->getMessage());
        }
    }

    // Бүх роль авах
    public function getAllRoles()
    {
        try {
            $roles = Role::paginate(15);
            return $this->response->withPaginator($roles, new  RoleTransformer());
        } catch (Exception $e) {
            return $this->response->errorInternalError($e->getMessage());
        }
    }

    // Бүх хот_аймаг авах
    public function getAllCityProvinces()
    {
        $cityProvinces = CityProvince::paginate(30);
        return $this->response->withPaginator($cityProvinces, new CityProvinceTransformer());
    }

    // Бүх сургууль авах
    public function getAllSchools()
    {
        try {
            $schools = DB::table('schools')
                ->orderBy('name', 'asc')
                ->get();
            return response()->json($schools);
        } catch (\Exception $e) {
            return $this->response->errorInternalError($e->getMessage());
        }
    }

    // Бүх хичээл авах
    public function getAllLessons()
    {
        $lessons = Lesson::all();
        return response()->json($lessons);
    }

    // Олимпиад хүрээ авах
    public function getAllOlympiadScopes()
    {
        $olympiadScopes = OlympiadScope::all();
        return response()->json($olympiadScopes);
    }

    // Бүх зоон авах
    public function getAllZones()
    {
        $zones = Zone::all();
        return response()->json($zones);
    }

    // Зоон хадгалах
    public function saveZone(Request $request)
    {
        if (Auth::check()) {
            try {
                if ($request->isMethod('put')) {

                } else {
                    $inputs = $request->all();
                    $olympiadDetail = OlympiadDetail::where('olympiad_id', $inputs['olympiad_id'])->first();
                    $inputs['user_id'] = Auth::id();
                    $inputs['olympiad_detail_id'] = $olympiadDetail->id;
                    $olympiadZone = $this->olympiadZone->create($inputs);
                    if ($olympiadZone) {
                        return response()->json(['responseMessage' => 'Olympiad Zone Created'], 200);
                    } else {
                        return response()->json(['responseMessage' => 'Could not create create  olympiad zone'], 401);
                    }
                }
            } catch (\Exception $e) {
                return $this->response->errorInternalError($e);
            }
        }
    }

    // Бодлогууд авах
    public function getProblemsByZoneId($id)
    {
        if (Auth::check()) {
            return response()->json(Problem::where('olympiad_zone_id', '=', $id)
                ->get());
        }
    }
}