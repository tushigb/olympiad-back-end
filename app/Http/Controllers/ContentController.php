<?php

namespace App\Http\Controllers;

use App\Content;
use App\OlympiadContentImage;
use App\Transformer\ContentTransformer;
use Illuminate\Http\Request;
use EllipseSynergie\ApiResponse\Contracts\Response;
use Illuminate\Support\Facades\Auth;
use Validator;
use Illuminate\Support\Facades\DB;

class ContentController extends Controller
{
    protected $response;
    protected $content;
    protected $contentImage;

    public function __construct(Response $response,
                                Content $content,
                                OlympiadContentImage $olympiadContentImage)
    {
        $this->response = $response;
        $this->content = $content;
        $this->contentImage = $olympiadContentImage;
    }

    // Infinite scroll
    public function getAll($type)
    {
        ($type == 'content') ?
            $contents = Content::where('is_content', '=', true)
                ->orderBy('id', 'desc')
                ->paginate(2) :
            $contents = Content::where('is_content', '=', false)
                ->orderBy('id', 'desc')
                ->paginate(2);
        return $this->response->withPaginator($contents, new ContentTransformer());
    }

    // Хэрэглэгчийн id-аас шалтгаалж content-ууд авах
    public function getAllByUserId($type)
    {
        ($type == 'content') ? $is_content = true : $is_content = false;
        try {
            if (Auth::user()->role_id == 1) {
                $contents = Content::where('is_content', '=', $is_content)->paginate(2);
            } else if (Auth::user()->role_id == 2) {
                $contents = Content::where('is_content', '=', $is_content)
                    ->where('user_id', '=', Auth::user()->role_id)
                    ->paginate(2);
            }
            return $this->response->withPaginator($contents, new ContentTransformer());
        } catch (\Exception $e) {
            return $this->response->errorInternalError($e);
        }
    }

    // Хэрэглэгчийн id-аас шалтгаалж content авах
    public function getOneByUserId($id)
    {
        try {
            if (Auth::user()->role_id == 1) {
                $content = Content::where('user_id', '=', Auth::user()->role_id)
                    ->where('id', '=', $id)
                    ->first();
            } else if (Auth::user()->role_id == 2) {
                $content = Content::where('user_id', '=', Auth::user()->role_id)
                    ->where('id', '=', $id)
                    ->first();
            }
            return $this->response->withItem($content, new ContentTransformer());
        } catch (\Exception $e) {
            return $this->response->errorInternalError($e);
        }
    }

    // Content устгах
    public function remove($id)
    {
        try {
            $content = Content::find($id);
            if (!$content) {
                return $this->response->errorNotFound('Olympiad Not Found');
            }
            if ($content->delete()) {
                return $this->response->withItem($content, new  ContentTransformer());
            } else {
                return $this->response->errorInternalError('Could not delete a olympiad');
            }
        } catch (\Exception $e) {
            return $this->response->errorInternalError($e);
        }
    }

    // Хадгалах, шинэчлэх
    public function save(Request $request)
    {
        try {
            $inputs = $request->all();
            if (isset($inputs['id'])) {
                $this->content = Content::find($inputs['id']);
                if ($this->content->update($inputs)) {
                    if ($request->has('images')) {
                        OlympiadContentImage::where('content_id', '=', $this->content->id)
                            ->delete();
                        $images = $request->input('images');
                        foreach ($images as $key => $image) {
                            $inputs['path'] = $image['path'];
                            $inputs['content_id'] = $this->content->id;
                            $this->contentImage->create($inputs);
                        }
                        return $this->response->withItem($this->content, new ContentTransformer());
                    }
                }
            } else {
                $inputs['user_id'] = Auth::id();
                $content = $this->content->create($inputs);
                if ($content) {
                    if ($request->has('images')) {
                        $images = $request->input('images');
                        foreach ($images as $key => $image) {
                            if ($key == 0) {
                                if (!$content->is_content) {
                                    $content->image_path = $image['path'];
                                    $content->update();
                                } else {
                                    $inputs['path'] = $image['path'];
                                    $inputs['content_id'] = $content->id;
                                    $this->contentImage->create($inputs);
                                }
                            } else {
                                $inputs['path'] = $image['path'];
                                $inputs['content_id'] = $content->id;
                                $this->contentImage->create($inputs);
                            }
                        }
                    }
                    return response()->json(['responseMessage' => 'Content created'], 200);
                } else return response()->json(['responseMessage' => 'error'], 200);
            }
        } catch
        (\Exception $e) {
            return $e->getMessage();
        }
    }

    // Index хуудасны мэдээ авах
    public function getIndexPage()
    {
        $content = DB::table('contents')
            ->where('is_content', '=', false)
            ->orderBy('id', 'desc')
            ->first();

        ($content) ?
            $data = array('content' => $content) : $data = array('content' => null);
        return view('index', $data);
//    $olympiads = DB::table('olympiads')
//        ->join('users', 'users.id', '=', 'olympiads.user_id')
//        ->join('olympiad_details', 'olympiad_details.olympiad_id', '=', 'olympiads.id')
//        ->join('lessons', 'lessons.id', '=', 'olympiad_details.lesson_id')
//        ->select('olympiads.id', 'olympiads.title', 'olympiads.logo', 'olympiads.cover', 'olympiads.user_id',
//            'olympiad_details.mandate_cost', 'users.first_name', 'olympiad_details.register_end_date',
//            'olympiad_details.start_date', 'lessons.name as lesson_name')
//        ->orderBy('olympiad_details.start_date')
//        ->get();
//    $data = array(
//        'olympiads' => $olympiads
//    );
//    return view('index', $data);
    }
}