<?php

namespace App\Http\Controllers;

use App\Invoice;
use App\Lesson;
use App\Mail\MandateShipped;
use App\Mandate;
use App\Olympiad;
use App\OlympiadContentImage;
use App\OlympiadDetail;
use App\OlympiadZone;
use App\Problem;
use App\Transformer\OlympiadTransformer;
use App\User;
use App\Zone;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use EllipseSynergie\ApiResponse\Contracts\Response;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use Validator;


class OlympiadController extends Controller
{
    public $successStatus = 200;
    protected $response;
    protected $olympiad;
    protected $olympiadDetail;
    protected $olympiadZone;
    protected $olympiadImage;
    protected $problem;
    protected $invoice;

    public function __construct(Response $response, Olympiad $olympiad,
                                OlympiadDetail $olympiadDetail,
                                OlympiadContentImage $olympiadContentImage,
                                OlympiadZone $olympiadZone,
                                Problem $problem,
                                Invoice $invoice)
    {
        $this->response = $response;
        $this->olympiad = $olympiad;
        $this->olympiadDetail = $olympiadDetail;
        $this->olympiadZone = $olympiadZone;
        $this->olympiadImage = $olympiadContentImage;
        $this->problem = $problem;
        $this->invoice = $invoice;
    }

    //Q-Pay QR Code generate хийж байгаа - Нэхэмжлэл үүсгэх
    public function generateQPay($mandate, $olympiad)
    {
        $client = new Client();
        $auth = base64_encode('qpay_tech_avdar:kGmbVU4C');
        $url = 'http://43.231.112.201:8080/WebServiceQPayMerchant.asmx/qPay_genInvoiceSimple';
        $invoiceNumber = 'm' . md5(Carbon::now()) . $mandate->id . Auth::id();
//        ' . '"' . "мандат" . $mandate->id . " " . Carbon::now() . '"' . '
        $data = '
        {
            "type": "4",
            "merchant_code": "TECH_AVDAR",
            "merchant_verification_code": "Sh9Ug2EZd78GjekP",
            "merchant_customer_code": "85032613",
            "json_data": {
                "invoice_code": "TECH_AVDAR_INVOICE",
                "merchant_branch_code": "1",
                "merchant_invoice_number": ' . '"' . $invoiceNumber . '"' . ',
                "invoice_date": ' . '"' . Carbon::now() . ".000" . '"' . ',
                "invoice_description": ' . '"' . $mandate->register_number . '"' . ',
                "invoice_total_amount": ' . '"' . $olympiad->mandate_cost . '"' . ',
                "item_btuk_code": "5654454",
                "vat_flag": "0"
            }
        }';
        $json = json_decode($data, true);
        try {
            $response = $client->post($url,
                [
                    'headers' => [
                        'Authorization' => 'Basic ' . $auth,
                        'Content-Type' => 'application/json'
                    ],
                    'json' => $json
                ]
            );
            return json_decode($response->getBody(), true);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    //Үндсэн вебээс олимпиадын дэлгэрэнгүй харах
    public function getOlympiadDetail($id)
    {
//        $mandates = DB::table('mandates as m')
//            ->join('olympiad_zones as oz', 'oz.id', '=', 'm.olympiad_zone_id')
//            ->join('zones as z1', 'z1.id', '=', 'oz.min_id')
//            ->join('zones as z2', 'z2.id', '=', 'oz.max_id')
//            ->join('zones as z3', 'z3.id', '=', 'oz.gender_id')
//            ->join('zone_types as zt', 'zt.id', '=', 'oz.zone_type_id')
//            ->select('m.*', 'oz.id as olympiad_zone_id', 'oz.start_date', 'z1.name as min', 'z2.name as max', 'z3.name as gender',
//                'zt.name as type')
//            ->where('m.user_id', '=', Auth::id())
//            ->where('m.olympiad_id', '=', $id)
//            ->get();
//
//        $olympiad_detail = OlympiadDetail::where('olympiad_id', '=', $id)
//            ->first();
//
//        if ($olympiad_detail->mandate_cost > 0) {
//            foreach ($mandates as $mandate) {
//                $invoice = json_decode(Invoice::where('mandate_id', '=', $mandate->id)
//                    ->select('invoice_json')
//                    ->first(), true)['invoice_json'];
//                $mandate->qr_image = json_decode($invoice, true)['json_data']['qPay_QRimage'];
//            }
//        }
//
//        return $mandates;

        $olympiad = DB::table('olympiads as o')
            ->join('users as u', 'u.id', '=', 'o.user_id')
            ->join('olympiad_details as od', 'od.olympiad_id', '=', 'o.id')
            ->join('lessons as l', 'l.id', '=', 'od.lesson_id')
            ->select('o.*', 'u.first_name', 'od.register_end_date', 'od.show_mobile',
                'od.start_date', 'od.end_date', 'od.mandate_cost', 'l.name as lesson_name')
            ->where('olympiad_id', '=', $id)
            ->first();

        if ($olympiad && $olympiad->status_id != 1) {

            $images = DB::table('olympiad_content_images')
                ->select('path')->where('olympiad_id', '=', $id)
                ->where('is_guidance', '=', false)
                ->get();

            $guidances = DB::table('olympiad_content_images')
                ->select('path')->where('olympiad_id', '=', $id)
                ->where('is_guidance', '=', true)
                ->get();

            $zones = DB::table('olympiad_zones as zo')
                ->join('zones as z', 'z.id', '=', 'zo.min_id')
                ->join('zones as z2', 'z2.id', '=', 'zo.max_id')
                ->join('zones as z3', 'z3.id', '=', 'zo.gender_id')
                ->join('zone_types as zt', 'zt.id', '=', 'zo.zone_type_id')
                ->select('zo.id', 'zo.start_date', 'z.name as min', 'z2.name as max', 'z3.name as gender',
                    'zt.name as type', 'zo.zone_type_id', 'zo.status_id', 'zo.participant_limit as limit',
                    DB::raw("(SELECT COUNT(*) FROM mandates WHERE olympiad_zone_id = zo.id) as count"))
                ->where('olympiad_id', '=', $id)
                ->orderBy('zo.min_id', 'asc')
                ->get();

            $organizer = User::find($olympiad->user_id);

            $mandate = Mandate::where('olympiad_id', '=', $olympiad->id)
                ->where('user_id', '=', auth()->id())
                ->first();


            if ($mandate) {
                $invoice = json_decode(Invoice::where('mandate_id', '=', $mandate->id)
                    ->select('invoice_json')
                    ->first(), true)['invoice_json'];
                $qr_image = json_decode($invoice, true)['json_data']['qPay_QRimage'];
            } else {
                $mandate = null;
                $currentZone = null;
                $qr_image = null;
            }

            if (auth()->check()) {
                $authUser = DB::table('users')
                    ->select('id', 'dob', DB::raw("(SELECT TIMESTAMPDIFF(YEAR,dob,CURDATE())) as age"))
                    ->where('id', '=', auth()->id())
                    ->first();
                $age = $authUser->age;
//                return response()->json($authUser);
            } else {
                $age = null;
            }

            $data = array(
                'olympiad' => $olympiad,
                'images' => $images,
                'zones' => $zones,
                'guidances' => $guidances,
//                'currentZone' => $currentZone,
                'organizer' => $organizer,
                'mandate' => $mandate,
                'age' => $age,
                'qr_image' => $qr_image
            );
            return view('olympiad_detail', $data);
        } else {
            return view('error');
        }
    }

    //VueJS-ээр дуудах олимпиадын зоонууд
    public function getOlympiadZones($id)
    {
        $zones = DB::table('olympiad_zones as zo')
            ->join('zones as z', 'z.id', '=', 'zo.min_id')
            ->join('zones as z2', 'z2.id', '=', 'zo.max_id')
            ->join('zones as z3', 'z3.id', '=', 'zo.gender_id')
            ->join('zone_types as zt', 'zt.id', '=', 'zo.zone_type_id')
            ->select('zo.id', 'zo.start_date', 'z.name as min', 'z2.name as max', 'z3.name as gender',
                'zt.name as type', 'zo.zone_type_id', 'zo.status_id', 'zo.participant_limit as limit',
                DB::raw("(SELECT COUNT(*) FROM mandates WHERE olympiad_zone_id = zo.id) as count"))
            ->where('olympiad_id', '=', $id)
            ->orderBy('zo.min_id', 'asc')
            ->get();
        return response()->json($zones);
    }

    //VueJS-ээр дуудах хэрэгтэй өгөгдлүүд
    public function getOlympiadDetailData($id)
    {
        $mandates = DB::table('mandates as m')
            ->join('olympiad_zones as oz', 'oz.id', '=', 'm.olympiad_zone_id')
            ->join('zones as z1', 'z1.id', '=', 'oz.min_id')
            ->join('zones as z2', 'z2.id', '=', 'oz.max_id')
            ->join('zones as z3', 'z3.id', '=', 'oz.gender_id')
            ->join('zone_types as zt', 'zt.id', '=', 'oz.zone_type_id')
            ->select('m.*', 'oz.id as olympiad_zone_id', 'oz.start_date', 'z1.name as min', 'z2.name as max', 'z3.name as gender',
                'zt.name as type')
            ->where('m.user_id', '=', Auth::id())
            ->where('m.olympiad_id', '=', $id)
            ->get();

        $olympiad_detail = OlympiadDetail::where('olympiad_id', '=', $id)
            ->first();

        if ($olympiad_detail->mandate_cost > 0) {
            foreach ($mandates as $mandate) {
                $invoice = json_decode(Invoice::where('mandate_id', '=', $mandate->id)
                    ->select('invoice_json')
                    ->first(), true)['invoice_json'];
                $mandate->qr_image = json_decode($invoice, true)['json_data']['qPay_QRimage'];
            }
        }

        $user = User::select('class', DB::raw("(SELECT TIMESTAMPDIFF(YEAR,dob,CURDATE())) as age"))
            ->where('id', '=', auth()->id())
            ->first();

        $data = array(
            'mandates' => $mandates,
            'user' => $user,
        );

        return response()->json($data);
    }

    // Infinite scroll
    public function getOlympiads()
    {
        $olympiads = DB::table('olympiads')
            ->join('users', 'users.id', '=', 'olympiads.user_id')
            ->join('olympiad_details', 'olympiad_details.olympiad_id', '=', 'olympiads.id')
            ->join('lessons', 'lessons.id', '=', 'olympiad_details.lesson_id')
            ->where('status_id', '=', 2)
            ->select('olympiads.id', 'olympiads.title', 'olympiads.logo', 'olympiads.cover', 'olympiads.user_id',
                'olympiad_details.mandate_cost', 'users.first_name', 'olympiad_details.register_end_date',
                'olympiad_details.start_date', 'lessons.name as lesson_name')
            ->orderBy('olympiad_details.start_date')
            ->paginate(2);
        $olympiads = json_decode(json_encode($olympiads), true);
        return response()->json($olympiads, 200);
    }

    //Бүх олимпиадыг авах / зөвхөн админ эрхтэй хэрэглэгч л авч чадна - Filter
    public function getAll($per_page, $lesson, $status)
    {
        if (Auth::check() && Auth::user()->role_id == 1) {
            try {
                if ($lesson == 0) {
                    $count = DB::table('lessons')->count();
                    $lesson = [];
                    for ($i = 0; $i < $count; $i++) {
                        array_push($lesson, (int)$i + 1);
                    }
                }

                ($status == 0) ?
                    $olympiads = Olympiad::join('olympiad_details', 'olympiad_details.olympiad_id', '=', 'olympiads.id')
                        ->whereIn('olympiad_details.lesson_id', [$lesson])
                        ->paginate($per_page) :

                    $olympiads = Olympiad::join('olympiad_details', 'olympiad_details.olympiad_id', '=', 'olympiads.id')
                        ->whereIn('status_id', [$status])
                        ->whereIn('olympiad_details.lesson_id', [$lesson])
                        ->paginate($per_page);

                return $this->response->withPaginator($olympiads, new OlympiadTransformer(2));

            } catch (\Exception $e) {
//            return $this->response->errorInternalError($e);
                return $e->getMessage() . ' ' . $e->getLine();
            }
        }
    }

    //Хэрэглэгчээс шалтгаалж хэрэглэгчийн олимпиадуудыг авах
    public function getOlympiadsByUserId()
    {
        if (Auth::check()) {
            try {
                if (Auth::user()->role_id == 2) {
                    $olympiads = Olympiad::where('user_id', '=', Auth::id())
                        ->orderBy('id', 'desc')
                        ->paginate(10);
                    return $this->response->withPaginator($olympiads, new OlympiadTransformer(2));
                } else if (Auth::user()->role_id == 1) {
                    $olympiads = Olympiad::orderBy('id', 'desc')
                        ->paginate(10);
                    return $this->response->withPaginator($olympiads, new OlympiadTransformer(2));
                }
            } catch (\Exception $e) {
                return $this->response->errorInternalError($e);
            }
        }
    }

    //Хэрэглэгчээс шалтгаалж олимпиадыг авах
    public function getOne($id)
    {
        if (Auth::check()) {
            try {
                $olympiad = Olympiad::find($id);
                if (!$olympiad) {
                    return $this->response->errorNotFound('Olympiad Not Found');
                } else {
                    if ($olympiad->user_id == Auth::id() || Auth::user()->role_id == 1) {
                        return $this->response->withItem($olympiad, new OlympiadTransformer(1));
                    } else {
                        return response()->json(['responseMessage' => 'You cant access this olympiad'], 403);
                    }
                }
            } catch (\Exception $e) {
                return $this->response->errorInternalError($e);
            }
        }
    }

    //Устгах
    public function remove($id)
    {
        if (Auth::check()) {
            try {
                $olympiad = Olympiad::find($id);
                if (!$olympiad) {
                    return $this->response->errorNotFound('Olympiad Not Found');
                }
                if ($olympiad->user_id == Auth::id() || Auth::user()->role_id == 1) {
                    if ($olympiad->delete()) {
                        return response()->json(['responseMessage' => 'Olympiad deleted'], 200);
                    } else {
                        return $this->response->errorInternalError('Could not delete a olympiad');
                    }
                }
            } catch (\Exception $e) {
                return $this->response->errorInternalError($e);
            }
        }
    }

    //Хадгалах, засах
    public function save(Request $request)
    {
        if (Auth::check()) {
            try {
                if ($request->isMethod('put')) {
                    if ($olympiad = $this->olympiad->find($request->input('id'))) {
                        if ($olympiad->logo != $request->input('logo'))
                            Storage::delete($olympiad->logo);
                        if ($olympiad->cover != $request->input('cover'))
                            Storage::delete($olympiad->cover);
                        if ($olympiad->update($request->all())) {
                            if ($olympiadDetail = OlympiadDetail::find($olympiad->id)) {
                                $inputs = $request->input('olympiad_detail');
                                $inputs['edited_user_id'] = Auth::id();
                                if ($olympiadDetail->update($inputs)) {

                                    if ($request->has('guidances')) {
                                        $old_images = OlympiadContentImage::where('olympiad_id', '=', $olympiad->id)
                                            ->where('is_guidance', '=', true)
                                            ->get();
                                        $new_images = $request->input('guidances');
                                        foreach ($old_images as $old_image) {
                                            $having = false;
                                            foreach ($new_images as $new_image) {
                                                if ($old_image['path'] == $new_image['path']) {
                                                    $having = true;
                                                }
                                            }
                                            if (!$having) {
                                                Storage::delete($old_image->path);
                                                $old_image->delete();
                                            }
                                        }
                                    }

                                    if ($request->has('images')) {
                                        $old_images = OlympiadContentImage::where('olympiad_id', '=', $olympiad->id)
                                            ->where('is_guidance', '=', false)
                                            ->get();
                                        $new_images = $request->input('images');
                                        foreach ($old_images as $old_image) {
                                            $having = false;
                                            foreach ($new_images as $new_image) {
                                                if ($old_image['path'] == $new_image['path']) {
                                                    $having = true;
                                                }
                                            }
                                            if (!$having) {
                                                Storage::delete($old_image->path);
                                                $old_image->delete();
                                            }
                                        }
//                                        foreach ($new_images as $key_new => $new_image) {
//                                            $having = false;
//                                            foreach ($old_images as $key_old => $old_image) {
//                                                if ($new_image->path == $old_image->path) {
//                                                    $having = true;
//                                                }
//                                            }
//                                            if (!$having) {
//                                                Storage::delete($old_image->path);
//                                                $old_image->delete();
//                                                $inputs['path'] = $new_image['path'];
//                                                $inputs['is_guidance'] = false;
//                                                $this->olympiadImage->create($inputs);
//                                            }
//                                        }
                                    }

                                    if ($request->has('images')) {
                                        $images = $request->input('images');
                                        foreach ($images as $key => $image) {
                                            if (!isset($image['id']) && $image['path'] != null) {
                                                $inputs['path'] = $image['path'];
                                                $inputs['is_guidance'] = false;
                                                $this->olympiadImage->create($inputs);
                                            }
                                        }
                                    }

                                    if ($request->has('guidances')) {
                                        $images = $request->input('guidances');
                                        foreach ($images as $key => $image) {
                                            if (!isset($image['id']) && $image['path'] != null) {
                                                $inputs['path'] = $image['path'];
                                                $inputs['is_guidance'] = true;
                                                $this->olympiadImage->create($inputs);
                                            }
                                        }
                                    }

//                                    if ($request->has('guidances')) {
//                                        $old_images = OlympiadContentImage::where('olympiad_id', '=', $olympiad->id)
//                                            ->where('is_guidance', '=', true)
//                                            ->get();
//                                        $new_images = $request->input('images');
//                                        foreach ($new_images as $key_new => $new_image) {
//                                            $having = false;
//                                            foreach ($old_images as $key_old => $old_image) {
//                                                if ($new_image->path == $old_image->path) {
//                                                    $having = true;
//                                                }
//                                            }
//                                            if (!$having) {
//                                                Storage::delete($old_images[$key_new]->path);
//                                                $old_image->delete();
//                                                $inputs['path'] = $new_image['path'];
//                                                $inputs['is_guidance'] = true;
//                                                $this->olympiadImage->create($inputs);
//                                            }
//                                        }
//                                    }

//                                    $images = DB::table('olympiad_content_images')
//                                        ->where('olympiad_id', '=', $olympiad->id)
//                                        ->get();
//
//
//                                    foreach ($images as $image) {
//                                        OlympiadContentImage::find($image->id)->delete();
//                                        Storage::delete($image->path);
//                                    }
//
//                                    if ($request->has('images')) {
//                                        $images = $request->input('images');
//                                        foreach ($images as $key => $image) {
//                                            if ($image['path'] != null) {
//                                                $inputs['path'] = $image['path'];
//                                                $inputs['is_guidance'] = false;
//                                                $this->olympiadImage->create($inputs);
//                                            }
//                                        }
//                                    }
//
//                                    if ($request->has('guidances')) {
//                                        $images = $request->input('guidances');
//                                        foreach ($images as $key => $image) {
//                                            if ($image['path'] != null) {
//                                                $inputs['path'] = $image['path'];
//                                                $inputs['is_guidance'] = true;
//                                                $this->olympiadImage->create($inputs);
//                                            }
//                                        }
//                                    }

                                    if ($olympiad->status_id == 1) {
                                        OlympiadZone::where('olympiad_id', '=', $olympiad->id)
                                            ->where('olympiad_detail_id', '=', $olympiadDetail->id)
                                            ->delete();

                                        if ($request->has('zones')) {
                                            $zones = $request->input('zones');
                                            foreach ($zones as $zone) {
                                                $inputs = $zone;
                                                $inputs['user_id'] = Auth::id();
                                                $inputs['olympiad_id'] = $olympiad->id;
                                                $inputs['olympiad_detail_id'] = $olympiadDetail->id;
                                                $inputs['status_id'] = 1;
                                                $olympiadZone = $this->olympiadZone->create($inputs);
                                                foreach ($zone['problems'] as $problem) {
                                                    $inputs = $problem;
                                                    $inputs['user_id'] = Auth::id();
                                                    $inputs['olympiad_zone_id'] = $olympiadZone->id;
                                                    $this->problem->create($inputs);
                                                }
                                            }
                                        }
                                    }
                                    return response()->json([
                                        'responseMessage' => 'Амжилттай шинэчлэгдлээ'], 200);
                                }
                            }
                        }
                    }
                } else {
                    $inputs = $request->all();
                    $inputs['status_id'] = 1;
                    $olympiad = $this->olympiad->create($inputs);
                    if ($olympiad) {
                        $inputs = $request->all();
                        $inputs['olympiad_id'] = $olympiad->id;
                        $olympiadDetail = $this->olympiadDetail->create($inputs);
                        if ($olympiadDetail) {
                            $inputs['olympiad_detail_id'] = $olympiadDetail->id;
                            if ($request->has('images')) {
                                $images = $request->input('images');
                                foreach ($images as $key => $image) {
                                    $inputs['path'] = $image['path'];
                                    $inputs['is_guidance'] = false;
                                    $this->olympiadImage->create($inputs);
                                }
                            }
                            if ($request->has('guidances')) {
                                $images = $request->input('guidances');
                                foreach ($images as $key => $image) {
                                    $inputs['path'] = $image['path'];
                                    $inputs['is_guidance'] = true;
                                    $this->olympiadImage->create($inputs);
                                }
                            }
                            if ($request->has('zones')) {
                                $zones = $request->input('zones');
                                foreach ($zones as $zone) {
                                    $inputs = $zone;
                                    $inputs['user_id'] = Auth::id();
                                    $inputs['olympiad_id'] = $olympiad->id;
                                    $inputs['olympiad_detail_id'] = $olympiadDetail->id;
                                    $inputs['status_id'] = 1;
                                    $olympiadZone = $this->olympiadZone->create($inputs);
                                    foreach ($zone['problems'] as $problem) {
                                        $inputs = $problem;
                                        $inputs['user_id'] = Auth::id();
                                        $inputs['olympiad_zone_id'] = $olympiadZone->id;
                                        $this->problem->create($inputs);
                                    }
                                }
                            }
                            return response()->json(['responseMessage' => 'Olympiad Created'], 200);
                        }
                    }
                }
            } catch (\Exception $e) {
                return $e->getMessage() . ' ' . $e->getLine();
//        return $this->response->errorInternalError($e);
            }
        }
    }

    // Олимпиадын зоон авах
    public function getOlympiadZone($id)
    {
        try {
            $olympiadZone = OlympiadZone::find($id);
            if ($olympiadZone)
                return response()->json($olympiadZone, 200);
            else return response()->json(['responseMessage' => 'Could not get olympiad zone'], 401);
        } catch (\Exception $e) {
            return $this->response->errorInternalError($e);
        }
    }

    // Олимпиадын статус өөрчлөх
    public function setOlympiadStatus($id, $status_id)
    {
        if (Auth::user()->role_id == 1) {
            try {
                $olympiad = Olympiad::find($id);
                if ($olympiad) {
                    $olympiad->status_id = $status_id;
                    if ($olympiad->update()) {
                        return response()->json(['responseMessage' => 'success'], 200);
                    }
                }
            } catch (\Exception $e) {
                return $this->response->errorInternalError($e);
            }
        }
    }

    // Олимпиадын зооны статус өөрчлөх
    public function setOlympiadZoneStatus($id, $status_id)
    {
        try {
            $olympiadZone = OlympiadZone::find($id);
            if ($olympiadZone) {
                if (Auth::user()->role_id == 2) {
                    if ($olympiadZone->status_id < $status_id) {
                        $olympiadZone->status_id = $status_id;
                        if ($olympiadZone->update())
                            return response()->json($olympiadZone, 200);
                        else return response()->json(['responseMessage' => 'Could not update olympiad zone'], 500);
                    } else {
                        return response()->json(['responseMessage' => 'Could not update olympiad zone'], 500);
                    }
                } else if (Auth::user()->role_id == 1) {
                    $olympiadZone->status_id = $status_id;
                    if ($olympiadZone->update())
                        return response()->json($olympiadZone, 200);
                    else return response()->json(['responseMessage' => 'Could not update olympiad zone'], 500);
                }
            } else {
                return response()->json(['responseMessage' => 'Could not get olympiad zone'], 404);
            }

        } catch (\Exception $e) {
            return $this->response->errorInternalError($e);
        }
    }

    // Олимпиадын зооны оролцогчийн мэдээлэл авах
    public function getParticipant($id)
    {
        if (Auth::check()) {
            try {
                if (Auth::user()->role_id == 1 || Auth::user()->role_id == 2) {
                    return response()->json(
                        DB::table('users as u')
                            ->join('city_provinces as cp', 'cp.id', '=', 'u.ad_city_province_id')
                            ->join('sum_districts as sd', 'sd.id', '=', 'u.ad_sum_district_id')
                            ->join('schools as s', 's.id', '=', 'u.school_id')
                            ->select('u.register_number', 'u.first_name', 'u.last_name',
                                'u.sex', 'u.dob', 'u.email', 'u.image_path', 'u.email', 'u.ad_apart_room as address',
                                'u.mobile_phone', 'u.home_phone', 'hurry_phone', 'u.student_code',
                                'u.profession', 'u.class', 'u.class_group', 'u.role_id',
                                'cp.name as city_province', 'sd.name as sum_district',
                                'u.ad_team_section as team_section', 's.name as school')
                            ->where('u.id', '=', $id)
                            ->first()
                    );
                } else return response()->json(['responseMessage' => 'Access denied'], 403);
            } catch (\Exception $e) {
                return $this->response->errorInternalError($e->getMessage());
            }
        }
    }

    // Зооны pdf файл upload хийх
    public function uploadPdf(Request $request, $zone_id, $column)
    {
        try {
            $validator = Validator::make($request->all(), [
                'pdf_file' => 'required|mimes:pdf|max:10000'
            ]);

            if ($validator->fails()) {
                return response()->json($validator->errors(), 401);
            }

            $pdfName = $request->file("pdf_file")->store('olympiad_pdf');

            if ($pdfName) {
                $olympiadZone = OlympiadZone::find($zone_id);
                ($column == 'problem') ? $olympiadZone->problem_pdf = $pdfName : $olympiadZone->solution_pdf = $pdfName;
                if (($olympiadZone->update()))
                    return response()->json([
                        'message' => 'You have successfully uploaded pdf',
                        'path' => $pdfName,
                    ]);
                else return response()->json(['message' => 'error'], 401);
            } else {
                return response()->json(['message' => 'error'], 401);
            }
        } catch (\Exception $e) {
            return $this->response->errorInternalError($e->getMessage());
        }
    }

    // Зооны pdf файл устгах
    public function deletePdf(Request $request, $zone_id, $column)
    {
        try {
            $olympiadZone = OlympiadZone::find($zone_id);
            ($column == 'problem') ? $olympiadZone->problem_pdf = null : $olympiadZone->solution_pdf = null;
            if ($olympiadZone->update()) {
                if (Storage::delete($request->pdf_url))
                    return response()->json([
                        'responseMessage' => 'You have successfully deleted pdf file'], 200);
                else return response()->json(['responseMessage' => 'error'], 401);
            } else return response()->json(['responseMessage' => 'error'], 401);
        } catch (\Exception $e) {
            return $this->response->errorInternalError($e->getMessage());
        }
    }

    public function sendMandateViaEmail($id)
    {
        if (Auth::check()) {
            try {
                if (Auth::user()->role_id == 1 || Auth::user()->role_id == 2) {
                    $mandate = DB::table('mandates as m')
                        ->join('olympiad_zones as o', 'm.olympiad_zone_id', '=', 'o.id')
                        ->join('olympiads as ol', 'm.olympiad_id', '=', 'ol.id')
                        ->join('olympiad_details as od', 'm.olympiad_detail_id', '=', 'od.id')
                        ->join('zones as z', 'z.id', '=', 'o.min_id')
                        ->join('zones as z1', 'z1.id', '=', 'o.max_id')
                        ->join('zone_types as zt', 'o.zone_type_id', '=', 'zt.id')
                        ->join('schools as s', 'm.school_id', '=', 's.id')
                        ->select('m.id', 'm.first_name', 'm.last_name', 'm.sex', 'm.class', 'm.class_group', 'm.image_path',
                            'm.email as email', 'ol.logo', 'ol.title', 'ol.address', 'od.start_date as olympiad_start_date',
                            'o.start_date as zone_start_date', 'z.name as min',
                            'z1.name as max', 'o.zone_type_id', 'zt.name as zone_type_name', 's.name as school_name')
                        ->where('m.id', '=', $id)
                        ->first();

                    $pdf = App::make('dompdf.wrapper');
                    $pdf->loadView('mandate_mail_pdf', ['mandate' => $mandate]);
                    Mail::to($mandate->email)
                        ->send(new MandateShipped($pdf->output(), $mandate));
                    if (Mail::failures())
                        return response()->json(['responseMessage' => 'Error'], 500);
                    else return response()->json(['responseMessage' => 'Mail sent'], 200);
                }
            } catch (\Exception $e) {
                return $this->response->errorInternalError($e->getMessage());
            }
        }
    }
}