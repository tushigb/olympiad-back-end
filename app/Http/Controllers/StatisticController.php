<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class StatisticController extends Controller
{
    // Олимпиадын ерөнхий статистик авах
    public function generalStatOlympiad($id)
    {
        try {
            $stats = DB::table('mandates')
                ->select(
                    DB::raw("(SELECT COUNT(*) FROM mandates where olympiad_id = " . $id . ") as total"),
                    DB::raw("(SELECT COUNT(*) FROM mandates where is_paid = TRUE AND olympiad_id = " . $id . ") as verified"),
                    DB::raw("(SELECT COUNT(sex) FROM mandates WHERE is_paid = TRUE AND is_came = true AND sex = true AND olympiad_id = " . $id . ") as male"),
                    DB::raw("(SELECT COUNT(sex) FROM mandates WHERE is_paid = TRUE AND is_came = true AND sex = false AND olympiad_id = " . $id . ") as female"),
                    DB::raw("(SELECT COUNT(is_came) FROM mandates WHERE is_paid = TRUE AND is_came = true AND olympiad_id = " . $id . ") as came"),
                    DB::raw("(SELECT COUNT(is_came) FROM mandates WHERE is_paid = TRUE AND is_came = false AND olympiad_id = " . $id . ") as notCame"))
                ->groupBy('total', 'verified', 'male', 'female', 'came', 'notCame')
                ->first();
            return response()->json($stats);
        } catch (\Exception $e) {
            return $e->getMessage() . ' ' . $e->getLine();
        }
    }

    // Олимпиадын нийт зооны ашиг авах
    public function benefitStatOlympiad($id)
    {
        try {
            $benefits = DB::select("SELECT oz.id as zone_id, oz.min_id, oz.max_id, count(m.id) as mandate_count, 
                                    o.mandate_cost, count(m.id) * o.mandate_cost as benefit, oz.olympiad_id
                                    FROM olympiad_zones oz
                                    INNER JOIN mandates m ON oz.id = m.olympiad_zone_id
                                    INNER JOIN olympiad_details o ON m.olympiad_detail_id = o.id
                                    WHERE m.is_paid = TRUE AND oz.olympiad_id = " . $id . "
                                    GROUP BY oz.id, o.mandate_cost");
            return response()->json($benefits);
        } catch (\Exception $e) {
            return $e->getMessage() . ' ' . $e->getLine();
        }
    }

    // Олимпиадын нийт зооны дундаж оноо
    public function averageScoreByZoneIdStatOlympiad($id)
    {
        try {
            $averages = DB::select("SELECT id as zone_id, max_id, min_id, avg(total) as average_score
                                    FROM (SELECT oz.id, oz.max_id, oz.min_id, oz.zone_type_id, 
                                    oz.gender_id, oz.olympiad_id, oz.olympiad_detail_id,
                                    ma.user_id, sum(ma.score) as total
                                    FROM olympiad_zones as oz
                                    INNER JOIN marks as  ma ON oz.id = ma.olympiad_zone_id
                                    WHERE oz.olympiad_id = " . $id . "
                                    GROUP BY ma.user_id, oz.id) as tmp
                                    GROUP BY id;");
            return response()->json($averages);
        } catch (\Exception $e) {
            return $e->getMessage() . ' ' . $e->getLine();
        }
    }

    // Зооны ерөнхий статистик авах
    public function generalStat($id)
    {
        try {
            $stats = DB::table('mandates')
                ->select(
                    DB::raw("(SELECT COUNT(*) FROM mandates where olympiad_zone_id = " . $id . ") as total"),
                    DB::raw("(SELECT COUNT(*) FROM mandates where is_paid = TRUE AND olympiad_zone_id = " . $id . ") as verified"),
                    DB::raw("(SELECT COUNT(sex) FROM mandates WHERE is_paid = TRUE AND is_came = true AND sex = true AND olympiad_zone_id = " . $id . ") as male"),
                    DB::raw("(SELECT COUNT(sex) FROM mandates WHERE is_paid = TRUE AND is_came = true AND sex = false AND olympiad_zone_id = " . $id . ") as female"),
                    DB::raw("(SELECT COUNT(is_came) FROM mandates WHERE is_paid = TRUE AND is_came = true AND olympiad_zone_id = " . $id . ") as came"),
                    DB::raw("(SELECT COUNT(is_came) FROM mandates WHERE is_paid = TRUE AND is_came = false AND olympiad_zone_id = " . $id . ") as notCame"))
                ->groupBy('total', 'verified', 'male', 'female', 'came', 'notCame')
                ->first();
            return response()->json($stats);
        } catch (\Exception $e) {
            return $e->getMessage() . ' ' . $e->getLine();
        }
    }

    // Зооны дундаж статистик авах
    public function averageStat($id)
    {
        try {
            $stats = DB::select(
                "SELECT avg(total) as average, min(total) as minimum, max(total) as maximum,
                (SELECT avg(total) FROM (SELECT user_id, olympiad_zone_id, total FROM
                (SELECT ma.user_id, ma.olympiad_zone_id, sum(ma.score) as total FROM marks as ma
                INNER JOIN mandates ON ma.user_id = mandates.user_id
                WHERE ma.olympiad_zone_id = " . $id . " AND mandates.sex = 0
                GROUP BY ma.user_id) as tmpf) as tmpff) as female,
                (SELECT avg(total) FROM (SELECT user_id, olympiad_zone_id, total FROM
                (SELECT ma.user_id, ma.olympiad_zone_id, sum(ma.score) as total FROM marks as ma
                INNER JOIN mandates ON ma.user_id = mandates.user_id
                WHERE ma.olympiad_zone_id = " . $id . " AND mandates.sex = 1
                GROUP BY ma.user_id) as tmpf) as tmpff) as male FROM
                (SELECT user_id, olympiad_zone_id, total FROM
                (SELECT m.user_id, m.olympiad_zone_id, sum(m.score) as total FROM marks as m
                WHERE m.olympiad_zone_id = " . $id . " GROUP BY m.user_id) as tmp
                GROUP BY user_id) as tmpp");
            return response()->json($stats[0]);
        } catch (\Exception $e) {
            return $e->getMessage() . ' ' . $e->getLine();
        }
    }
}
