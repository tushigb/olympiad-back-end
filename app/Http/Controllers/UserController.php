<?php

namespace App\Http\Controllers;

use App\CityProvince;
use App\Mandate;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests;
use EllipseSynergie\ApiResponse\Contracts\Response;
use App\Transformer\UserTransformer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Mockery\Exception;
use Validator;

class UserController extends Controller
{
    public $successStatus = 200;
    protected $response;
    protected $user;

    public function __construct(Response $response,
                                User $user)
    {
        $this->response = $response;
        $this->user = $user;
    }

    // Бүх хэрэглэгч авах
    public function getAll($first, $second, $role_id, $per_page)
    {
        if (Auth::user()->role_id == 1) {
            ($first == 1) ? $first = true : $first = false;
            ($second == 1) ? $second = true : $second = false;
            if ($per_page > 50) $per_page = 50;
            if ($role_id == 'all')
                $users = User::whereIn('is_verified', [$first, $second])
                    ->paginate($per_page);
            else
                $users = User::whereIn('is_verified', [$first, $second])
                    ->where('role_id', '=', $role_id)
                    ->paginate($per_page);
            return $this->response->withPaginator($users, new UserTransformer());
        }
    }

    public function getOrganizers()
    {
        $companies = DB::table('users')
            ->where('role_id', '=', 2)
            ->where('is_verified', '=', true)
            ->whereNull('last_name')
            ->select('id', 'first_name', 'image_path', 'mobile_phone', 'web_site')
            ->get();
        $people = DB::table('users')
            ->where('role_id', '=', 2)
            ->where('is_verified', '=', true)
            ->select('id', 'first_name', 'image_path', 'mobile_phone', 'web_site')
            ->whereNotNull('last_name')
            ->get();

        $data = array(
            'companies' => $companies,
            'people' => $people
        );
        return view('about', $data);
    }

    // Нэгийг авах
    public function getOne($id)
    {
        $user = Role::find($id);
        if (!$user) {
            return $this->response->errorNotFound('User Not Found');
        }
        return $this->response->withItem($user, new  UserTransformer());
    }

    // Устгах
    public function remove($id)
    {
        try {
            $user = Role::find($id); //Роль авах
            if (!$user) {
                return $this->response->errorNotFound('User Not Found');
            }
            if ($user->delete()) {
                return $this->response->withItem($user, new  UserTransformer());
            } else {
                return $this->response->errorInternalError('Could not delete a user');
            }
        } catch (\Exception $e) {
            return $this->response->errorInternalError($e);
        }
    }

    // Бүртгүүлэх
    public function save(Request $request)
    {
//        if ($validator->fails()) {
//            return response()->json([
//                'register_number.required' => 'Регистерийн дугаар заавал бөгөлсөн байх шаардлагатай!',
//                'first_name.required' => 'Нэр заавал бөгөлсөн байх шаардлагатай!',
//                'email.required' => 'И-Мэйл заавал бөгөлсөн байх шаардлагатай!',
//                'mobile_phone.required' => 'Гар утас заавал бөгөлсөн байх шаардлагатай!',
//                'hurry_phone.required' => 'Яаралтай үед холбогдох дугаар заавал бөгөлсөн байх шаардлагатай!',
//                'image_path.required' => 'Зураг хоосон байж болохгүй!',
//                'password.required' => 'Нууц үг заавал бөгөлсөн байх шаардлагатай!',
//                'c_password.required' => 'Нууц үг заавал бөгөлсөн байх шаардлагатай!',
//                'role_id.required' => 'Роль заавал бөгөлсөн байх шаардлагатай!',
//                'ad_city_province_id.required' => 'Хот аймаг заавал сонгосон байх шаардлагатай!',
//                'ad_sum_district_id.required' => 'Сум дүүрэг заавал сонгосон байх шаардлагатай!',
//                'ad_team_section.required' => 'Баг хороо заавал сонгосон байх шаардлагатай!',
//                'ad_apart_room.required' => 'Гэрийн хаяг заавал бөгөлсөн байх шаардлагатай!'
//            ], 401);

////            return [
////                'register_number.required' => 'Регистерийн дугаар заавал бөгөлсөн байх шаардлагатай!',
////                'first_name.required' => 'Нэр заавал бөгөлсөн байх шаардлагатай!',
////                'email.required' => 'И-Мэйл заавал бөгөлсөн байх шаардлагатай!',
////                'mobile_phone.required' => 'Гар утас заавал бөгөлсөн байх шаардлагатай!',
////                'hurry_phone.required' => 'Яаралтай үед холбогдох дугаар заавал бөгөлсөн байх шаардлагатай!',
////                'image_path.required' => 'Зураг хоосон байж болохгүй!',
////                'password.required' => 'Нууц үг заавал бөгөлсөн байх шаардлагатай!',
////                'c_password.required' => 'Нууц үг заавал бөгөлсөн байх шаардлагатай!',
////                'role_id.required' => 'Роль заавал бөгөлсөн байх шаардлагатай!',
////                'ad_city_province_id.required' => 'Хот аймаг заавал сонгосон байх шаардлагатай!',
////                'ad_sum_district_id.required' => 'Сум дүүрэг заавал сонгосон байх шаардлагатай!',
////                'ad_team_section.required' => 'Баг хороо заавал сонгосон байх шаардлагатай!',
////                'ad_apart_room.required' => 'Гэрийн хаяг заавал бөгөлсөн байх шаардлагатай!'
////            ];
//        }

        if ($request->isMethod('put')) {
            $validator = Validator::make($request->all(), [
                'register_number' => 'required',
                'first_name' => 'required',
                'email' => 'required|email',
                'mobile_phone' => 'required',
                'hurry_phone' => 'required',
                'image_path' => 'required',
                'role_id' => 'required',
                'ad_city_province_id' => 'required',
                'ad_sum_district_id' => 'required',
                'ad_team_section' => 'required',
                'ad_apart_room' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json($validator->errors(), 401);
            }
            if (Auth::check()) {
                $inputs = $request->all();
                $this->user = Auth::user();
                if ($this->user->update($inputs)) {
                    return response()->json(['user' => Auth::user()], 200);
                }
            }
        } else {
            $validator = Validator::make($request->all(), [
                'register_number' => 'required',
                'first_name' => 'required',
                'email' => 'required|email',
                'mobile_phone' => 'required',
                'hurry_phone' => 'required',
                'image_path' => 'required',
                'password' => 'required',
                'c_password' => 'required|same:password',
                'role_id' => 'required',
                'ad_city_province_id' => 'required',
                'ad_sum_district_id' => 'required',
                'ad_team_section' => 'required',
                'ad_apart_room' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json($validator->errors(), 401);
            }
            $user = User::where("email", $request->email)->first();
            if ($user)
                return response()->json(['responseMessage' => 'И-Мэйл бүртгэлтэй байна!'], 401);

            $user = User::where('register_number', $request->register_number)->first();
            if ($user)
                return response()->json(['responseMessage' => 'Регистерийн дугаар бүртгэлтэй байна!'], 401);

            try {
                $inputs = $request->all();
                $inputs['password'] = bcrypt($inputs['password']);
                $inputs['is_verified'] = false;
                $this->user->create($inputs);
                return response()->json(['responseMessage' => 'User registered']);
            } catch
            (\Exception $e) {
                return $this->response->errorInternalError($e->getMessage());
            }
        }
    }

    // Төлөв солих
    public function setStatus($id, $status)
    {
        if (Auth::user()->role_id == 1) {
            try {
                ($status == 1) ? $status = true : $status = false;
                $this->user = User::find($id);
                $this->user->is_verified = $status;
                if ($this->user->update())
                    return response()->json(['responseMessage' => 'success'], 200);
                else return response()->json(['responseMessage' => 'error'], 401);
            } catch (\Exception $e) {
                return $this->response->errorInternalError($e->getMessage());
            }
        }
    }

    public function delete($id)
    {
        try {
            if (Auth::user()->role_id == 1) {
                $this->user = User::find($id);
                Storage::delete($this->user->image_path);
                if ($this->user->delete()) {
                    return response()->json(['message' => 'Амжилттай устлаа'], 200);
                } else return response()->json(['message' => 'Алдаа гарлаа'], 401);
            }
        } catch (\Exception $e) {
            return $this->response->errorInternalError($e->getMessage());
        }
    }

    // Нууц үг солих
    public function changePassword(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'current_password' => 'required',
                'password' => 'required',
                'c_password' => 'required|same:password',
            ]);

            if ($validator->fails()) {
                return response()->json(['error' => $validator->errors()], 401);
            }

            $this->user = Auth::user();
            if ($this->user) {
                if (Hash::check($request->current_password, $this->user->password)) {
                    $input = $request->all();
                    $input['password'] = bcrypt($input['password']);
                    $this->user->password = $input['password'];
                    $this->user->update();
                    return response()->json(['responseMessage' => 'password updated'], 200);
                } else {
                    return response()->json(['responseMessage' => 'current password wrong'], 401);
                }
            } else {
                return response()->json(['responseMessage' => 'user not found'], 401);
            }

        } catch (\Exception $e) {
            return $this->response->errorInternalError($e->getMessage());
        }
    }

    // Token устгах
    public function deleteToken(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'email' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json($validator->errors(), 401);
            }

            $user = User::where("email", $request->email)->first();
            DB::table('oauth_access_tokens')->where('user_id', '=', $user->id)->delete();
            return response()->json(['responseMessage' => 'token deleted']);
        } catch (\Exception $e) {
            return $this->response->errorInternalError($e->getMessage());
        }
    }

    // Зохион байгуулагчийн профайл
    public function getOrganizationProfile($id)
    {
        $user = User::where('id', '=', $id)
            ->first();

        if ($user->role_id == 2) {
            $olympiads = DB::table('olympiads')
                ->join('users', 'users.id', '=', 'olympiads.user_id')
                ->join('olympiad_details', 'olympiad_details.olympiad_id', '=', 'olympiads.id')
                ->join('lessons', 'lessons.id', '=', 'olympiad_details.lesson_id')
                ->select('olympiads.id', 'olympiads.title', 'olympiads.logo', 'olympiads.cover',
                    'users.first_name', 'olympiad_details.register_end_date',
                    'lessons.name as lesson_name')
                ->where('olympiads.user_id', '=', $id)
                ->get();
            $data = array(
                'olympiads' => $olympiads,
                'user' => $user,
            );
            return view('organization_profile', $data);
        } else {
            return view('error');
        }
    }

    // Хэрэглэгчийн профайл
    public function getParticipantProfile($id)
    {
        $user = User::join('schools', 'schools.id', '=', 'users.school_id')
            ->select('users.first_name', 'users.class', 'users.class_group', 'schools.name',
                'users.image_path')
            ->where('users.id', '=', $id)
            ->first();

        if ($user) {
            if ($user->role_id != 1 && $user->role_id != 2) {

//                $olympiads = DB::table('mandates as m')
//                    ->join('olympiads as o', 'o.id', '=', 'm.olympiad_id')
//                    ->join('olympiad_details as od', 'od.id', '=', 'm.olympiad_detail_id')
//                    ->join('lessons as l', 'l.id', '=', 'od.lesson_id')
//                    ->join('users as u', 'm.user_id', '=', 'u.id')
//                    ->select('o.id', 'o.title', 'o.logo', 'o.cover',
//                        'u.first_name', 'od.register_end_date', 'l.name as lesson_name')
//                    ->groupBy('id', 'register_end_date', 'lesson_name')
//                    ->where('m.user_id', '=', $id)
//                    ->get();

                $olympiads = DB::select("
select o.id, o.title, o.logo, o.cover, u.first_name, od.register_end_date, l.name as lesson_name from mandates as m
inner join olympiads as o on m.olympiad_id = o.id
  inner join olympiad_details od on m.olympiad_detail_id = od.id
  inner join users as u on m.user_id = u.id
  inner join lessons l on od.lesson_id = l.id
where m.user_id = " . $id . "
group by o.id, register_end_date, lesson_name");

                $data = array(
                    'user' => $user,
                    'olympiads' => $olympiads,
                );
                return view('participant_profile', $data);
            } else {
                return view('error');
            }
        } else {
            return view('error');
        }
    }

    public function getSimpleStatistics()
    {
        if (Auth::user()->role_id == 1) {
            try {
                $statistics = DB::table('users')
                    ->select(
                        DB::raw("(SELECT COUNT(*) FROM users) as total_users"),
                        DB::raw("(SELECT COUNT(*) FROM users where is_verified = TRUE) as verified"),
                        DB::raw("(SELECT COUNT(*) FROM users where role_id = 2 AND is_verified = TRUE) as organizer"),
                        DB::raw("(SELECT COUNT(*) FROM users where role_id = 3 AND is_verified = TRUE) as teacher"),
                        DB::raw("(SELECT COUNT(*) FROM users where role_id = 4 AND is_verified = TRUE) as student"),
                        DB::raw("(SELECT COUNT(*) FROM users where role_id = 5 AND is_verified = TRUE) as pupil"),
                        DB::raw("(SELECT COUNT(*) FROM users where created_at >= (CURDATE() - INTERVAL 7 DAY)) as new_users"),
                        DB::raw("(SELECT COUNT(*) FROM olympiads) as total_olympiads"),
                        DB::raw("(SELECT COUNT(*) FROM olympiads where status_id = 1) as not_verified_olympiads"),
                        DB::raw("(SELECT COUNT(*) FROM olympiads where status_id = 2) as verified_olympiads"),
                        DB::raw("(SELECT COUNT(*) FROM olympiads where status_id = 3) as ended_olympiads"),
                        DB::raw("(SELECT COUNT(*) FROM olympiads where status_id = 4) as aborted_olympiads"),
                        DB::raw("(SELECT COUNT(*) FROM olympiads where status_id = 5) as canceled_olympiads")
                    )->groupBy('total_users')
                    ->first();
                return response()->json($statistics);
//                $stats = DB::table('mandates')
//                    ->select(
//                        DB::raw("(SELECT COUNT(*) FROM mandates where olympiad_id = " . $id . ") as total"),
//                        DB::raw("(SELECT COUNT(*) FROM mandates where is_paid = TRUE AND olympiad_id = " . $id . ") as verified"),
//                        DB::raw("(SELECT COUNT(sex) FROM mandates WHERE is_paid = TRUE AND is_came = true AND sex = true AND olympiad_id = " . $id . ") as male"),
//                        DB::raw("(SELECT COUNT(sex) FROM mandates WHERE is_paid = TRUE AND is_came = true AND sex = false AND olympiad_id = " . $id . ") as female"),
//                        DB::raw("(SELECT COUNT(is_came) FROM mandates WHERE is_paid = TRUE AND is_came = true AND olympiad_id = " . $id . ") as came"),
//                        DB::raw("(SELECT COUNT(is_came) FROM mandates WHERE is_paid = TRUE AND is_came = false AND olympiad_id = " . $id . ") as notCame"))
//                    ->groupBy('total', 'verified', 'male', 'female', 'came', 'notCame')
//                    ->first();
            } catch (\Exception $e) {
                return $this->response->errorInternalError($e->getMessage());
            }
        }
    }
}