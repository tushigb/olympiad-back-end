<?php

namespace App\Http\Controllers;

use App\Invoice;
use App\Transformer\FileTransformer;
//use EllipseSynergie\ApiResponse\Contracts\Response;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use File;
use Response;

class FileController extends Controller
{
    protected $response;
    protected $file;
    protected $invoice;

    public function __construct(\EllipseSynergie\ApiResponse\Contracts\Response $response,
                                \App\File $file,
                                Invoice $invoice)
    {
        $this->response = $response;
        $this->file = $file;
        $this->invoice = $invoice;
    }

    //Q-Pay QR Code generate хийж байгаа - Нэхэмжлэл үүсгэх
    public function generateQPay($file)
    {
        $client = new Client();
        $auth = base64_encode('qpay_tech_avdar:kGmbVU4C');
        $url = 'http://43.231.112.201:8080/WebServiceQPayMerchant.asmx/qPay_genInvoiceSimple';
        $invoiceNumber = 'f' . md5(Carbon::now()) . 'f' . $file->id . 'f' . Auth::id();
        $data = '
        {
            "type": "4",
            "merchant_code": "TECH_AVDAR",
            "merchant_verification_code": "Sh9Ug2EZd78GjekP",
            "merchant_customer_code": "85032613",
            "json_data": {
                "invoice_code": "TECH_AVDAR_INVOICE",
                "merchant_branch_code": "1",
                "merchant_invoice_number": ' . '"' . $invoiceNumber . '"' . ',
                "invoice_date": ' . '"' . Carbon::now() . ".000" . '"' . ',
                "invoice_description": ' . '"' . Auth::user()->register_number . '"' . ',
                "invoice_total_amount": ' . '"' . $file->price . '"' . ',
                "item_btuk_code": "5654454",
                "vat_flag": "0"
            }
        }';
        $json = json_decode($data, true);
        try {
            $response = $client->post($url,
                [
                    'headers' => [
                        'Authorization' => 'Basic ' . $auth,
                        'Content-Type' => 'application/json'
                    ],
                    'json' => $json
                ]);
            $response = json_decode($response->getBody(), true);
            $response['merchant_invoice_number'] = $invoiceNumber;
            return $response;
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function getPaymentMethod($file_id)
    {
        $file = \App\File::find($file_id);
        $invoice = json_decode(Invoice::where('file_id', '=', $file_id)
            ->where('user_id', '=', Auth::id())
            ->select('invoice_json')
            ->first(), true)['invoice_json'];
        if ($invoice) {
            $qr_image = json_decode($invoice, true)['json_data']['qPay_QRimage'];
            return response()->json($qr_image);
        } else {
            $qpay_json = $this->generateQPay($file);
            $inputs['invoice_id'] = $qpay_json['json_data']['invoice_id'];
            $inputs['user_id'] = Auth::id();
            $inputs['file_id'] = $file->id;
            $inputs['invoice_json'] = json_encode($qpay_json);
            $inputs['is_paid'] = false;
            $invoice = $this->invoice->create($inputs);
            if ($invoice) {
                $qr_image = json_decode($invoice->invoice_json, true)['json_data']['qPay_QRimage'];
            }
            return response()->json($qr_image);
        }
    }

    public function getAll()
    {
        try {
            $files = \App\File::orderBy('id', 'desc')
                ->paginate(15);
            return $this->response->withPaginator($files, new FileTransformer());
        } catch (\Exception $e) {
            return $this->response->errorInternalError($e);
        }
    }

    // Хэрэглэгчийн id-аас шалтгаалж авах
    public function getAllByUserId()
    {
        try {
            if (Auth::user()->role_id == 2) {
                $files = \App\File::where('user_id', '=', Auth::id())
                    ->orderBy('id', 'desc')
                    ->paginate(15);
            } else if (Auth::user()->role_id == 1) {
                $files = \App\File::orderBy('id', 'desc')
                    ->paginate(15);
            }
            return $this->response->withPaginator($files, new FileTransformer());
        } catch (\Exception $e) {
            return $this->response->errorInternalError($e);
        }
    }

    // Хадгалах, шинэчлэх
    public function save(Request $request)
    {
        try {
            $inputs = $request->all();
            if (isset($inputs['id'])) {

            } else {
                $inputs['user_id'] = Auth::id();
                $file = $this->file->create($inputs);
                if ($file) {
                    return response()->json($file);
                }
            }
        } catch (\Exception $e) {
            return $this->response->errorInternalError($e);
        }
    }

    // Устгах
    public function remove()
    {

    }

    // Файл харах - PDF-ээр
    public function getFile($id)
    {
        $file = \App\File::find($id);
        if ($file) {
            if ($file->user_id == Auth::id() || Auth::user()->role_id == 1) {
                $path = storage_path('app/' . $file->path);

                $file = File::get($path);
                $type = File::mimeType($path);

                $response = Response::make($file, 200);
                $response->header("Content-Type", $type);
                return $response;
            }
        }
    }
}
