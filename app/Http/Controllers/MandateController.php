<?php

namespace App\Http\Controllers;

use App\Invoice;
use App\Mail\MandateShipped;
use App\Mandate;
use App\Mark;
use App\Olympiad;
use App\OlympiadDetail;
use App\OlympiadZone;
use App\Problem;
use App\Transformer\MandateTransformer;
use App\User;
use App\Zone;
use Carbon\Carbon;
use EllipseSynergie\ApiResponse\Contracts\Response;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Validation\Rules\In;

class MandateController extends Controller
{
    protected $response;
    protected $mandate;
    protected $mark;
    protected $invoice;

    public function __construct(Response $response, Mandate $mandate, Mark $mark,
                                Invoice $invoice)
    {
        $this->response = $response;
        $this->mandate = $mandate;
        $this->mark = $mark;
        $this->invoice = $invoice;
    }

    private function checkGender($zone, $user)
    {
        if ($zone->gender_id == 27) {
            return true;
        } else if ($zone->gender_id == 28 && $user->sex == true) { //Эр
            return true;
        } else if ($zone->gender_id == 29 && $user->sex == false) { //Эм
            return true;
        } else return false;
    }

    private function checkClass($min, $max, $user)
    {
        if ($min == $max) {
            if ($user->class == $min)
                return true;
            else return false;
        } else {
            if ($user->class >= $min && $user->class <= $max)
                return true;
            else return false;
        }
    }

    private function checkAge($min, $max, $user)
    {
        $from = new \DateTime($user->dob);
        $to = new \DateTime('today');
        $age = $from->diff($to)->y;

        if ($min == $max) {
            if ($age == $min)
                return true;
            else return false;
        } else {
            if ($age >= $min && $age <= $max)
                return true;
            else return false;
        }
    }

    //Q-Pay QR Code generate хийж байгаа - Нэхэмжлэл үүсгэх
    public function generateQPay($mandate, $olympiad)
    {
        $client = new Client();
        $auth = base64_encode('qpay_tech_avdar:kGmbVU4C');
        $url = 'http://43.231.112.201:8080/WebServiceQPayMerchant.asmx/qPay_genInvoiceSimple';
//        $url = 'https://avdar.sus.ebs.mn/olympiad/qpay/generate_invoice';
        $invoiceNumber = 'm' . md5(Carbon::now()) . 'm' . $mandate->id . 'm' . Auth::id();
//        ' . '"' . "мандат" . $mandate->id . " " . Carbon::now() . '"' . '
        $data = '
        {
            "type": "4",
            "merchant_code": "TECH_AVDAR",
            "merchant_verification_code": "Sh9Ug2EZd78GjekP",
            "merchant_customer_code": "85032613",
            "json_data": {
                "invoice_code": "TECH_AVDAR_INVOICE",
                "merchant_branch_code": "1",
                "merchant_invoice_number": ' . '"' . $invoiceNumber . '"' . ',
                "invoice_date": ' . '"' . Carbon::now() . ".000" . '"' . ',
                "invoice_description": ' . '"' . $mandate->register_number . '"' . ',
                "invoice_total_amount": ' . '"' . $olympiad->mandate_cost . '"' . ',
                "item_btuk_code": "5654454",
                "vat_flag": "0"
            }
        }';
        $json = json_decode($data, true);
        try {
            $response = $client->post($url,
                [
                    'headers' => [
                        'Authorization' => 'Basic ' . $auth,
                        'Content-Type' => 'application/json'
                    ],
                    'json' => $json
                ]
            );
            $response = json_decode($response->getBody(), true);
            $response['merchant_invoice_number'] = $invoiceNumber;
//            return json_decode($response->getBody(), true);
//            return json_decode($response);
            return $response;
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function sendMandateViaEmail($id)
    {
        $mandate = DB::table('mandates as m')
            ->join('olympiad_zones as o', 'm.olympiad_zone_id', '=', 'o.id')
            ->join('olympiads as ol', 'm.olympiad_id', '=', 'ol.id')
            ->join('olympiad_details as od', 'm.olympiad_detail_id', '=', 'od.id')
            ->join('zones as z', 'z.id', '=', 'o.min_id')
            ->join('zones as z1', 'z1.id', '=', 'o.max_id')
            ->join('zone_types as zt', 'o.zone_type_id', '=', 'zt.id')
            ->join('schools as s', 'm.school_id', '=', 's.id')
            ->select('m.id', 'm.first_name', 'm.last_name', 'm.sex', 'm.class', 'm.class_group', 'm.image_path',
                'm.email', 'ol.logo', 'ol.title', 'ol.address', 'od.start_date as olympiad_start_date',
                'o.start_date as zone_start_date', 'z.name as min',
                'z1.name as max', 'o.zone_type_id', 'zt.name as zone_type_name', 's.name as school_name')
            ->where('m.id', '=', $id)
            ->first();

        $pdf = App::make('dompdf.wrapper');
        $pdf->loadView('mandate_mail_pdf', ['mandate' => $mandate]);
        Mail::to($mandate->email)
            ->send(new MandateShipped($pdf->output(), $mandate));
        if (Mail::failures())
            return response()->json(['responseMessage' => 'Error'], 500);
        else return response()->json(['responseMessage' => 'Mail sent'], 200);
    }

    //Мандат хадгалах, шинэчлэх
    public function save(Request $request, $id)
    {
        try {
            if (Auth::check()) {
                $user = Auth::user();
                $inputs = json_decode(json_encode($user), true);
                $inputs['olympiad_id'] = $id;
                $olympiadDetail = OlympiadDetail::where('olympiad_id', '=', $id)->first();
                $inputs['olympiad_detail_id'] = $olympiadDetail->id;
                $inputs['is_came'] = false;
                $inputs['is_qr'] = false;

                if (Carbon::now() <= $olympiadDetail->register_end_date) {
                    if ($olympiadDetail->mandate_cost > 0) {
                        $inputs['is_paid'] = false;
                        $inputs['payment'] = $olympiadDetail->mandate_cost;
                    } else {
                        $inputs['is_paid'] = true;
                        $inputs['payment'] = $olympiadDetail->mandate_cost;
                    }

                    $inputs['user_id'] = Auth::id();
                    $inputs['olympiad_zone_id'] = $request->input('zone_id');

                    $zone = DB::table('olympiad_zones')
                        ->where('id', '=', $request->input('zone_id'))
                        ->select('participant_limit',
                            DB::raw("(SELECT COUNT(*) FROM mandates where olympiad_zone_id = " .
                                $request->input('zone_id') . ") as participant_count"))
                        ->first();

                    if ($zone->participant_count < $zone->participant_limit) {
                        $mandate = Mandate::where('olympiad_id', '=', $id)
                            ->where('user_id', '=', Auth::id())
                            ->where('olympiad_zone_id', '=', $request->input('zone_id'))
                            ->first();
                        if (!$mandate)
                            if ($mandate = $this->mandate->create($inputs)) {
                                if ($olympiadDetail->mandate_cost > 0) {
                                    $qpay_json = $this->generateQPay($mandate, $olympiadDetail);
                                    $inputs['invoice_id'] = $qpay_json['json_data']['invoice_id'];
                                    $inputs['user_id'] = Auth::id();
                                    $inputs['mandate_id'] = $mandate->id;
                                    $inputs['invoice_json'] = json_encode($qpay_json);
                                    $inputs['is_paid'] = false;
                                    if ($this->invoice->create($inputs)) {
                                        return redirect('olympiad/' . $id)
                                            ->with('success', 'Амжилттай бүртгэгдлээ!');
                                    }
                                } else {
//                                    $this->sendMandateViaEmail($mandate->id);
                                }
                                return redirect('olympiad/' . $id)
                                    ->with('success', 'Амжилттай бүртгэгдлээ!');
                            } else return redirect('olympiad/' . $id)
                                ->with('error', 'Бүртгэл амжилтгүй боллоо!');
                        else {
                            return redirect('olympiad/' . $id)
                                ->with('error', 'Энэ зоонд аль хэдийн бүртгэгдсэн байна!');
                        }
                    } else return redirect('olympiad/' . $id)
                        ->with('error', 'Олимпиадад оролцогчдийн тоо бүрдсэн байна!');

//                $request = false;
//                foreach ($zones as $zone) {
//                    $min = Zone::where('id', '=', $zone->min_id)->first();
//                    $max = Zone::where('id', '=', $zone->max_id)->first();
//                    $min = intval($min->name);
//                    $max = intval($max->name);
//
//                    switch ($zone->zone_type_id) {
//                        case 1: // анги
//                            if ($this->checkGender($zone, $user) && $this->checkClass($min, $max, $user)) {
//                                $inputs['olympiad_zone_id'] = $zone->id;
//                                $this->mandate->create($inputs);
//                                $request = true;
//                                break 2;
//                            }
//                            break 1;
//                        case 2: // нас
//                            if ($this->checkGender($zone, $user) && $this->checkAge($min, $max, $user)) {
//                                $inputs['olympiad_zone_id'] = $zone->id;
//                                $this->mandate->create($inputs);
//                                $request = true;
//                                break 2;
//                            }
//                            break 1;
//                    }
//                }
//                if ($request)
//                    return redirect('olympiad/' . $id)
//                        ->with('success', 'Амжилттай бүртгэгдлээ!');
//                else {
//                    return redirect('olympiad/' . $id)
//                        ->with('error', 'Бүртгэл амжилтгүй боллоо!');
//                }
                } else {
                    return redirect('olympiad/' . $id)
                        ->with('error', 'Уучлаарай бүртгэлийн хугацаа хэтэрсэн байна!');
                }


//                if ($request->has('mandate_id')) {
//                    $this->mandate = Mandate::find($request->input('mandate_id'));
//                    $this->mandate->olympiad_zone_id = $request->input('zone_id');
//                    if ($this->mandate->update())
//                        return redirect('olympiad/' . $id)
//                            ->with('success', 'Амжилттай шинэчлэгдлээ!');
//                    else return redirect('olympiad/' . $id)
//                        ->with('error', 'Шинэчлэлт амжилтгүй боллоо!');
//                } else {
//
//                }
            }
        } catch
        (\Exception $e) {
            return $e->getMessage() . ' ' . $e->getLine();
        }
    }

    public function delete(Request $request)
    {
        $mandate = Mandate::find($request->input('mandate_id'));
        Invoice::where('mandate_id', '=', $request->input('mandate_id'))->delete();
        $mandate->delete();
        return redirect('olympiad/' . $mandate->olympiad_id);
    }

    // Олимпиадын зооны оролцогчдыг авах
    public function getMandatesByZoneId($id, $sortKey, $type, $sort, $both = null)
    {
        if (Auth::check()) {
            try {
                $mandates = Mandate::where('olympiad_zone_id', '=', $id)
                    ->whereIn('is_came', [$sort, $both])
//                    ->where('is_came', '=', false)
                    ->orderBy($sortKey, $type)
                    ->paginate(15);
                return $this->response->withPaginator($mandates, new MandateTransformer());
            } catch
            (\Exception $e) {
                return $e->getMessage() . ' ' . $e->getLine();
            }
        }
    }

    // Хайлт хийх
    public function searchMandates($id, $sortKey, $value = null, $sort, $both = null)
    {
        try {
            $mandates = DB::table('mandates')->where([
                ['olympiad_zone_id', '=', $id],
                [$sortKey, 'like', $value . '%'],])
                ->whereIn('is_came', [$sort, $both])
                ->paginate(15);
            return $this->response->withPaginator($mandates, new MandateTransformer());
        } catch (\Exception $e) {
            return $e->getMessage() . ' ' . $e->getLine();
        }
    }

    // Ирц хадгалах
    public function saveAttendance(Request $request)
    {
        if (Auth::check()) {
            try {
                $mandates = $request->all();
                foreach ($mandates as $mandate) {
                    $this->mandate = Mandate::find($mandate['mandate_id']);
                    if (!$this->mandate->is_qr)
                        $this->mandate->is_came = $mandate['is_came'];
                    else $this->mandate->is_came = true;
                    $this->mandate->update();
                    if ($this->mandate->is_came == false) {
                        Mark::where('mandate_id', '=', $this->mandate->id)
                            ->delete();
                    } else {
                        $problems = Problem::where('olympiad_zone_id', '=', $this->mandate->olympiad_zone_id)
                            ->get();
                        foreach ($problems as $problem) {
                            $inputs = json_decode(json_encode($this->mandate), true);
                            $inputs['mandate_id'] = $this->mandate->id;
                            $inputs['problem_id'] = $problem->id;
                            $inputs['score'] = 0;
                            $this->mark->create($inputs);
                        }
                    }
                }
                return response()->json(['responseMessage' => 'success'], 200);
//                return response()->json($mandate->mandate_id);
            } catch (\Exception $e) {
                return $e->getMessage() . ' ' . $e->getLine();
            }
        }
    }

    // Дүн хадгалах
    public function saveMark(Request $request)
    {
        if (Auth::check()) {
            try {
                $marks = $request->all();
                $savedMarks = [];
                $zone = OlympiadZone::find($marks[0]['olympiad_zone_id']);
                if ($zone->status_id == 2) {
                    foreach ($marks as $mark) {
                        if (isset($mark['id'])) {
                            $this->mark = Mark::find($mark['id']);
                            $this->mark->score = $mark['score'];
                            $this->mark->update();
                            array_push($savedMarks, $this->mark);
                        } else {
                            $inputs['mark'] = $mark;
                            array_push($savedMarks, $this->mark->create($mark));
                        }
                    }
                    return response()->json($savedMarks);
                } else {
                    return response()->json(['responseMessage' => 'Ирц баталгаажуулснаар дүн бүртгэх боломжтой!'], 403);
                }
            } catch (\Exception $e) {
                return $e->getMessage() . ' ' . $e->getLine();
            }
        }
    }

    // Хуурамч төлбөрийн хэсэг
    public function payment(Request $request)
    {
        try {
            $this->mandate = Mandate::where('register_number', '=', $request->input('register_number'))
                ->where('is_paid', '=', false)
                ->first();
            if ($this->mandate) {
                $this->mandate->payment = $this->mandate->payment - $request->input('payment');
                if ($this->mandate->payment > 0) {
                    if ($this->mandate->update()) {
                        return response()->json(['balance' => $this->mandate->payment], 200);
                    }
                } else {
                    $this->mandate->is_paid = true;
                    if ($this->mandate->update()) {
                        return response()->json(['balance' => $this->mandate->payment,
                            'responseMessage' => 'successful'], 200);
                    }
                }
            } else {
                return response()->json(['responseMessage' => 'asd'], 200);
            }
        } catch (\Exception $e) {
//            return $this->response->errorInternalError($e);
            return $e->getMessage() . ' ' . $e->getLine();
        }
    }

    //QR-аар ирц бүртгэх
    public function attendanceQR($id)
    {
        try {
            $this->mandate = Mandate::find($id);
            if ($this->mandate) {
                if (!$this->mandate->is_came && !$this->mandate->is_qr) {
                    $this->mandate->is_came = true;
                    $this->mandate->is_qr = true;
                    if ($this->mandate->update()) {
                        $this->mandate = DB::table('mandates as m')
                            ->join('schools as s', 'm.school_id', '=', 's.id')
                            ->select('m.id', 'm.first_name', 'm.last_name', 'm.sex', 'm.class',
                                'm.class_group', 'm.image_path', 'm.register_number', 's.name as school_name')
                            ->where('m.id', '=', $id)
                            ->first();
                        return response()->json($this->mandate, 200);
                    } else return response()->json(['responseMessage' => 'error'], 500);
                }
//                else {
//                    return response()->json(['message' => 'already checked'], 500);
//                }
            } else
                response()->json(['responseMessage' => 'mandate not found'], 404);
        } catch (\Exception $e) {
            return $e->getMessage() . ' ' . $e->getLine();
        }
    }

    // Мандатын онооны жагсаалт
    public function getMarkList($id)
    {
        $marks = DB::table('marks as ma')
            ->join('mandates as m', 'ma.mandate_id', '=', 'm.id')
            ->select('m.first_name', 'm.id', 'ma.score')
            ->get();

        $count = DB::table('problems')
            ->where('olympiad_zone_id', '=', $id)
            ->count();

        foreach ($marks as $key => $mark) {
            $participant = new \stdClass();
            $participant_marks = [];
            for ($i = 0; $i < $count; $i++) {
                $participant->id = $mark->id;
                $participant->first_name = $mark->first_name;
                array_push($participant_marks, $mark);
            }
            return response()->json($participant_marks);
        }
    }

    // Дүнгийн жагсаалт
    public function getScorePage($id)
    {
        $problems = Problem::where('olympiad_zone_id', '=', $id)
            ->select('name')
            ->get();

        $data = array(
            'problems' => $problems
        );

        return view('score', $data);
    }

    public function getScoreList($id)
    {
        $mandates = DB::table('marks as ma')
            ->join('mandates as m', 'ma.mandate_id', '=', 'm.id')
            ->join('schools as s', 'm.school_id', '=', 's.id')
            ->select('ma.mandate_id', 'm.register_number', 'm.first_name',
                'm.last_name', 'm.class', 'm.class_group', 's.name as school',
                DB::raw('SUM(score) as total'))
            ->where('ma.olympiad_zone_id', '=', $id)
            ->groupBy('ma.mandate_id')
            ->orderBy('total', 'desc')
            ->paginate(50);

        foreach ($mandates as $mandate) {
            $mandate->marks = Mark::where('mandate_id', '=', $mandate->mandate_id)
                ->select('score', 'problem_id')
                ->get();
        }

        return response()->json($mandates);
    }

    public function getSchoolAvgScoreList($id)
    {
        $schools = DB::select("select school_name, avg(total) as avg_score, count(school_name) as participant_count from
                              (select mandate_id, m2.school_id, s.name as school_name, sum(score) as total from marks as m
                              inner join mandates m2 on m.mandate_id = m2.id
                              inner join schools s on m2.school_id = s.id
                              where m.olympiad_zone_id = " . $id . "
                              group by mandate_id) as total2
                              group by school_name
                              order by avg_score desc");
        return response()->json($schools);
    }
}