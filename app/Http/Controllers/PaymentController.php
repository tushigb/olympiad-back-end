<?php

namespace App\Http\Controllers;

use App\Invoice;
use App\Mail\MandateShipped;
use App\Mandate;
use App\Payment;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class PaymentController extends Controller
{

    protected $invoice;
    protected $mandate;

    public function __construct(Invoice $invoice, Mandate $mandate)
    {
        $this->invoice = $invoice;
        $this->mandate = $mandate;
    }

    public function qPayService()
    {
        if (isset($_GET['invoice_id'])) {
            $this->invoice = Invoice::where('invoice_id', '=', $_GET['invoice_id'])
                ->first();
            if ($this->invoice) {
                $merchant_invoice_number = json_decode($this->invoice->invoice_json, true)['merchant_invoice_number'];
                $payment = $this->checkInvoicePayment($merchant_invoice_number);
                return $payment;
                $this->invoice->payment_json = json_encode($payment['json_data']['invoice_transactions']);
                if ($this->invoice->update()) {
                    $this->mandate = Mandate::find($this->invoice->mandate_id);
                    if ($this->mandate->payment > 0) {
                        $mandate_current_payment = $this->mandate->payment;
                        $mandate_current_payment -= $this->mandate->payment / 100 * 1;
                        $invoice_payment = $payment['json_data']['invoice_transactions'][count($payment['json_data']['invoice_transactions']) - 1]['amount'];
                        $mandate_current_payment -= $invoice_payment;
                        if ($mandate_current_payment == 0) {
                            $this->mandate->payment = 0;
                            $this->mandate->is_paid = true;
                            if ($this->mandate->update()) {

                                $mandate = DB::table('mandates as m')
                                    ->join('olympiad_zones as o', 'm.olympiad_zone_id', '=', 'o.id')
                                    ->join('olympiads as ol', 'm.olympiad_id', '=', 'ol.id')
                                    ->join('olympiad_details as od', 'm.olympiad_detail_id', '=', 'od.id')
                                    ->join('zones as z', 'z.id', '=', 'o.min_id')
                                    ->join('zones as z1', 'z1.id', '=', 'o.max_id')
                                    ->join('zone_types as zt', 'o.zone_type_id', '=', 'zt.id')
                                    ->join('schools as s', 'm.school_id', '=', 's.id')
                                    ->select('m.id', 'm.first_name', 'm.last_name', 'm.sex', 'm.class', 'm.class_group',
                                        'm.image_path', 'ol.logo', 'ol.title', 'ol.address',
                                        'od.start_date as olympiad_start_date', 'o.start_date as zone_start_date',
                                        'z.name as min', 'z1.name as max', 'o.zone_type_id',
                                        'zt.name as zone_type_name', 's.name as school_name')
                                    ->where('m.id', '=', $this->mandate->id)
                                    ->first();

                                $pdf = App::make('dompdf.wrapper');
                                $pdf->loadView('mandate_mail_pdf', ['mandate' => $mandate]);
                                Mail::to('tushig.0803@gmail.com')
                                    ->send(new MandateShipped($pdf->output(), $mandate));
//                            if (Mail::failures())
//                                return response()->json(['responseMessage' => 'Error'], 500);
//                            else return response()->json(['responseMessage' => 'Mail sent'], 200);
                            }
                        } else {
                            $this->mandate->payment -= $invoice_payment / 99 * 100;
                            $this->mandate->update();
//                    return $invoice_payment / 99 * 100;
                        }
                    }
                }
            } else {
                return 'Нэхэмжлэл олдсонгүй';
            }
        }
    }

    public function avdarSus()
    {
        $client = new Client();
        $url = 'https://avdar.sus.ebs.mn/olympiad/qpay/payment/3';
        try {
            $response = $client->get($url, [
                'headers' => [
                    'Content-Type' => 'application/json'
                ]
            ]);
            return $response;
        } catch (\Exception $e) {
            return $e;
        }
    }

    private function checkInvoicePayment($merchant_invoice_number)
    {
        $client = new Client();
        $auth = base64_encode('qpay_tech_avdar:kGmbVU4C');
        $url = 'http://43.231.112.201:8080/WebServiceQPayMerchant.asmx/qPay_checkInvoicePayment2';
        $data = '
        {
            "type": "2",
            "merchant_code": "TECH_AVDAR",
            "merchant_verification_code": "Sh9Ug2EZd78GjekP",
            "json_data": {
                "lang_code": "ENG",
                "invoice_code": "TECH_AVDAR_INVOICE",
                "merchant_invoice_number": ' . '"' . $merchant_invoice_number . '"' . '
            }
        }';

        $json = json_decode($data, true);
        try {
            $response = $client->post($url,
                [
                    'headers' => [
                        'Authorization' => 'Basic ' . $auth,
                        'Content-Type' => 'application/json'
                    ],
                    'json' => $json
                ]
            );
            $response = json_decode($response->getBody(), true);
            return $response;
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }


}
