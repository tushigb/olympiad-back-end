<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    protected $fillable = [
        'invoice_id', 'user_id', 'mandate_id', 'file_id',
        'invoice_json', 'is_paid'
    ];
}
