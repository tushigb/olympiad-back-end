<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class CityProvince extends Model
{
    use Notifiable;

    public function user()
    {
        return $this->hasMany('App\User');
    }

    public function sumDistrict()
    {
        return $this->hasMany('App\SumDistrict');
    }
}