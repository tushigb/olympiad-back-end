<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Content extends Model
{
    protected $fillable = [
        'title', 'description', 'speech', 'name',
        'name_description', 'image_path', 'content',
        'user_id', 'is_content',
        'edited_user_id'
    ];

    public function olympiadContentImage()
    {
        return $this->hasMany('App\OlympiadContentImage');
    }
}