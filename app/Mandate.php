<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mandate extends Model
{
    protected $fillable = [
        'is_came', 'is_qr', 'register_number', 'dob',
        'sex', 'email', 'first_name', 'last_name',
        'mobile_phone', 'hurry_phone', 'home_phone',
        'pupil_class', 'student_code', 'profession', 'image_path',
        'is_paid', 'payment', 'role_id', 'user_id', 'ad_city_province_id',
        'ad_sum_district_id', 'ad_team_section', 'ad_apart_room',
        'school_id', 'olympiad_id', 'olympiad_detail_id', 'olympiad_zone_id',
        'class', 'class_group'
    ];

//    public function mark()
//    {
//        return $this->hasMany('App\Mark');
//    }
}