<!doctype html>
<html lang="{{ app()->getLocale() }}" xmlns:v-on="http://www.w3.org/1999/xhtml"
      xmlns:v-bind="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,400,700&amp;subset=cyrillic,cyrillic-ext"
          rel="stylesheet">
    <link rel="stylesheet" href="{{asset('css/theme.css')}}" type="text/css"/>
    <link rel="stylesheet" href="{{asset('css/uikit.css')}}" type="text/css"/>
    <link href="https://unpkg.com/ionicons@4.1.0/dist/css/ionicons.min.css" rel="stylesheet">
    {{--<link rel="stylesheet" href="../../public/css/uikit.min.css" type="text/css"/>--}}
    <link rel="stylesheet" href="../../public/css/theme.css" type="text/css"/>
    <style>
        .mapouter {
            overflow: hidden;
            height: 450px;
            width: 100%;
        }

        .gmap_canvas {
            background: none !important;
            height: 450px;
            width: 100%;
        }
    </style>
    <title>Olympiad</title>
</head>
<body>
<div id="app">
    @include('.layout.navbar')
    @if($content != null)
    <div class="uk-section uk-section-muted uk-background-cover uk-padding-remove-vertical"
         style="border-style: none none solid none; border-width: 0.15mm; border-color: #f3f4f7;">
        <div class="uk-container">
            <div class="uk-text-center" uk-grid>
                <div class="uk-flex uk-flex-middle uk-text-center">
                    <div class="uk-child-width-1-2" uk-grid>
                        <div class="uk-width-expand@s">
                            <h3 class="sc-text-brand sc-text-400 uk-text-left uk-text-lead">
                                {{$content->title}}
                                {{--Шинэ Олимпиадын мэдээлэл--}}
                            </h3>
                            <p class="sc-text-default sc-text-200 uk-text-left">
                                {{$content->description}}
                                {{--“Ирээдүйн эзэд” байгалийн ухааны нэгдмэл--}}
                                {{--агуулгын--}}
                                {{--анхдугаар--}}
                                {{--олимпиад.--}}
                            </p>
                        </div>
                        <div class="uk-visible@s">
                            <p class="sc-text-default sc-text-200 uk-text-left">
                                {{--Миний бие залуучуудаа тэр дундаа дэлхийн чиг хандлагыг өөрчилж буй--}}
                                {{--технологийн салбарын залуучуудаа дэмжиж ажилладаг билээ...--}}
                                {{$content->speech}}
                            </p>
                            <p class="sc-text-default uk-text-left ">
                                <span class="sc-color-grey sc-text-400 sc-text-size-12">
                                    {{$content->name}}
                                    {{--Жадамбын Энхбаяр--}}
                                </span><br>
                                <span class="sc-color-grey sc-text-200 uk-text-meta sc-text-size-12 uk-text-uppercase">
                                    {{$content->name_description}}
                                    {{--Монгол Улсын Их Хурлын гишүүн--}}
                                </span><br>
                                <span><a class="sc-link-default sc-text-200 sc-text-size-11"
                                         href="https://www.facebook.com/enkhbayar.jadambiin/">
                                        {{--{{$settings['link']}}--}}
                                        {{--Дэлгэрэнгүй--}}
                                    </a></span>
                        </div>
                    </div>
                    <img class="uk-align-right uk-visible@l" src="/storage/{{$content->image_path}}" width="400">
                </div>
            </div>
        </div>
    </div>
    @endif

    <div style="border-style: none none solid none; border-width: 0.15mm; border-color: #f3f4f7; padding: 10px">
        <div class="uk-container">
            <div class="uk-margin-remove-bottom" uk-grid>
                <div class="uk-text-left">
                    <p><span class="sc-color-grey sc-text-200 sc-text-size-11">Цэсээ сонгоно уу</span><br>
                        <a v-on:click="changeType('olympiad')"
                           class="sc-link-brand sc-text-400"
                           v-bind:class="{'sc-link-brand-active': type == 'olympiad'}">
                            Олимпиад
                        </a>
                    </p>
                </div>
                <div class="uk-text-left">
                    <p><span class="uk-text-small uk-text-meta"></span><br>
                        <a v-on:click="changeType('content')" class="sc-link-brand sc-text-400"
                           v-bind:class="{'sc-link-brand-active': type == 'content'}">
                            Мэдээлэл
                        </a></p>
                </div>
                <div class="uk-text-left">
                    <p><span class="uk-text-small uk-text-meta"></span><br>
                        <a v-on:click="changeType('speech')" class="sc-link-brand sc-text-400"
                           v-bind:class="{'sc-link-brand-active': type == 'speech'}">
                            Ярилцлага
                        </a></p>
                </div>
                <div class="uk-text-left">
                    <p><span class="uk-text-small uk-text-meta"></span><br><a class="sc-link-brand sc-text-400">
                            Чансаа
                        </a></p>
                </div>
                <div class="uk-text-left">
                    <p><span class="uk-text-small uk-text-meta"></span><br><a class="sc-link-brand sc-text-400">
                            Түүх
                        </a></p>
                </div>
                <div class="uk-text-left">
                    <p><span class="uk-text-small uk-text-meta"></span><br><a href="/about"
                                                                              class="sc-link-brand sc-text-400">
                            Бидний тухай
                        </a></p>
                </div>
                <div class="uk-text-left">
                    <p><span class="uk-text-small uk-text-meta"></span><br><a class="sc-link-brand sc-text-400"
                                                                              href="#modal-contact" uk-toggle>
                            Холбоо барих
                        </a></p>
                </div>
            </div>
            <template v-if="is_olympiad">
                <hr>
                <div uk-grid>
                    <div class="uk-width-1-2@s">
                        <span class="sc-color-grey sc-text-200 sc-text-size-11 uk-text-meta">Байршил</span>
                        <select class="sc-text-default sc-text-200 uk-select">
                            <option>Бүгд</option>
                            <option>Улаанбаатар</option>
                        </select>
                    </div>
                    <div class="uk-width-1-2@s">
                        <span class="sc-color-grey sc-text-200 sc-text-size-11 uk-text-meta">Төрөл</span>
                        <select class="sc-text-default sc-text-200 uk-select">
                            <option>Бүгд</option>
                            <option>Математик</option>
                        </select>
                    </div>
                </div>
            </template>
        </div>
    </div>

    <div class="uk-section uk-section-muted">
        <div class="uk-container">
            <div>
                {{--@foreach($olympiads as $olympiad)--}}
                {{--Content with cover photo--}}
                {{--@if($olympiad->cover != null)--}}
                {{--<div class="uk-card uk-card-default uk-grid-collapse uk-child-width-1-2@m uk-margin"--}}
                {{--uk-grid uk-scrollspy="cls: uk-animation-slide-bottom;">--}}
                {{--<div class="uk-card-media-left">--}}
                {{--<div class="uk-background-cover uk-background-muted uk-height-medium uk-panel uk-flex uk-flex-left uk-flex-middle"--}}
                {{--style="background-image: linear-gradient(rgba(51, 63, 72, 0.6), rgba(51, 63, 72, 0.9)), url({{$olympiad->cover}});--}}
                {{--background-size: cover; padding-left: 30px; max-height: 270px; min-height: 240px">--}}
                {{--<div uk-grid>--}}
                {{--<div>--}}
                {{--<a href="/organization/{{$olympiad->user_id}}">--}}
                {{--<img src="{{$olympiad->logo}}" height="100px" width="100px">--}}
                {{--</a>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--<div>--}}
                {{--<div class="uk-card-body">--}}
                {{--<p class="sc-text-blue uk-text-meta uk-margin-remove-bottom">--}}
                {{--{{$olympiad->first_name}}--}}
                {{--</p>--}}
                {{--<div uk-grid class="uk-margin-small-bottom">--}}
                {{--<div class="uk-width-expand uk-visible@m">--}}
                {{--<h3 class="sc-text-default uk-card-title uk-margin-remove-top">--}}
                {{--{{str_limit($olympiad->title, 60)}}--}}
                {{--</h3>--}}
                {{--</div>--}}
                {{--<div class="uk-width-expand uk-hidden@m">--}}
                {{--<h3 class="uk-card-title uk-margin-remove-top">--}}
                {{--{{$olympiad->title}}--}}
                {{--</h3>--}}
                {{--</div>--}}
                {{--<div class="uk-width-1-4"></div>--}}
                {{--</div>--}}
                {{--<hr class="uk-hidden@m">--}}
                {{--<div class="uk-margin">--}}
                {{--<i class="sc-color-grey sc-text-size-13 sc-text-200 ion-ios-location uk-hidden@m"></i>--}}
                {{--<span class="sc-color-grey sc-text-size-13 sc-text-200 uk-text-meta uk-margin-right uk-hidden@m">Ulaanbaatar/Mongolia</span>--}}
                {{--<i class="sc-color-grey sc-text-size-13 sc-text-200 ion-android-time uk-hidden@m"></i>--}}
                {{--<span class="sc-color-grey sc-text-size-13 sc-text-200 uk-text-meta uk-hidden@m uk-margin-right">--}}
                {{--Хугацаа: {{date('Y-m-d', strtotime($olympiad->register_end_date))}}--}}
                {{--</span>--}}
                {{--<i class="sc-color-grey sc-text-size-13 sc-text-200 ion-edit uk-hidden@m"></i>--}}
                {{--<span class="sc-color-grey sc-text-size-13 sc-text-200 uk-text-meta uk-hidden@m">--}}
                {{--{{$olympiad->lesson_name}}--}}
                {{--</span>--}}
                {{--<hr class="uk-hidden@m">--}}
                {{--<div class="uk-hidden@m uk-margin-top">--}}
                {{--<a href="/olympiad/{{$olympiad->id}}" style="border-radius: 5px;"--}}
                {{--class="sc-text-200 uk-button-score uk-button uk-width-1-1">--}}
                {{--Дэлгэрэнгүй--}}
                {{--</a>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--<div uk-grid class="uk-flex uk-flex-middle">--}}
                {{--<div class="uk-width-expand@m">--}}
                {{--<div class="uk-visible@m">--}}
                {{--<i class="sc-color-grey sc-text-size-13 sc-text-200 ion-ios-location"></i>--}}
                {{--<span class="sc-color-grey sc-text-size-13 sc-text-200 uk-text-meta">--}}
                {{--Ulaanbaatar/Mongolia--}}
                {{--</span><br>--}}
                {{--<i class="sc-color-grey sc-text-size-13 sc-text-200 ion-android-calendar"></i>--}}
                {{--<span class="sc-color-grey sc-text-size-13 sc-text-200 uk-text-meta">--}}
                {{--Бүртгэл дуусах: {{date('Y-m-d', strtotime($olympiad->register_end_date))}}--}}
                {{--</span><br>--}}
                {{--<i class="sc-color-grey sc-text-size-13 sc-text-200 ion-compose"></i>--}}
                {{--<span class="sc-color-grey sc-text-size-13 sc-text-200 uk-text-meta">--}}
                {{--{{$olympiad->lesson_name}}--}}
                {{--</span><br>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--<div class="uk-visible@m uk-width-auto@m">--}}
                {{--<a href="/olympiad/{{$olympiad->id}}" style="border-radius: 5px;"--}}
                {{--class="sc-text-200 uk-button uk-button-score uk-width-1-1">--}}
                {{--Дэлгэрэнгүй--}}
                {{--</a>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--Content with cover photo END--}}
                {{--@else--}}
                {{--Content without cover photo--}}
                {{--<div class="uk-card uk-card-default uk-card-small uk-card-body uk-margin"--}}
                {{--uk-scrollspy="cls: uk-animation-slide-bottom;">--}}
                {{--<div class="uk-flex uk-flex-middle" uk-grid>--}}
                {{--<div class="uk-width-auto@m">--}}
                {{--@if($olympiad->logo != null)--}}
                {{--<img style="margin-left: 11.6px" class="uk-visible@m" src="{{$olympiad->logo}}"--}}
                {{--height="100px" width="100px"--}}
                {{--href="/organization/{{$olympiad->user_id}}">--}}
                {{--@else--}}
                {{--<img style="margin-left: 11.6px" class="uk-visible@m"--}}
                {{--src="{{asset('avdar_logo.png')}}"--}}
                {{--height="100px" width="100px"--}}
                {{--href="/organization/{{$olympiad->user_id}}">--}}
                {{--@endif--}}
                {{--</div>--}}
                {{--<div class="uk-width-expand@s">--}}
                {{--<p class="uk-text-meta uk-margin-remove-bottom" style="color: cornflowerblue;">--}}
                {{--{{$olympiad->first_name}}--}}
                {{--</p>--}}
                {{--<div class="uk-margin-remove-bottom">--}}
                {{--<div>--}}
                {{--<h3 class="uk-card-title uk-margin-remove-top">--}}
                {{--{{str_limit($olympiad->title, 60)}}--}}
                {{--</h3>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--<hr class="uk-hidden@m">--}}
                {{--<div class="uk-margin-remove-top">--}}
                {{--<i class="ion-ios-location"></i>--}}
                {{--<span class="uk-text-meta uk-margin-right">Ulaanbaatar/Mongolia</span>--}}
                {{--<i class="ion-ios-location"></i>--}}
                {{--<span class="uk-text-meta uk-margin-right">--}}
                {{--Бүртгэл дуусах: {{date('Y-m-d', strtotime($olympiad->register_end_date))}}--}}
                {{--</span>--}}
                {{--<i class="ion-ios-location"></i>--}}
                {{--<span class="uk-text-meta">--}}
                {{--{{$olympiad->lesson_name}}--}}
                {{--</span>--}}
                {{--</div>--}}
                {{--<hr class="uk-hidden@m">--}}
                {{--</div>--}}
                {{--<div class="uk-width-auto@m">--}}
                {{--<a href="/olympiad/{{$olympiad->id}}" style="border-radius: 5px; margin-right: 9px"--}}
                {{--class="sc-text-200 uk-button-score uk-button uk-visible@m uk-hidden@l">--}}
                {{--Дэлгэрэнгүй--}}
                {{--</a>--}}
                {{--<a href="/olympiad/{{$olympiad->id}}" style="border-radius: 5px; margin-right: 20px"--}}
                {{--class="sc-text-200 uk-button-score uk-button uk-visible@l">--}}
                {{--Дэлгэрэнгүй--}}
                {{--</a>--}}
                {{--<a href="/olympiad/{{$olympiad->id}}" style="border-radius: 5px;"--}}
                {{--class="sc-text-200 uk-button-score uk-button uk-hidden@m uk-width-1-1">--}}
                {{--Дэлгэрэнгүй--}}
                {{--</a>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--@endif--}}
                {{--Content without cover photo END--}}
                {{--@endforeach--}}
                <template v-if="olympiads">
                    <div>
                        <template v-for="olympiad in olympiads">
                            <template v-if="olympiad.cover != null">
                                <div class="uk-card uk-card-default uk-grid-collapse uk-child-width-1-2@m uk-margin"
                                     uk-grid uk-scrollspy="cls: uk-animation-slide-bottom; repeat: true;">
                                    <div class="uk-card-media-left">
                                        <div class="uk-background-cover uk-background-muted uk-height-medium uk-panel uk-flex uk-flex-left uk-flex-middle"
                                             v-bind:style="{ 'background-image': 'linear-gradient(rgba(51, 63, 72, 0.6), rgba(51, 63, 72, 0.9)),url(' + '/storage/' +  olympiad.cover + ')', 'padding-left': '30px', 'max-height': '270px', 'min-height': '240px'}"
                                             v-bind:style="'background-image: linear-gradient(rgba(51, 63, 72, 0.6), rgba(51, 63, 72, 0.9), url()); padding-left: 30px; max-height: 270px; min-height: 240px;'"
                                             style="background-image: linear-gradient(rgba(51, 63, 72, 0.6), rgba(51, 63, 72, 0.9)),  url({{asset('light.jpg')}});
                                                     background-size: cover; padding-left: 30px; max-height: 270px; min-height: 240px">
                                            <div uk-grid>
                                                <div>
                                                    <a :href="'organization/'+olympiad.user_id">
                                                        <img :src="'/storage/' + olympiad.logo" height="100px"
                                                             width="100px">
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div>
                                        <div class="uk-card-body">
                                            <p class="sc-text-blue uk-text-meta uk-margin-remove-bottom">
                                                @{{olympiad.first_name}}
                                            </p>
                                            <div uk-grid class="uk-margin-small-bottom">
                                                <div class="uk-width-expand uk-visible@m">
                                                    <h3 class="sc-text-default uk-card-title uk-margin-remove-top">
                                                        @{{ olympiad.title.substring(0, 60) }}
                                                    </h3>
                                                </div>
                                                <div class="uk-width-expand uk-hidden@m">
                                                    <h3 class="uk-card-title uk-margin-remove-top">
                                                        @{{olympiad.title}}
                                                    </h3>
                                                </div>
                                            </div>
                                            <hr class="uk-hidden@m">
                                            <div class="uk-margin">
                                                <i class="sc-color-grey sc-text-size-13 sc-text-200 icon ion-md-compass uk-hidden@m"></i>
                                                <span class="sc-color-grey sc-text-size-13 sc-text-200 uk-text-meta uk-margin-right uk-hidden@m">Ulaanbaatar/Mongolia</span>
                                                <i class="sc-color-grey sc-text-size-13 sc-text-200 icon ion-md-calendar uk-hidden@m"></i>
                                                <span class="sc-color-grey sc-text-size-13 sc-text-200 uk-text-meta uk-hidden@m uk-margin-right">
                                                    Хугацаа: @{{ olympiad.register_end_date.substring(0, 10) }}
                                                </span>
                                                <i class="sc-color-grey sc-text-size-13 sc-text-200 icon ion-md-bookmark uk-hidden@m"></i>
                                                <span class="sc-color-grey sc-text-size-13 sc-text-200 uk-text-meta uk-hidden@m">
                                                    @{{olympiad.lesson_name}}
                                                </span>
                                                <hr class="uk-hidden@m">
                                                <div class="uk-hidden@m uk-margin-top">
                                                    <a :href="'/olympiad/' + olympiad.id" style="border-radius: 5px;"
                                                       class="sc-text-200 uk-button-score uk-button uk-width-1-1">
                                                        Дэлгэрэнгүй
                                                    </a>
                                                </div>
                                            </div>
                                            <div uk-grid class="uk-flex uk-flex-middle">
                                                <div class="uk-width-expand@m">
                                                    <div class="uk-visible@m">
                                                        <i class="sc-color-grey sc-text-size-13 sc-text-200 icon ion-md-compass"></i>
                                                        <span class="sc-color-grey sc-text-size-13 sc-text-200 uk-text-meta">
                                                            Ulaanbaatar/Mongolia
                                                        </span><br>
                                                        <i class="sc-color-grey sc-text-size-13 sc-text-200 icon ion-md-calendar"></i>
                                                        <span class="sc-color-grey sc-text-size-13 sc-text-200 uk-text-meta">
                                                            Бүртгэл дуусах: @{{ olympiad.register_end_date.substring(0, 10) }}
                                                        </span><br>
                                                        <i class="sc-color-grey sc-text-size-13 sc-text-200 icon ion-md-bookmark"></i>
                                                        <span class="sc-color-grey sc-text-size-13 sc-text-200 uk-text-meta">
                                                            @{{olympiad.lesson_name}}
                                                        </span><br>
                                                    </div>
                                                </div>
                                                <div class="uk-visible@m uk-width-auto@m">
                                                    <a :href="'/olympiad/' + olympiad.id" style="border-radius: 5px;"
                                                       class="sc-text-200 uk-button uk-button-score uk-width-1-1">
                                                        Дэлгэрэнгүй
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </template>
                            <template v-if="olympiad.cover == null">
                                <div class="uk-card uk-card-default uk-card-small uk-card-body uk-margin"
                                     uk-scrollspy="cls: uk-animation-slide-bottom;">
                                    <div class="uk-flex uk-flex-middle" uk-grid>
                                        <div class="uk-width-auto@m">
                                            <template v-if="olympiad.logo != null">
                                                <a :href="'/organization/' + olympiad.user_id">
                                                    <img style="margin-left: 11.6px" class="uk-visible@m"
                                                         :src="'/storage/' + olympiad.logo"
                                                         height="100px" width="100px">
                                                </a>
                                            </template>
                                            <template v-if="olympiad.logo == null">
                                                <a :href="'organization/' + olympiad.user_id">
                                                    <img style="margin-left: 11.6px" class="uk-visible@m"
                                                         src="{{asset('avdar_logo.png')}}"
                                                         height="100px" width="100px">
                                                </a>
                                            </template>
                                        </div>
                                        <div class="uk-width-expand@s">
                                            <p class="uk-text-meta uk-margin-remove-bottom"
                                               style="color: cornflowerblue;">
                                                @{{olympiad.first_name}}
                                            </p>
                                            <div class="uk-margin-remove-bottom">
                                                <div>
                                                    <h3 class="uk-card-title uk-margin-remove-top">
                                                        @{{ olympiad.title.substring(0, 60)}}
                                                    </h3>
                                                </div>
                                            </div>
                                            <hr class="uk-hidden@m">
                                            <div class="uk-margin-remove-top">
                                                <i class="icon ion-md-compass"></i>
                                                <span class="uk-text-meta uk-margin-right">Ulaanbaatar/Mongolia</span>
                                                <i class="ion-md-calendar"></i>
                                                <span class="uk-text-meta uk-margin-right">
                Бүртгэл дуусах: @{{ olympiad.register_end_date.substring(0, 10) }}
                </span>
                                                <i class="ion-md-bookmark"></i>
                                                <span class="uk-text-meta">
                @{{ olympiad.lesson_name }}
                </span>
                                            </div>
                                            <hr class="uk-hidden@m">
                                        </div>
                                        <div class="uk-width-auto@m">
                                            <a :href="'/olympiad/' + olympiad.id"
                                               style="border-radius: 5px; margin-right: 9px"
                                               class="sc-text-200 uk-button-score uk-button uk-visible@m uk-hidden@l">
                                                Дэлгэрэнгүй
                                            </a>
                                            <a :href="'/olympiad/' + olympiad.id"
                                               style="border-radius: 5px; margin-right: 20px"
                                               class="sc-text-200 uk-button-score uk-button uk-visible@l">
                                                Дэлгэрэнгүй
                                            </a>
                                            <a :href="'/olympiad/' + olympiad.id" style="border-radius: 5px;"
                                               class="sc-text-200 uk-button-score uk-button uk-hidden@m uk-width-1-1">
                                                Дэлгэрэнгүй
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </template>
                        </template>
                    </div>
                </template>
                <template v-if="contents.length > 0">
                    <template v-for="content in contents">
                        <div v-if="!content.is_content" class="uk-card uk-card-default uk-margin-bottom"
                             uk-scrollspy="cls: uk-animation-slide-bottom; repeat: true;"
                             style="border-radius: 25px; border-bottom-left-radius: 0;">
                            <div class="uk-card-header">
                                <div class="uk-grid-small uk-flex-middle" uk-grid>
                                    <div class="uk-width-expand">
                                        <p class="sc-text-brand sc-text-400 uk-text-meta uk-margin-remove-top uk-margin-remove-bottom">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Ярилцлага
                                            {{--<span class="sc-color-grey sc-text-200 uk-text-meta uk-margin-remove-top">--}}
                                            {{--<span>•</span>--}}
                                            {{--<time datetime="2016-04-01T19:00">--}}
                                            {{--@{{ content.created_at.date.substring(0, 16) }}--}}
                                            {{--April 01, 2016--}}
                                            {{--/--}}
                                            {{--Нийтэлсэн:--}}
                                            {{--</time>--}}
                                            {{--</span>--}}
                                        </p>
                                        <h3 class="uk-margin-remove-top sc-text-default sc-text-400 uk-card-title uk-margin-remove-bottom">
                                            <span style="color: #00AB8E;">●</span>
                                            <a v-on:click="getCurrentContent(content)" class="sc-link-none"
                                               href="#content-detail"
                                               uk-toggle>
                                                @{{ content.title }}
                                            </a>
                                            {{--Sqore träffade Moa, 29 år – en lärare som har hittat sitt drömyrke--}}
                                        </h3>
                                        <i class="sc-color-grey sc-text-200 uk-text-meta uk-margin-remove-top">
                                            <time datetime="2016-04-01T19:00">
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Нийтэлсэн:
                                                @{{ content.created_at.date.substring(0, 16) }}
                                            </time>
                                        </i>
                                    </div>
                                </div>
                            </div>
                            {{--<div class="uk-card-body">--}}
                            {{--<p class="sc-text-default sc-text-200">--}}
                            {{--@{{ content.description }}--}}
                            {{--Mycket frihet, en het arbetsmarknad, långa--}}
                            {{--ledigheter,--}}
                            {{--och en chans att vara en positiv--}}
                            {{--förebild i många unga människors liv. Det är bara några av de sakerna som Moa--}}
                            {{--Sjöberg--}}
                            {{--älskar--}}
                            {{--med sitt jobb! Moa Sjöberg är 29 år gammal och har arbetat som lärare på--}}
                            {{--International--}}
                            {{--School of the Gothenburg Region, ISGR, i ett […]</p>--}}
                            {{--</div>--}}
                            {{--<div class="uk-card-footer">--}}
                            {{--<a href="#" class="uk-button uk-button-text">Дэлгэрэнгүй</a>--}}
                            {{--</div>--}}
                        </div>
                        <div v-if="content.is_content">
                            <h1 class="sc-text-default sc-text-400 uk-margin-remove-bottom">
                                <a v-on:click="getCurrentContent(content)" class="sc-link-underline"
                                   href="#content-detail"
                                   uk-toggle>@{{ content.title }}</a>
                            </h1>
                            <p class="sc-color-grey sc-text-200 uk-text-meta uk-margin-remove-top uk-margin-remove-bottom">
                                <time datetime="2016-04-01T19:00">
                                    @{{ 'Нийтэлсэн: ' + content.created_at.date.substring(0, 16) + ' Нийтэлсэн ' + '?'
                                    }}
                                </time>
                            </p>
                            <p class="sc-text-default sc-text-400">
                                @{{ content.description }}
                            </p>
                            <hr>
                        </div>
                    </template>
                </template>
                <div style="display: block;" id="spinner" class="uk-text-center">
                    <div style="color: #00AB8E" uk-spinner="ratio: 1.5"></div>
                </div>
            </div>
        </div>
    </div>
    @include('.layout.footer')
    @include('.layout.login_modal')
    @include('.layout.content_modal')
    @include('.layout.contact_modal')
    @include('.layout.offcanvas')
    @include('.layout.terms_condition_modal')
    @include('.layout.security_condition_modal')
    @include('.layout.service_modal')
</div>


</body>
<script src="{{asset('js/uikit.min.js')}}"></script>
<script src="{{asset('js/uikit-icons.min.js')}}"></script>
<script src="{{asset('js/vuejs.js')}}"></script>
<script src="https://cdn.jsdelivr.net/npm/vue-resource@1.3.6"></script>
<script>
    var app = new Vue({
        el: '#app',
        data: {
            message: 'Hello Vue!',
            email: null,
            password: null,
            loginError: null,
            type: 'olympiad',
            olympiads: [],
            contents: [],
            currentContent: {},
            is_olympiad: null,
            next_page_url: null,
            is_downloading: false
        },
        created: function () {
            this.changeType(this.type);
            this.isOlympiad();
            // this.getData();
            // this.getOlympiads('/rest/olympiads');
        },
        methods: {
            login: function () {
                this.loginError = null;
                var jsonObject = JSON.stringify({email: this.email, password: this.password});
                this.$http.post('/login', jsonObject).then(function (data) {
                    location.reload();
                }, function (error) {
                    this.loginError = error.body.responseMessage;
                });
            },
            logout: function () {
                this.$http.get('/logout').then(function () {
                    localStorage.removeItem('user');
                    location.reload();
                })
            },
            getData: function () {
                var urlParams = new URLSearchParams(window.location.search);
                if (urlParams.get('type') == 'olympiad')
                    this.getOlympiads('/rest/olympiads');
                else if (urlParams.get('type') == 'content')
                    this.getContents('/rest/contents/content');
                else if (urlParams.get('type') == 'speech')
                    this.getContents('/rest/contents/speech');
            },
            getOlympiads: function (url) {
                this.contents = [];
                this.is_downloading = true;
                document.getElementById('spinner').style.display = 'block';
                this.$http.get(url).then(function (data) {
                    this.next_page_url = data.body.next_page_url;
                    for (var _i = 0; _i < data.body.data.length; _i++) {
                        this.olympiads.push(data.body.data[_i]);
                    }
                    this.is_downloading = false;
                    document.getElementById('spinner').style.display = 'none';
                });
            },
            getContents: function (url) {
                this.olympiads = [];
                this.is_downloading = true;
                document.getElementById('spinner').style.display = 'block';
                this.$http.get(url).then(function (data) {
                    this.next_page_url = data.body.meta.pagination.links.next;
                    for (var _i = 0; _i < data.body.data.length; _i++) {
                        this.contents.push(data.body.data[_i]);
                    }
                    console.log(this.contents);
                    this.is_downloading = false;
                    document.getElementById('spinner').style.display = 'none';
                });
            },
            getCurrentContent: function (content) {
                this.currentContent = content;
                this.currentContent.date = content.created_at.date.substring(0, 16);
                console.log(this.currentContent.created_at.date);
                console.log(content);
            },
            changeType: function (type) {
                this.type = type;
                var newurl = window.location.protocol + "//" + window.location.host + window.location.pathname + '?type=' + type;
                window.history.pushState({path: newurl}, '', newurl);
                this.contents = [];
                this.isOlympiad();
                this.getData();
            },
            isOlympiad: function () {
                var urlParams = new URLSearchParams(window.location.search);
                if (urlParams.get('type') == 'olympiad')
                    this.is_olympiad = true;
                else this.is_olympiad = false;
            },
            notyf: function () {
                console.log(this.olympiads);
            }
        }
    });
    window.onscroll = function () {
        if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight - (document.getElementById('footerSection').clientHeight) - 50) {
            if (app.next_page_url)
                if (!app.is_downloading) {
                    var urlParams = new URLSearchParams(window.location.search);
                    if (urlParams.get('type') == 'olympiad')
                        app.getOlympiads(app.next_page_url);
                    else
                        app.getContents(app.next_page_url);
                }
        }
    };
</script>
</html>