{{--ID: {{$mandate->id}}<br>--}}
{{--LOGO: {{$olympiad->logo}}--}}
        <!doctype html>
<html>
<head>
    <meta name="viewport" content="width=device-width"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed&amp;subset=cyrillic-ext" rel="stylesheet">
    <title>Мандат</title>
    <style>
        /* -------------------------------------
        GLOBAL RESETS
        ------------------------------------- */
        img {
            border: none;
            -ms-interpolation-mode: bicubic;
            max-width: 100%;
        }

        body {
            background-color: #f6f6f6;
            font-family: 'Roboto Condensed', sans-serif !important;
            -webkit-font-smoothing: antialiased;
            font-size: 14px;
            line-height: 1.4;
            margin: 0;
            padding: 0;
            -ms-text-size-adjust: 100%;
            -webkit-text-size-adjust: 100%;
        }

        table {
            border-collapse: separate;
            mso-table-lspace: 0pt;
            mso-table-rspace: 0pt;
            width: 100%;
        }

        table td {
            font-family: 'Roboto Condensed', sans-serif !important;
            font-size: 14px;
            vertical-align: top;
        }

        /* -------------------------------------
        BODY & CONTAINER
        ------------------------------------- */
        .body {
            background-color: #f6f6f6;
            width: 100%;
        }

        /* Set a max-width, and make it display as block so it will automatically stretch to that width, but will also shrink down on a phone or something */
        .container {
            display: block;
            Margin: 0 auto !important;
            /* makes it centered */
            max-width: 580px;
            padding: 10px;
            width: 580px;
        }

        /* This should also be a block element, so that it will fill 100% of the .container */
        .content {
            box-sizing: border-box;
            display: block;
            Margin: 0 auto;
            max-width: 580px;
            padding: 10px;
        }

        /* -------------------------------------
        HEADER, FOOTER, MAIN
        ------------------------------------- */
        .main {
            background: #ffffff;
            border-radius: 3px;
            width: 100%;
        }

        .wrapper {
            box-sizing: border-box;
            padding: 20px;
        }

        .content-block {
            padding-bottom: 10px;
            padding-top: 10px;
        }

        .footer {
            clear: both;
            Margin-top: 10px;
            text-align: center;
            width: 100%;
        }

        .footer td,
        .footer p,
        .footer span,
        .footer a {
            color: #999999;
            font-size: 12px;
            text-align: center;
        }

        /* -------------------------------------
        TYPOGRAPHY
        ------------------------------------- */
        h1,
        h2,
        h3,
        h4 {
            color: #000000;
            font-family: 'Roboto Condensed', sans-serif;
            font-weight: 400;
            line-height: 1.4;
            margin: 0;
            Margin-bottom: 30px;
        }

        h1 {
            font-size: 35px;
            font-weight: 300;
            text-align: center;
            text-transform: capitalize;
        }

        p,
        ul,
        ol {
            font-family: 'Roboto Condensed', sans-serif;
            font-size: 14px;
            font-weight: normal;
            margin: 0;
            Margin-bottom: 15px;
        }

        p li,
        ul li,
        ol li {
            list-style-position: inside;
            margin-left: 5px;
        }

        a {
            color: #3498db;
            text-decoration: underline;
        }

        /* -------------------------------------
        BUTTONS
        ------------------------------------- */
        .btn {
            box-sizing: border-box;
            width: 100%;
        }

        .btn > tbody > tr > td {
            padding-bottom: 15px;
        }

        .btn table {
            width: auto;
        }

        .btn table td {
            background-color: #ffffff;
            border-radius: 5px;
            text-align: center;
        }

        .btn a {
            background-color: #ffffff;
            border: solid 1px #3498db;
            border-radius: 5px;
            box-sizing: border-box;
            color: #3498db;
            cursor: pointer;
            display: inline-block;
            font-size: 14px;
            font-weight: bold;
            margin: 0;
            padding: 12px 25px;
            text-decoration: none;
            text-transform: capitalize;
        }

        .btn-primary table td {
            background-color: #3498db;
        }

        .btn-primary a {
            background-color: #3498db;
            border-color: #3498db;
            color: #ffffff;
        }

        /* -------------------------------------
        OTHER STYLES THAT MIGHT BE USEFUL
        ------------------------------------- */
        .last {
            margin-bottom: 0;
        }

        .first {
            margin-top: 0;
        }

        .align-center {
            text-align: center;
        }

        .align-right {
            text-align: right;
        }

        .align-left {
            text-align: left;
        }

        .clear {
            clear: both;
        }

        .mt0 {
            margin-top: 0;
        }

        .mb0 {
            margin-bottom: 0;
        }

        .preheader {
            color: transparent;
            display: none;
            height: 0;
            max-height: 0;
            max-width: 0;
            opacity: 0;
            overflow: hidden;
            mso-hide: all;
            visibility: hidden;
            width: 0;
        }

        .powered-by a {
            text-decoration: none;
        }

        hr {
            border: 0;
            border-bottom: 1px solid #f6f6f6;
            Margin: 20px 0;
        }

        /* -------------------------------------
        RESPONSIVE AND MOBILE FRIENDLY STYLES
        ------------------------------------- */
        @media only screen and (max-width: 620px) {
            table[class=body] h1 {
                font-size: 28px !important;
                margin-bottom: 10px !important;
            }

            table[class=body] p,
            table[class=body] ul,
            table[class=body] ol,
            table[class=body] td,
            table[class=body] span,
            table[class=body] a {
                font-size: 16px !important;
            }

            table[class=body] .wrapper,
            table[class=body] .article {
                padding: 10px !important;
            }

            table[class=body] .content {
                padding: 0 !important;
            }

            table[class=body] .container {
                padding: 0 !important;
                width: 100% !important;
            }

            table[class=body] .main {
                border-left-width: 0 !important;
                border-radius: 0 !important;
                border-right-width: 0 !important;
            }

            table[class=body] .btn table {
                width: 100% !important;
            }

            table[class=body] .btn a {
                width: 100% !important;
            }

            table[class=body] .img-responsive {
                height: auto !important;
                max-width: 100% !important;
                width: auto !important;
            }
        }

        /* -------------------------------------
        PRESERVE THESE STYLES IN THE HEAD
        ------------------------------------- */
        @media all {
            .ExternalClass {
                width: 100%;
            }

            .ExternalClass,
            .ExternalClass p,
            .ExternalClass span,
            .ExternalClass font,
            .ExternalClass td,
            .ExternalClass div {
                line-height: 100%;
            }

            .apple-link a {
                color: inherit !important;
                font-family: inherit !important;
                font-size: inherit !important;
                font-weight: inherit !important;
                line-height: inherit !important;
                text-decoration: none !important;
            }

            .btn-primary table td:hover {
                background-color: #34495e !important;
            }

            .btn-primary a:hover {
                background-color: #34495e !important;
                border-color: #34495e !important;
            }
        }
    </style>
</head>
<body class="">
<table border="0" cellpadding="0" cellspacing="0" class="body">
    <tr>
        <td>&nbsp;</td>
        <td class="container">
            <div class="content">

                <!-- START CENTERED WHITE CONTAINER -->
                <span class="preheader">This is preheader text. Some clients will show this text as a preview.</span>
                <table class="main">

                    <!-- START MAIN CONTENT AREA -->
                    <tr>
                        <td class="wrapper">
                            <table border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <p>Сайн уу, {{$mandate->first_name}}</p>
                                        <p>Олимпиадын нэр:</p>
                                        <p><b>{{$mandate->title}}</b></p>
                                        <p>Болох газар:</p>
                                        {!! $mandate->address !!}
                                        <table border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
                                            <tbody>
                                            <tr>
                                                <td align="left">
                                                    <table border="0" cellpadding="0" cellspacing="0">
                                                        <tbody>
                                                        <tr>
                                                            <td>
                                                                <a href="http://htmlemail.io" target="_blank">
                                                                    Call To Action</a></td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <i>Энэхүү имэйл нь автоматаар илгээгдсэн учир хариу бичих шаардлагагүй</i>
                                        <p><b>Танд амжилт хүсье.</b></p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <!-- END MAIN CONTENT AREA -->
                </table>

                <!-- START FOOTER -->
                <div class="footer">
                    <table border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td class="content-block">
                                <span class="apple-link">TechAvdar, Хүрээ хотхон 3-р хороо, Чингэлтэй дүүрэг Улаанбаатар хот, Монгол улс</span>
                                <br>Дахин имэйл хүлээж авахыг хүсэхгүй бол энэ товч дээр дарна уу / <a>Unsubscribe</a>.
                            </td>
                        </tr>
                        <tr>
                            <td class="content-block powered-by">
                                <a>© Copyright TechAvdar - 2018</a>
                            </td>
                        </tr>
                    </table>
                </div>
                <!-- END FOOTER -->

                <!-- END CENTERED WHITE CONTAINER -->
            </div>
        </td>
        <td>&nbsp;</td>
    </tr>
</table>
</body>
</html>


{{--<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">--}}
{{--<html xmlns="http://www.w3.org/1999/xhtml"--}}
{{--xmlns:v="urn:schemas-microsoft-com:vml"--}}
{{--xmlns:o="urn:schemas-microsoft-com:office:office">--}}
{{--<head>--}}
{{--<!--[if gte mso 9]>--}}
{{--<xml>--}}
{{--<o:OfficeDocumentSettings>--}}
{{--<o:AllowPNG/>--}}
{{--<o:PixelsPerInch>96</o:PixelsPerInch>--}}
{{--</o:OfficeDocumentSettings>--}}
{{--</xml><![endif]-->--}}
{{--<!-- fix outlook zooming on 120 DPI windows devices -->--}}
{{--<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">--}}
{{--<meta name="viewport" content="width=device-width, initial-scale=1"> <!-- So that mobile will display zoomed in -->--}}
{{--<meta http-equiv="X-UA-Compatible" content="IE=edge"> <!-- enable media queries for windows phone 8 -->--}}
{{--<meta name="format-detection" content="date=no"> <!-- disable auto date linking in iOS 7-9 -->--}}
{{--<meta name="format-detection" content="telephone=no"> <!-- disable auto telephone linking in iOS 7-9 -->--}}
{{--<title>Two Columns to Rows (Simple)</title>--}}

{{--<style type="text/css">--}}
{{--body {--}}
{{--margin: 0;--}}
{{--padding: 0;--}}
{{---ms-text-size-adjust: 100%;--}}
{{---webkit-text-size-adjust: 100%;--}}
{{--}--}}

{{--table {--}}
{{--border-spacing: 0;--}}
{{--}--}}

{{--table td {--}}
{{--border-collapse: collapse;--}}
{{--}--}}

{{--.ExternalClass {--}}
{{--width: 100%;--}}
{{--}--}}

{{--.ExternalClass,--}}
{{--.ExternalClass p,--}}
{{--.ExternalClass span,--}}
{{--.ExternalClass font,--}}
{{--.ExternalClass td,--}}
{{--.ExternalClass div {--}}
{{--line-height: 100%;--}}
{{--}--}}

{{--.ReadMsgBody {--}}
{{--width: 100%;--}}
{{--background-color: #ebebeb;--}}
{{--}--}}

{{--table {--}}
{{--mso-table-lspace: 0pt;--}}
{{--mso-table-rspace: 0pt;--}}
{{--}--}}

{{--img {--}}
{{---ms-interpolation-mode: bicubic;--}}
{{--}--}}

{{--.yshortcuts a {--}}
{{--border-bottom: none !important;--}}
{{--}--}}

{{--@media screen and (max-width: 599px) {--}}
{{--.force-row,--}}
{{--.container {--}}
{{--width: 100% !important;--}}
{{--max-width: 100% !important;--}}
{{--}--}}
{{--}--}}

{{--@media screen and (max-width: 400px) {--}}
{{--.container-padding {--}}
{{--padding-left: 12px !important;--}}
{{--padding-right: 12px !important;--}}
{{--}--}}
{{--}--}}

{{--.ios-footer a {--}}
{{--color: #aaaaaa !important;--}}
{{--text-decoration: underline;--}}
{{--}--}}

{{--a[href^="x-apple-data-detectors:"],--}}
{{--a[x-apple-data-detectors] {--}}
{{--color: inherit !important;--}}
{{--text-decoration: none !important;--}}
{{--font-size: inherit !important;--}}
{{--font-family: inherit !important;--}}
{{--font-weight: inherit !important;--}}
{{--line-height: inherit !important;--}}
{{--}--}}
{{--</style>--}}
{{--</head>--}}

{{--<body style="margin:0; padding:0;" bgcolor="#F0F0F0" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">--}}

{{--<!-- 100% background wrapper (grey background) -->--}}
{{--<table border="0" width="100%" height="100%" cellpadding="0" cellspacing="0" bgcolor="#F0F0F0">--}}
{{--<tr>--}}
{{--<td align="center" valign="top" bgcolor="#F0F0F0" style="background-color: #F0F0F0;">--}}

{{--<br>--}}

{{--<!-- 600px container (white background) -->--}}
{{--<table border="0" width="600" cellpadding="0" cellspacing="0" class="container"--}}
{{--style="width:600px;max-width:600px">--}}
{{--<tr>--}}
{{--<td class="container-padding header" align="left"--}}
{{--style="font-family:Helvetica, Arial, sans-serif;font-size:24px;font-weight:bold;padding-bottom:12px;color:#DF4726;padding-left:24px;padding-right:24px">--}}
{{--Antwort v1.0--}}
{{--</td>--}}
{{--</tr>--}}
{{--<tr>--}}
{{--<td class="container-padding content" align="left"--}}
{{--style="padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff">--}}
{{--<br>--}}
{{--<div class="title"--}}
{{--style="font-family:Helvetica, Arial, sans-serif;font-size:18px;font-weight:600;color:#374550">--}}
{{--Two Columns (simple)--}}
{{--</div>--}}
{{--<br>--}}

{{--<div class="body-text"--}}
{{--style="font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333">--}}
{{--This is an example of transforming two columns on desktop into rows on mobile.--}}
{{--<br><br>--}}
{{--</div>--}}

{{--<div class="hr" style="height:1px;border-bottom:1px solid #cccccc">&nbsp;</div>--}}
{{--<br>--}}

{{--<!-- example: two columns (simple) -->--}}

{{--<!--[if mso]>--}}
{{--<table border="0" cellpadding="0" cellspacing="0" width="100%">--}}
{{--<tr>--}}
{{--<td width="50%" valign="top"><![endif]-->--}}

{{--<table width="264" border="0" cellpadding="0" cellspacing="0" align="left" class="force-row">--}}
{{--<tr>--}}
{{--<td class="col" valign="top"--}}
{{--style="font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;width:100%">--}}
{{--<strong>Herman Melville</strong>--}}
{{--<br><br>--}}
{{--It's worse than being in the whirled woods, the last day of the year! Who'd go--}}
{{--climbing after chestnuts now? But there they go, all cursing, and here I don't.--}}
{{--<br><br>--}}
{{--</td>--}}
{{--</tr>--}}
{{--</table>--}}

{{--<!--[if mso]></td>--}}
{{--<td width="50%" valign="top"><![endif]-->--}}

{{--<table width="264" border="0" cellpadding="0" cellspacing="0" align="right" class="force-row">--}}
{{--<tr>--}}
{{--<td class="col" valign="top"--}}
{{--style="font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;width:100%">--}}
{{--<strong>I am Ishmael</strong>--}}
{{--<br><br>--}}
{{--White squalls? white whale, shirr! shirr! Here have I heard all their chat just now,--}}
{{--and the white whale—shirr! shirr!—but spoken of once! and…--}}
{{--<br><br>--}}
{{--</td>--}}
{{--</tr>--}}
{{--</table>--}}

{{--<!--[if mso]></td></tr></table><![endif]-->--}}


{{--<!--/ end example -->--}}

{{--<div class="hr" style="height:1px;border-bottom:1px solid #cccccc;clear: both;">&nbsp;</div>--}}
{{--<br>--}}

{{--<div class="subtitle"--}}
{{--style="font-family:Helvetica, Arial, sans-serif;font-size:16px;font-weight:600;color:#2469A0">--}}
{{--Code Walkthrough--}}
{{--</div>--}}

{{--<div class="body-text"--}}
{{--style="font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333">--}}
{{--<ol>--}}
{{--<li>--}}
{{--Create a columns container <code--}}
{{--style="background-color:#eee;padding:0 4px;font-family:Menlo, Courier, monospace;font-size:12px">&lt;table&gt;</code>--}}
{{--just for Outlook using <code--}}
{{--style="background-color:#eee;padding:0 4px;font-family:Menlo, Courier, monospace;font-size:12px">if--}}
{{--mso</code> conditionals.<br>--}}
{{--The container's <code--}}
{{--style="background-color:#eee;padding:0 4px;font-family:Menlo, Courier, monospace;font-size:12px">&lt;td&gt;</code>'s--}}
{{--have a width of <code--}}
{{--style="background-color:#eee;padding:0 4px;font-family:Menlo, Courier, monospace;font-size:12px">50%.</code>--}}
{{--<br><br>--}}
{{--</li>--}}
{{--<li>--}}
{{--Wrap our columns in a <code--}}
{{--style="background-color:#eee;padding:0 4px;font-family:Menlo, Courier, monospace;font-size:12px">&lt;table&gt;</code>--}}
{{--with a <strong>fixed width</strong> of <code--}}
{{--style="background-color:#eee;padding:0 4px;font-family:Menlo, Courier, monospace;font-size:12px">264px</code>.--}}
{{--<ul>--}}
{{--<li>--}}
{{--264px = (600 - 24*3) / 2 - container width minus margins divided by number--}}
{{--of columns--}}
{{--</li>--}}
{{--<li>--}}
{{--First table is aligned left.--}}
{{--</li>--}}
{{--<li>--}}
{{--Second table is aligned right.--}}
{{--</li>--}}
{{--</ul>--}}
{{--<br>--}}
{{--</li>--}}
{{--<li>--}}
{{--Apply <code--}}
{{--style="background-color:#eee;padding:0 4px;font-family:Menlo, Courier, monospace;font-size:12px">clear:--}}
{{--both;</code> to first element after our wrapper table.--}}
{{--</li>--}}
{{--</ol>--}}

{{--<br>--}}
{{--<em>--}}
{{--<small>Last updated: 10 October 2014</small>--}}
{{--</em>--}}
{{--</div>--}}

{{--<br>--}}
{{--</td>--}}
{{--</tr>--}}
{{--<tr>--}}
{{--<td class="container-padding footer-text" align="left"--}}
{{--style="font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px">--}}
{{--<br><br>--}}
{{--Sample Footer text: © 2015 Acme, Inc.--}}
{{--<br><br>--}}

{{--You are receiving this email because you opted in on our website. Update your <a href="#"--}}
{{--style="color:#aaaaaa">email--}}
{{--preferences</a> or <a href="#" style="color:#aaaaaa">unsubscribe</a>.--}}
{{--<br><br>--}}

{{--<strong>Acme, Inc.</strong><br>--}}
{{--<span class="ios-footer">--}}
{{--123 Main St.<br>--}}
{{--Springfield, MA 12345<br>--}}
{{--</span>--}}
{{--<a href="http://www.acme-inc.com" style="color:#aaaaaa">www.acme-inc.com</a><br>--}}

{{--<br><br>--}}

{{--</td>--}}
{{--</tr>--}}
{{--</table>--}}
{{--<!--/600px container -->--}}


{{--</td>--}}
{{--</tr>--}}
{{--</table>--}}
{{--<!--/100% background wrapper-->--}}

{{--</body>--}}
{{--</html>--}}