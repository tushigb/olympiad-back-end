<!doctype html>
<html lang="{{ app()->getLocale() }}" xmlns:v-on="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,400,700&amp;subset=cyrillic,cyrillic-ext"
          rel="stylesheet">
    <link rel="stylesheet" href="{{asset('css/theme.css')}}" type="text/css"/>
    <link rel="stylesheet" href="{{asset('css/uikit.css')}}" type="text/css"/>
    <link rel="stylesheet" href="{{asset('css/ionicons.min.css')}}">
    <link rel="stylesheet" href="../../public/css/uikit.min.css" type="text/css"/>
    <link rel="stylesheet" href="../../public/css/theme.css" type="text/css"/>
    <title>Olympiad</title>
</head>
<body>
<div id="app">

    @include('.layout.navbar')

    <div style="border-style: none none solid none; border-width: 0.15mm; border-color: #f3f4f7;">
        <div class="uk-section uk-light uk-background-cover" style="background-image: url({{asset('bgAbout.jpg')}})">
            <div uk-scrollspy="cls: uk-animation-slide-top-medium;" class="uk-container uk-text-center">
                <h1 class="sc-text-white sc-text-200 uk-margin-small-bottom">Монгол улсын олимпиадын нэгдсэн мэдээллийн
                    цахим сан</h1>
                <h3 class="sc-text-default sc-text-200 uk-margin-remove-top">
                    Олимпиад зохион байгуулах, оролцох, мэдээлэл авах
                </h3>
            </div>
        </div>
    </div>

    <div style="border-style: none none solid none; border-width: 0.15mm; border-color: #f3f4f7;">
        <div class="uk-section ">
            <div class="uk-container">
                <div class="uk-flex uk-flex-middle uk-child-width-expand@s" uk-grid>
                    <div uk-scrollspy="cls: uk-animation-slide-left;" class="uk-grid-item-match">
                        <p class="sc-text-default sc-text-200 uk-text-justify">
                            Олимпиад зохион байгуулах, түүнд оролцох үйл ажиллагаа технологийн хоцрогдсон байдлын улмаас
                            маш
                            олон төрлийн хүндрэл бэрхшээлийг авчирдаг. Энэ хүндрэл нь ганц зохион байгуулагч гэлтгүй
                            оролцогчдод мөн адил тулгардаг. Хамгийн энгийн жишээ бол мандатын төлбөр тооцоо юм, өнөө үед
                            дэлхий нийт хиймэл оюун ухаан, машин лернинг хөгжүүлж байхад монголчууд бид хүүхдээ
                            олимпиаданд
                            оролцуулах гэж өдөржин түгжрэн байж очиж мандат авч бүртгүүлдэг байж болохгүй, бид ч үүнтэй
                            эвлэрэхгүй. Олимпиадын дүн харахын тухай бол энд бүр яриад ч хэрэггүй байх. Та бидэнтэй
                            санал
                            нийлж байна уу?
                        </p>
                        <p class="sc-text-default sc-text-200 uk-text-justify">
                            Олимпиадын албан ёсны цахим мэдээллийн сан болох olympiad.edu.mn нь ТЕК АВДАР ХХК-ийн албан
                            ёсны
                            оюуны өмч бөгөөд, тогтвортой ажиллагааг хангана ажиллана.
                        </p>
                        {{--<p class="sc-text-default sc-text-200">--}}
                        {{--Sqore was founded by four university graduates when they participated in the Swedish version of--}}
                        {{--entrepreneurship TV show, "The Dragon's Den". The company is backed by VC firm Northzone, and--}}
                        {{--angel investors Mats Gabrielsson, Mikael Ahlström and Emad Zand.--}}
                        {{--</p>--}}
                    </div>
                    <div uk-scrollspy="cls: uk-animation-slide-right;">
                        <img src="{{asset('images/about_image.gif')}}">
                        {{--<div class="uk-card uk-card-default uk-card-body">--}}
                        {{--<h3>Heading</h3>--}}
                        {{--<p>--}}
                        {{--Lorem ipsum dolor sit amet.--}}
                        {{--</p>--}}
                        {{--</div>--}}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="uk-section">
        <div class="uk-container">
            <div class="uk-margin-large-bottom uk-text-center">
                <h2 class="sc-text-brand sc-text-400">Шилдэг олимпиад зохион байгуулагчид</h2>
                <p class="sc-text-default sc-text-200">Эх орондоо дэлхийн стандартанд нийцсэн олимпиад зохион байгуулж
                    байгаа, биднийг дэмжигч бүх хувь хүн, албан байгууллагууддаа баярлалаа!
                </p>
            </div>
            <div class="uk-text-center">
                <h3 class="sc-text-default sc-text-400">Хувь хүн</h3>
            </div>
            <div class="uk-child-width-1-2@m" uk-grid>
                @foreach($people as $person)
                    <div>
                        <div class="uk-card uk-card-default uk-grid-collapse uk-child-width-1-2@s uk-margin" uk-grid>
                            <div class="uk-card-media-left uk-width-1-4@s uk-cover-container">
                                <img src="/storage/{{$person->image_path}}" alt=""
                                     uk-cover>
                                <canvas width="600" height="400"></canvas>
                            </div>
                            <div class="uk-card-body uk-width-expand@s">
                                <h3 class="sc-text-brand sc-text-300 uk-card-title uk-margin-remove-bottom">{{$person->first_name}}</h3>
                                <h6 class="sc-text-default sc-text-200 uk-margin-remove-top">CEO/Founder</h6>
                                <div class="uk-grid-collapse" uk-grid>
                                    <div class="uk-margin-small-right">
                                        <p class="sc-text-default sc-text-200 sc-text-border sc-text-size-12">
                                            <span class="ion-ios-telephone"></span>
                                            {{substr($person->mobile_phone, 0, 4) . '-' . substr($person->mobile_phone, 4, 8)}}
                                            {{--Business Development--}}
                                        </p>
                                    </div>
                                    @if($person->web_site)
                                        <div class="uk-margin-small-right">
                                            <p class="sc-text-default sc-text-200 sc-text-border sc-text-size-12">
                                                <span class="ion-earth"></span>
                                                <a href="{{$person->web_site}}" target="_blank" class="sc-link-brand">Вебсайт</a>
                                            </p>
                                        </div>
                                    @endif
                                    <div class="uk-margin-small-right">
                                        <p class="sc-text-default sc-text-200 sc-text-border sc-text-size-12">
                                            <a href="/organization/{{$person->id}}"
                                               class="sc-link-brand">Дэлгэрэнгүй</a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
                {{--<div>--}}
                {{--<div class="uk-card uk-card-default uk-grid-collapse uk-child-width-1-2@s uk-margin" uk-grid>--}}
                {{--<div class="uk-card-media-left uk-width-1-4@s uk-cover-container">--}}
                {{--<img src="{{asset('guy.jpg')}}" alt="" uk-cover>--}}
                {{--<canvas width="600" height="400"></canvas>--}}
                {{--</div>--}}
                {{--<div class="uk-card-body uk-width-expand@s">--}}
                {{--<h3 class="sc-text-brand sc-text-300 uk-card-title uk-margin-remove-bottom">Media Left</h3>--}}
                {{--<h6 class="sc-text-default sc-text-200 uk-margin-remove-top">CEO/Founder</h6>--}}
                {{--<div class="uk-grid-collapse uk-child-width-auto@m" uk-grid>--}}
                {{--<div class="uk-margin-small-right">--}}
                {{--<p class="sc-text-default sc-text-200 sc-text-border sc-text-size-12">Business--}}
                {{--Development</p>--}}
                {{--</div>--}}
                {{--<div class="uk-margin-small-right">--}}
                {{--<p class="sc-text-default sc-text-200 sc-text-border sc-text-size-12">Karaoke</p>--}}
                {{--</div>--}}
                {{--<div class="uk-margin-small-right">--}}
                {{--<p class="sc-text-default sc-text-200 sc-text-border sc-text-size-12">Packing Light</p>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--<div>--}}
                {{--<div class="uk-card uk-card-default uk-grid-collapse uk-child-width-1-2@s uk-margin" uk-grid>--}}
                {{--<div class="uk-card-media-left uk-width-1-4@s uk-cover-container">--}}
                {{--<img src="{{asset('guy.jpg')}}" alt="" uk-cover>--}}
                {{--<canvas width="600" height="400"></canvas>--}}
                {{--</div>--}}
                {{--<div class="uk-card-body uk-width-expand@s">--}}
                {{--<h3 class="sc-text-brand sc-text-300 uk-card-title uk-margin-remove-bottom">Media Left</h3>--}}
                {{--<h6 class="sc-text-default sc-text-200 uk-margin-remove-top">CEO/Founder</h6>--}}
                {{--<div class="uk-grid-collapse uk-child-width-auto@m" uk-grid>--}}
                {{--<div class="uk-margin-small-right">--}}
                {{--<p class="sc-text-default sc-text-200 sc-text-border sc-text-size-12">Business--}}
                {{--Development</p>--}}
                {{--</div>--}}
                {{--<div class="uk-margin-small-right">--}}
                {{--<p class="sc-text-default sc-text-200 sc-text-border sc-text-size-12">Karaoke</p>--}}
                {{--</div>--}}
                {{--<div class="uk-margin-small-right">--}}
                {{--<p class="sc-text-default sc-text-200 sc-text-border sc-text-size-12">Packing Light</p>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
            </div>
            <hr class="uk-hr">
            <div class="uk-text-center">
                <h3 class="sc-text-default sc-text-400">Байгууллага</h3>
            </div>
            <div class="uk-child-width-1-2@m" uk-grid>
                @foreach($companies as $company)
                    <div>
                        <div class="uk-card uk-card-default uk-grid-collapse uk-child-width-1-2@s uk-margin" uk-grid>
                            <div style="border-style: none solid none none; border-width: 0.15mm; border-color: lightgrey;"
                                 class="uk-card-media-left uk-width-1-4@s uk-cover-container">
                                <img style="padding: 10px" width="40px" src="/storage/{{$company->image_path}}" alt=""
                                     uk-cover>
                                <canvas width="600" height="400"></canvas>
                            </div>
                            <div class="uk-card-body uk-width-expand@s">
                                <h3 class="sc-text-brand sc-text-300 uk-card-title uk-margin-remove-bottom">{{$company->first_name}}</h3>
                                <h6 class="sc-text-default sc-text-200 uk-margin-remove-top">CEO/Founder</h6>
                                <div class="uk-grid-collapse" uk-grid>
                                    <div class="uk-margin-small-right">
                                        <p class="sc-text-default sc-text-200 sc-text-border sc-text-size-12">
                                            <span class="ion-ios-telephone"></span>
                                            {{substr($company->mobile_phone, 0, 4) . '-' . substr($company->mobile_phone, 4, 8)}}
                                            {{--Business Development--}}
                                        </p>
                                    </div>
                                    @if($company->web_site)
                                        <div class="uk-margin-small-right">
                                            <p class="sc-text-default sc-text-200 sc-text-border sc-text-size-12">
                                                <span class="ion-earth"></span>
                                                <a href="{{$company->web_site}}" target="_blank" class="sc-link-brand">Вебсайт</a>
                                            </p>
                                        </div>
                                    @endif
                                    <div class="uk-margin-small-right">
                                        <p class="sc-text-default sc-text-200 sc-text-border sc-text-size-12">
                                            {{--<span class="ion-information-circled"></span>--}}
                                            <a href="/organization/{{$company->id}}"
                                               class="sc-link-brand">Дэлгэрэнгүй</a>
                                            {{--Packing Light--}}
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
                {{--<div>--}}
                {{--<div class="uk-card uk-card-default uk-grid-collapse uk-child-width-1-2@s uk-margin" uk-grid>--}}
                {{--<div class="uk-card-media-left uk-width-1-4@s uk-cover-container">--}}
                {{--<img src="{{asset('guy.jpg')}}" alt="" uk-cover>--}}
                {{--<canvas width="600" height="400"></canvas>--}}
                {{--</div>--}}
                {{--<div class="uk-card-body uk-width-expand@s">--}}
                {{--<h3 class="sc-text-brand sc-text-300 uk-card-title uk-margin-remove-bottom">Media Left</h3>--}}
                {{--<h6 class="sc-text-default sc-text-200 uk-margin-remove-top">CEO/Founder</h6>--}}
                {{--<div class="uk-grid-collapse uk-child-width-auto@m" uk-grid>--}}
                {{--<div class="uk-margin-small-right">--}}
                {{--<p class="sc-text-default sc-text-200 sc-text-border sc-text-size-12">Business--}}
                {{--Development</p>--}}
                {{--</div>--}}
                {{--<div class="uk-margin-small-right">--}}
                {{--<p class="sc-text-default sc-text-200 sc-text-border sc-text-size-12">Karaoke</p>--}}
                {{--</div>--}}
                {{--<div class="uk-margin-small-right">--}}
                {{--<p class="sc-text-default sc-text-200 sc-text-border sc-text-size-12">Packing Light</p>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--<div>--}}
                {{--<div class="uk-card uk-card-default uk-grid-collapse uk-child-width-1-2@s uk-margin" uk-grid>--}}
                {{--<div class="uk-card-media-left uk-width-1-4@s uk-cover-container">--}}
                {{--<img src="{{asset('guy.jpg')}}" alt="" uk-cover>--}}
                {{--<canvas width="600" height="400"></canvas>--}}
                {{--</div>--}}
                {{--<div class="uk-card-body uk-width-expand@s">--}}
                {{--<h3 class="sc-text-brand sc-text-300 uk-card-title uk-margin-remove-bottom">Media Left</h3>--}}
                {{--<h6 class="sc-text-default sc-text-200 uk-margin-remove-top">CEO/Founder</h6>--}}
                {{--<div class="uk-grid-collapse uk-child-width-auto@m" uk-grid>--}}
                {{--<div class="uk-margin-small-right">--}}
                {{--<p class="sc-text-default sc-text-200 sc-text-border sc-text-size-12">Business--}}
                {{--Development</p>--}}
                {{--</div>--}}
                {{--<div class="uk-margin-small-right">--}}
                {{--<p class="sc-text-default sc-text-200 sc-text-border sc-text-size-12">Karaoke</p>--}}
                {{--</div>--}}
                {{--<div class="uk-margin-small-right">--}}
                {{--<p class="sc-text-default sc-text-200 sc-text-border sc-text-size-12">Packing Light</p>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
            </div>
        </div>
    </div>
    @include('.layout.footer')
    @include('.layout.login_modal')
    @include('.layout.offcanvas')
</div>
</body>
<script src="{{asset('js/uikit.min.js')}}"></script>
<script src="{{asset('js/uikit-icons.min.js')}}"></script>
<script src="{{asset('js/vuejs.js')}}"></script>
<script src="https://cdn.jsdelivr.net/npm/vue-resource@1.3.6"></script>
<script>
    var app = new Vue({
        el: '#app',
        data: {
            message: 'Hello Vue!',
            email: null,
            password: null,
            loginError: null,
        },
        methods: {
            login: function () {
                this.loginError = null;
                var jsonObject = JSON.stringify({email: this.email, password: this.password});
                this.$http.post('/login', jsonObject).then(function (data) {
                    location.reload();
                }, function (error) {
                    this.loginError = error.body.responseMessage;
                });
            },
            logout: function () {
                this.$http.get('/logout').then(function () {
                    localStorage.removeItem('user');
                    // console.log(window.location.pathname);
                    location.reload();
                })
            },
        }
    })
</script>