<!doctype html>
<html lang="{{ app()->getLocale() }}" xmlns:v-on="http://www.w3.org/1999/xhtml"
      xmlns:v-bind="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,400,700&amp;subset=cyrillic,cyrillic-ext"
          rel="stylesheet">
    <link rel="stylesheet" href="{{asset('css/theme.css')}}" type="text/css"/>
    <link rel="stylesheet" href="{{asset('css/uikit.css')}}" type="text/css"/>
    <link href="https://unpkg.com/ionicons@4.1.0/dist/css/ionicons.min.css" rel="stylesheet">
    {{--<link rel="stylesheet" href="../../public/css/uikit.min.css" type="text/css"/>--}}
    <link rel="stylesheet" href="../../public/css/theme.css" type="text/css"/>
    <style>
        .mapouter {
            overflow: hidden;
            height: 450px;
            width: 100%;
        }

        .gmap_canvas {
            background: none !important;
            height: 450px;
            width: 100%;
        }
    </style>
    <title>Olympiad</title>
</head>
<body>
<div id="app">
    @include('.layout.navbar')
    <div class="uk-section uk-section-muted">
        <div class="uk-container">
            <div class="uk-child-width-1-3@m" uk-grid>
                <template v-for="(file, index) in files">
                    <div>
                        <div v-on:mouseover="over(index)" v-on:mouseleave="leave(index)"
                             class="uk-card uk-card-default uk-grid-collapse uk-margin" uk-grid>
                            <div class="uk-card-body uk-width-expand@s">
                                <h3 :id="'name' + index"
                                    class="sc-text-brand sc-text-300 uk-card-title uk-margin-remove-bottom">
                                    @{{ file.sub_name }}
                                </h3>
                                <h6 :id="'description' + index"
                                    class="sc-text-default sc-text-200 uk-margin-remove-top">
                                    @{{ file.sub_description }}
                                </h6>
                                <div class="uk-grid-collapse" uk-grid>
                                    <div class="uk-margin-small-right uk-margin-small-bottom uk-margin-small-top">
                                        <p class="sc-text-default sc-text-200 sc-text-border sc-text-size-12">
                                            <span class="ion-ios-telephone"></span>
                                            @{{ file.lesson.name }}
                                        </p>
                                    </div>
                                    <div class="uk-margin-small-right uk-margin-small-bottom uk-margin-small-top">
                                        <p class="sc-text-default sc-text-200 sc-text-border sc-text-size-12">
                                            <span class="ion-earth"></span>
                                            <b>@{{ file.price }}₮</b>
                                        </p>
                                    </div>
                                    <div v-if="user && user.role_id != 2" class="uk-margin-small-right
                                    uk-margin-small-bottom uk-margin-small-top">
                                        <p class="sc-text-default sc-text-200 sc-text-border sc-text-size-12">
                                            <a v-on:click="getPaymentMethod(file.id, index)" href="#file-payment-modal"
                                               uk-toggle class="sc-link-brand">Худалдан
                                                авах</a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </template>
            </div>
        </div>
    </div>
    @include('.layout.footer')
    @include('.layout.login_modal')
    @include('.layout.file_payment_modal')
</div>
</body>
<script src="{{asset('js/uikit.min.js')}}"></script>
<script src="{{asset('js/uikit-icons.min.js')}}"></script>
<script src="{{asset('js/vuejs.js')}}"></script>
<script src="https://cdn.jsdelivr.net/npm/vue-resource@1.3.6"></script>
<script>
    var app = new Vue({
        el: '#app',
        data: {
            user: null,
            message: 'Hello Vue!',
            email: null,
            password: null,
            loginError: null,
            files: [],
            currentFile: null,
        },
        created: function () {
            this.$http.get('/rest/file/all').then(function (data) {
                app.files = data.body.data;
                app.files.forEach(function (file) {

                    file.sub_name = file.name.substring(0, 25);
                    file.sub_description = file.description.substring(0, 50);
                    file.is_hover = false;
                })
            });

            this.$http.get('/verify').then(function (data) {
                this.user = data.body.user;
            });
        },
        methods: {
            login: function () {
                this.loginError = null;
                var jsonObject = JSON.stringify({email: this.email, password: this.password});
                this.$http.post('/login', jsonObject).then(function (data) {
                    location.reload();
                }, function (error) {
                    this.loginError = error.body.responseMessage;
                });
            },
            logout: function () {
                this.$http.get('/logout').then(function () {
                    localStorage.removeItem('user');
                    // console.log(window.location.pathname);
                    location.reload();
                })
            },
            getPaymentMethod(id, index) {
                this.currentFile = this.files[index];
                if (!this.currentFile.payment) {
                    document.getElementById('spinner').style.display = 'block';
                    this.$http.get('/rest/file/payment/' + id).then(function (data) {
                        this.currentFile.payment = data.body;
                        document.getElementById('spinner').style.display = 'none';
                        document.getElementById('qpay_image').innerHTML =
                            '<img class="uk-align-center" src="data:image/png;base64, ' + this.currentFile.payment + '" '
                            + 'width="175px">';
                        console.log(this.currentFile);
                        // <img v-if="currentFile" class="uk-align-center" :src="'data:image/png;base64, ' + currentFile.payment"
                        //     width="175px">
                    });
                } else {
                    document.getElementById('qpay_image').innerHTML =
                        '<img class="uk-align-center" src="data:image/png;base64, ' + this.currentFile.payment + '" '
                        + 'width="175px">';
                }
            },
            over: function (i) {
                document.getElementById('name' + i).innerText = this.files[i].name;
                document.getElementById('description' + i).innerText = this.files[i].description;
            },
            leave: function (i) {
                document.getElementById('name' + i).innerText = this.files[i].sub_name;
                document.getElementById('description' + i).innerText = this.files[i].sub_description;
            }
        }
    })
</script>
</html>