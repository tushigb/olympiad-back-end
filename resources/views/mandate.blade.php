<!doctype html>
<html lang="{{ app()->getLocale() }}" xmlns:v-on="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,400,700&amp;subset=cyrillic,cyrillic-ext"
          rel="stylesheet">
    <link rel="stylesheet" href="{{asset('css/theme.css')}}" type="text/css"/>
    <link rel="stylesheet" href="{{asset('css/uikit.css')}}" type="text/css"/>
    <link rel="stylesheet" href="{{asset('css/ionicons.min.css')}}">
    <link rel="stylesheet" href="../../public/css/uikit.min.css" type="text/css"/>
    <link rel="stylesheet" href="../../public/css/theme.css" type="text/css"/>
    <title>Laravel</title>
</head>
<body>
<img class="uk-align-center"
     src="data:image/png;base64,{!! base64_encode(QrCode::format('png')->errorCorrection('Q')->mergeString(Storage::get($olympiad->logo), .22)->size(300)->margin(1)->generate($mandate_id)) !!} ">
</body>
</html>