<!doctype html>
<html lang="{{ app()->getLocale() }}" xmlns:v-on="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,400,700&amp;subset=cyrillic,cyrillic-ext"
          rel="stylesheet">
    <link rel="stylesheet" href="{{asset('css/theme.css')}}" type="text/css"/>
    <link rel="stylesheet" href="{{asset('css/uikit.css')}}" type="text/css"/>
    {{--<link rel="stylesheet" href="{{asset('css/ionicons.min.css')}}">--}}
    <link href="https://unpkg.com/ionicons@4.1.0/dist/css/ionicons.min.css" rel="stylesheet">
    <link rel="stylesheet" href="../../public/css/uikit.min.css" type="text/css"/>
    <link rel="stylesheet" href="../../public/css/theme.css" type="text/css"/>
    <title>Olympiad</title>
</head>

<body>
<div id="app">
    @include('.layout.navbar')

    {{--profile header--}}
    <div>
        <div class="uk-section uk-section-small uk-section-muted">
            <div class="uk-container">
                <div class="uk-flex uk-flex-middle" uk-grid>
                    <div class="uk-width-1-6">
                        <img src="/storage/{{$user->image_path}}" height="100px" width="100px">
                    </div>
                    <div class="uk-width-expand">
                        <h2 class="sc-text-default sc-text-400 uk-margin-remove-bottom">{{$user->first_name}}</h2>
                        <div class="uk-flex uk-flex-middle uk-margin-remove-bottom uk-margin-remove-top">
                            <i class="sc-text-default icon ion-md-school"></i>&nbsp;
                            <span class="sc-text-default uk-margin-small-right">
                                {{$user->name}}
                            </span>
                            <i class="sc-text-default icon ion-md-ribbon"></i>&nbsp;
                            <span class="sc-text-default">{{$user->class . $user->class_group}}</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div>
        <div class="uk-section">
            <div class="uk-container">
                <div uk-grid>
                    <div class="uk-width-1-3@m organizationDescription" style="border-right: 1px #e5e5e5 solid;">
                        <div>
                            {!! $user->description !!}
                        </div>
                        <hr class="uk-hidden@m">
                    </div>
                    <div class="uk-width-expand">
                        <div class="organizationContent">
                            @foreach($olympiads as $olympiad)
                                <div class="uk-card uk-card-default uk-card-small uk-card-body uk-margin"
                                     uk-scrollspy="cls: uk-animation-slide-bottom;">
                                    <div class="uk-flex uk-flex-middle" uk-grid>
                                        <div class="uk-width-auto@m">
                                            @if($olympiad->logo != null)
                                                <img style="margin-left: 11.6px" class="uk-visible@m"
                                                     src="/storage/{{$olympiad->logo}}"
                                                     height="100px" width="100px">
                                            @else
                                                <img style="margin-left: 11.6px" class="uk-visible@m"
                                                     src="../{{asset('avdar_logo.png')}}"
                                                     height="100px" width="100px">
                                            @endif
                                        </div>
                                        <div class="uk-width-expand@s">
                                            <p class="sc-text-blue uk-text-meta uk-margin-remove-bottom">
                                                {{$olympiad->first_name}}
                                            </p>
                                            <div class="uk-margin-remove-bottom">
                                                <div>
                                                    <h3 class="uk-card-title uk-margin-remove-top">
                                                        <a class="sc-link-default"
                                                           href="/olympiad/{{$olympiad->id}}">{{str_limit($olympiad->title, 60)}}</a>
                                                    </h3>
                                                </div>
                                            </div>
                                            <hr class="uk-hidden@m">
                                            <div class="uk-margin-remove-top">
                                                <i class="sc-color-grey sc-text-size-13 sc-text-200 icon ion-md-compass"></i>
                                                <span class="sc-color-grey sc-text-size-13 sc-text-200 uk-text-meta uk-margin-right">Ulaanbaatar/Mongolia</span>
                                                <i class="sc-color-grey sc-text-size-13 sc-text-200 icon ion-md-calendar"></i>
                                                <span class="sc-color-grey sc-text-size-13 sc-text-200 uk-text-meta uk-margin-right">
                                                    Хугацаа: {{date('Y-m-d', strtotime($olympiad->register_end_date))}}
                                                </span>
                                                <i class="sc-color-grey sc-text-size-13 sc-text-200 icon ion-md-bookmark"></i>
                                                <span class="sc-color-grey sc-text-size-13 sc-text-200 uk-text-meta">
                                                    {{$olympiad->lesson_name}}
                                                </span>
                                            </div>
                                            <hr class="uk-hidden@m">
                                        </div>
                                        <div class="uk-width-auto@m">
                                            {{--@if($user->id == auth()->id())--}}
                                                {{--<a href="/participant/{{$user->id}}/olympiad/{{$olympiad->id}}/mandate/{{$olympiad->mandate_id}}"--}}
                                                   {{--style="border-radius: 5px; margin-right: 9px; color: #fff !important;"--}}
                                                   {{--class="sc-text-200 uk-button-danger uk-button uk-visible@m uk-hidden@l"--}}
                                                   {{--target="_blank">--}}
                                                    {{--Мандат--}}
                                                {{--</a>--}}
                                                {{--<a href="/participant/{{$user->id}}/olympiad/{{$olympiad->id}}/mandate/{{$olympiad->mandate_id}}"--}}
                                                   {{--style="border-radius: 5px; margin-right: 20px; color: #fff !important;"--}}
                                                   {{--class="sc-text-200 uk-button-danger uk-button uk-visible@l"--}}
                                                   {{--target="_blank">--}}
                                                    {{--Мандат--}}
                                                {{--</a>--}}
                                                {{--<a href="/participant/{{$user->id}}/olympiad/{{$olympiad->id}}/mandate/{{$olympiad->mandate_id}}"--}}
                                                   {{--style="border-radius: 5px; color: #fff !important;"--}}
                                                   {{--class="sc-text-200 uk-button-danger uk-button uk-hidden@m uk-width-1-1"--}}
                                                   {{--target="_blank">--}}
                                                    {{--Мандат--}}
                                                {{--</a>--}}
                                            {{--@endif()--}}
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('.layout.footer')
    @include('.layout.login_modal')
    @include('.layout.offcanvas')
</div>
</body>
<script src="{{asset('js/uikit.min.js')}}"></script>
<script src="{{asset('js/uikit-icons.min.js')}}"></script>
<script src="{{asset('js/vuejs.js')}}"></script>
<script src="https://cdn.jsdelivr.net/npm/vue-resource@1.3.6"></script>
<script>
    var app = new Vue({
        el: '#app',
        data: {
            message: 'Hello Vue!',
            email: null,
            password: null,
            loginError: null,
        },
        methods: {
            login: function () {
                this.loginError = null;
                var jsonObject = JSON.stringify({email: this.email, password: this.password});
                this.$http.post('/login', jsonObject).then(function (data) {
                    location.reload();
                }, function (error) {
                    this.loginError = error.body.responseMessage;
                });
            },
            logout: function () {
                this.$http.get('/logout').then(function () {
                    localStorage.removeItem('user');
                    // console.log(window.location.pathname);
                    location.reload();
                })
            }
        }
    })
</script>
</html>