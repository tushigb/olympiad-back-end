<!doctype html>
<html lang="{{ app()->getLocale() }}" xmlns:v-on="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,400,700&amp;subset=cyrillic,cyrillic-ext"
          rel="stylesheet">
    <link rel="stylesheet" href="{{asset('css/theme.css')}}" type="text/css"/>
    <link rel="stylesheet" href="{{asset('css/uikit.css')}}" type="text/css"/>
    <link rel="stylesheet" href="{{asset('css/ionicons.min.css')}}">
    <link rel="stylesheet" href="../../public/css/uikit.min.css" type="text/css"/>
    <link rel="stylesheet" href="../../public/css/theme.css" type="text/css"/>
    <title>Olympiad</title>
</head>
<body>
<div>

</div>
@include('.layout.navbar')
<div>
    <div style="border-style: none none solid none; border-width: 1px; border-color: #f3f4f7;"
         class="uk-grid-collapse uk-child-width-1-2@s uk-flex-middle uk-grid" uk-grid>
        <div class="uk-background-cover uk-first-column"
             style="background-image: url({{asset('light.jpg')}}); box-sizing: border-box; min-height: 100vh; height: 100vh;"
             uk-height-viewport=""></div>
        <div class="uk-padding-large">
            <h1 class="sc-text-default sc-text-400 uk-margin-remove-bottom">Бидэнтэй холбогдох</h1>
            <h2 class="sc-text-brand sc-text-200 uk-margin-remove-top">
                Биднээс асуух асуулт байна уу?
            </h2>
        </div>
    </div>
</div>

<div style="border-style: none none solid none; border-width: 0.15mm; border-color: #f3f4f7;">
    <div class="uk-container uk-section">
        <div class="uk-child-width-1-2@m" uk-grid>
            <div class="uk-text-right">
                <h3 class="sc-text-brand sc-text-200 uk-text-left">Бидэнд сэтгэгдэл илгээхийг хүсвэл</h3>
                <form class="uk-form-stacked">
                    <div class="uk-margin">
                        <label class="sc-text-default sc-text-200 uk-form-label uk-text-left" for="form-stacked-text">Нэр</label>
                        <div class="uk-form-controls">
                            <input class="sc-text-200 uk-input" id="form-stacked-text" type="text"
                                   placeholder="Таны нэр">
                        </div>
                    </div>
                    <div class="uk-margin">
                        <label class="sc-text-default sc-text-200 uk-form-label uk-text-left" for="form-stacked-text">Байгууллага
                            <span>(Зайлшгүй биш)</span></label>
                        <div class="uk-form-controls">
                            <input class="sc-text-200 uk-input" id="form-stacked-text" type="text"
                                   placeholder="Байгууллага">
                        </div>
                    </div>
                    <div class="uk-margin">
                        <label class="sc-text-default sc-text-200 uk-form-label uk-text-left" for="form-stacked-text">Е-Мэйл</label>
                        <div class="uk-form-controls">
                            <input class="sc-text-200 uk-input" id="form-stacked-text" type="text"
                                   placeholder="Е-Мэйл хаяг">
                        </div>
                    </div>
                    <div class="uk-margin">
                        <label class="sc-text-default sc-text-200 uk-form-label uk-text-left"
                               for="form-stacked-text">Утас <span>(Зайлшгүй биш)</span></label>
                        <div class="uk-form-controls">
                            <input class="sc-text-200 uk-input" id="form-stacked-text" type="text"
                                   placeholder="Утасны дугаар">
                        </div>
                    </div>
                    <div class="uk-margin">
                        <label class="sc-text-default sc-text-200 uk-form-label uk-text-left" for="form-stacked-text">Сэтгэгдэл</label>
                        <div class="uk-form-controls">
                            <div class="uk-margin">
                                <textarea class="sc-text-200 uk-textarea" rows="3"
                                          placeholder="Таны сэтгэгдэл"></textarea>
                            </div>
                        </div>
                    </div>
                    <button style="border-radius: 5px;"
                            class="sc-text-200 uk-button-score uk-button">
                        Сэтгэгдэл үлдээх
                    </button>
                </form>
            </div>
            <div>
                <h3 class="sc-text-brand sc-text-200">Visit us in our Ulaanbaatar office</h3>
                <label class="sc-text-default sc-text-400 uk-margin-remove-bottom">SCORE</label>
                <p class="sc-text-default sc-text-200 uk-margin-remove-top uk-margin-remove-bottom">Хүрээ хотхон 3-р
                    хороо</p>
                <p class="sc-text-default sc-text-200 uk-margin-remove-top uk-margin-remove-bottom">Чингэлтэй дүүрэг</p>
                <p class="sc-text-default sc-text-200 uk-margin-remove-top uk-margin-remove-bottom">Улаанбаатар хот</p>
                <p class="sc-text-default sc-text-200 uk-margin-remove-top uk-margin-remove-bottom">Монгол улс</p>
            </div>
        </div>
    </div>
</div>

<div class="mapouter">
    <div class="gmap_canvas">
        <iframe width="100%" height="100%" id="gmap_canvas"
                src="https://maps.google.com/maps?q=Тусгаар тогтнолын ордон&t=&z=15&ie=UTF8&iwloc=&output=embed"
                frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
    </div>
    <a href="https://www.crocothemes.net"></a>
    <style>.mapouter {
            overflow: hidden;
            height: 450px;
            width: 100%;
        }

        .gmap_canvas {
            background: none !important;
            height: 450px;
            width: 100%;
        }</style>
</div>

@include('.layout.footer')
@include('.layout.login_modal')
@include('.layout.offcanvas')
</body>
<script src="{{asset('js/uikit.min.js')}}"></script>
<script src="{{asset('js/uikit-icons.min.js')}}"></script>
<script src="https://cdn.jsdelivr.net/npm/vue-resource@1.3.6"></script>
<script>
    var app = new Vue({
        el: '#app',
        data: {
            message: 'Hello Vue!',
            email: null,
            password: null,
            loginError: null,
        },
        methods: {
            login: function () {
                this.loginError = null;
                var jsonObject = JSON.stringify({email: this.email, password: this.password});
                this.$http.post('/login', jsonObject).then(function (data) {
                    location.reload();
                }, function (error) {
                    this.loginError = error.body.responseMessage;
                });
            },
            logout: function () {
                this.$http.get('/logout').then(function () {
                    localStorage.removeItem('user');
                    // console.log(window.location.pathname);
                    location.reload();
                })
            }
        }
    })
</script>