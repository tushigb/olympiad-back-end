<!doctype html>
<html>
<head>
    <title>Цахим Мандат</title>
    <!--

        An email present from your friends at Litmus (@litmusapp)

        Email is surprisingly hard. While this has been thoroughly tested, your mileage may vary.
        It's highly recommended that you test using a service like Litmus (http://litmus.com) and your own devices.

        Enjoy!

     -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <style type="text/css">
        /* CLIENT-SPECIFIC STYLES */
        body, table, td, a {
            -webkit-text-size-adjust: 100%;
            -ms-text-size-adjust: 100%;
        }

        /* Prevent WebKit and Windows mobile changing default text sizes */
        table, td {
            mso-table-lspace: 0pt;
            mso-table-rspace: 0pt;
        }

        /* Remove spacing between tables in Outlook 2007 and up */
        img {
            -ms-interpolation-mode: bicubic;
        }

        /* Allow smoother rendering of resized image in Internet Explorer */

        /* RESET STYLES */
        img {
            border: 0;
            height: auto;
            line-height: 100%;
            outline: none;
            text-decoration: none;
        }

        table {
            border-collapse: collapse !important;
        }

        body {
            height: 100% !important;
            margin: 0 !important;
            padding: 0 !important;
            width: 100% !important;
        }

        /* iOS BLUE LINKS */
        a[x-apple-data-detectors] {
            color: inherit !important;
            text-decoration: none !important;
            font-size: inherit !important;
            font-family: inherit !important;
            font-weight: inherit !important;
            line-height: inherit !important;
        }

        /* MOBILE STYLES */
        @media screen and (max-width: 525px) {

            /* ALLOWS FOR FLUID TABLES */
            .wrapper {
                width: 100% !important;
                max-width: 100% !important;
            }

            /* ADJUSTS LAYOUT OF LOGO IMAGE */
            .logo img {
                margin: 0 auto !important;
            }

            /* USE THESE CLASSES TO HIDE CONTENT ON MOBILE */
            .mobile-hide {
                display: none !important;
            }

            .img-max {
                max-width: 100% !important;
                width: 100% !important;
                height: auto !important;
            }

            /* FULL-WIDTH TABLES */
            .responsive-table {
                width: 100% !important;
            }

            /* UTILITY CLASSES FOR ADJUSTING PADDING ON MOBILE */
            .padding {
                padding: 10px 5% 15px 5% !important;
            }

            .padding-meta {
                padding: 30px 5% 0px 5% !important;
                text-align: center;
            }

            .padding-copy {
                padding: 10px 5% 10px 5% !important;
                text-align: center;
            }

            .no-padding {
                padding: 0 !important;
            }

            .section-padding {
                padding: 50px 15px 50px 15px !important;
            }

            /* ADJUST BUTTONS ON MOBILE */
            .mobile-button-container {
                margin: 0 auto;
                width: 100% !important;
            }

            .mobile-button {
                padding: 15px !important;
                border: 0 !important;
                font-size: 16px !important;
                display: block !important;
            }

        }

        /* ANDROID CENTER FIX */
        div[style*="margin: 16px 0;"] {
            margin: 0 !important;
        }
    </style>
</head>
<body style="margin: 0 !important; padding: 0 !important;">
<!-- ONE COLUMN SECTION -->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td bgcolor="#ffffff" align="center" style="padding: 15px; align-items: center" class="section-padding">
            <img style="margin-right: 30px; margin-top: 60px;"
                 src="data:image/png;base64,{!! base64_encode(QrCode::format('png')->errorCorrection('Q')->mergeString(Storage::get($mandate->logo), .22)->size(250)->margin(0)->generate($mandate->id)) !!} ">
            <img style="height: 250px; margin-left: 30px; margin-top: 60px;"
                 src="{{public_path() . '/'. $mandate->image_path}}">
            <hr>
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="500">
                <tr>
                    <td align="center" valign="top" width="500">
            <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 500px;"
                   class="responsive-table">
                <tr>
                    <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td>
                                    <!-- COPY -->
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td align="center"
                                                style="padding: 20px 0 0 0; font-size: 16px; line-height: 25px; font-family: 'Roboto Condensed', DejaVu Sans; color: #666666;"
                                                class="padding-copy"><span
                                                        style="text-transform: uppercase">{{$mandate->last_name .' '}}
                                                    <b>{{$mandate->first_name}}</b></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left"
                                                style="padding: 20px 0 0 0; font-size: 16px; line-height: 25px; font-family: 'Roboto Condensed', DejaVu Sans; color: #666666;"
                                                class="padding-copy">
                                                Сургууль: <span><b>{{$mandate->school_name}}</b></span>&nbsp;&nbsp;&nbsp;Анги:
                                                <span><b>{{$mandate->class . $mandate->class_group}}</b></span>&nbsp;&nbsp;&nbsp;Хүйс:
                                                @if($mandate->sex)
                                                    <span><b>Эр</b></span>
                                                @else
                                                    <span><b>Эм</b></span>
                                                @endif
                                                <br><br>
                                                <hr>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left"
                                                style="padding: 20px 0 0 0; font-size: 16px; line-height: 25px; font-family: 'Roboto Condensed', DejaVu Sans; color: #666666;"
                                                class="padding-copy">
                                                Олимпиадын нэр: <span
                                                        style="text-transform: uppercase"><b>{{$mandate->title}}</b></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left"
                                                style="padding: 20px 0 0 0; font-size: 16px; line-height: 25px; font-family: 'Roboto Condensed', DejaVu Sans; color: #666666;"
                                                class="padding-copy">
                                                Олимпиад эхлэх огноо: <span
                                                        style="text-transform: uppercase"><b>{!! substr($mandate->olympiad_start_date, 0, 10) !!}</b></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left"
                                                style="padding: 20px 0 0 0; font-size: 16px; line-height: 25px; font-family: 'Roboto Condensed', DejaVu Sans; color: #666666;"
                                                class="padding-copy">
                                                Зоон:
                                                <b>
                                                    @if($mandate->zone_type_id == 1)
                                                        @if($mandate->min == $mandate->max)
                                                            <span>{{$mandate->min . '-р '. $mandate->zone_type_name}}</span>
                                                        @else
                                                            <span>{{$mandate->min . '-р '. $mandate->min . $mandate->zone_type_name}}</span>
                                                        @endif
                                                    @elseif($mandate->zone_type_id == 2)
                                                        @if($mandate->min == $mandate->max)
                                                            <span>{{$mandate->min . ' '. $mandate->zone_type_name}}</span>
                                                        @else
                                                            <span>{{$mandate->min . ' '. $mandate->min . $mandate->zone_type_name}}</span>
                                                        @endif
                                                    @endif
                                                </b>
                                                &nbsp;&nbsp;&nbsp;Огноо:
                                                <span><b>{!! substr($mandate->zone_start_date, 0, 10) !!}</b></span>
                                                &nbsp;&nbsp;&nbsp;Цаг:
                                                <span><b>{!! substr($mandate->zone_start_date, 10, 15) !!}</b></span>
                                            </td>
                                        </tr>
                                        {{--<tr>--}}
                                        {{--<td align="left"--}}
                                        {{--style="padding: 20px 0 0 0; font-size: 16px; line-height: 25px; font-family: 'Roboto Condensed', DejaVu Sans; color: #666666;"--}}
                                        {{--class="padding-copy">Lorem ipsum dolor sit amet, consectetur adipiscing--}}
                                        {{--elit. Sed varius, leo a ullamcorper feugiat, ante purus sodales justo, a--}}
                                        {{--faucibus libero lacus a est. Aenean at mollis ipsum. Lorem ipsum dolor--}}
                                        {{--sit amet, consectetur adipiscing elit. Sed varius, leo a ullamcorper--}}
                                        {{--feugiat, ante purus sodales justo, a faucibus libero lacus a est. Aenean--}}
                                        {{--at mollis ipsum. Lorem ipsum dolor sit amet, consectetur adipiscing--}}
                                        {{--elit. Sed varius, leo a ullamcorper feugiat, ante purus sodales justo, a--}}
                                        {{--faucibus libero lacus a est. Aenean at mollis ipsum.--}}
                                        {{--</td>--}}
                                        {{--</tr>--}}
                                        {{--<tr>--}}
                                        {{--<td align="left"--}}
                                        {{--style="padding: 20px 0 0 0; font-size: 16px; line-height: 25px; font-family: 'Roboto Condensed', DejaVu Sans; color: #666666;"--}}
                                        {{--class="padding-copy">Lorem ipsum dolor sit amet, consectetur adipiscing--}}
                                        {{--elit. Sed varius, leo a ullamcorper feugiat, ante purus sodales justo, a--}}
                                        {{--faucibus libero lacus a est.--}}
                                        {{--</td>--}}
                                        {{--</tr>--}}
                                        <tr>
                                            <td align="left"
                                                style="font-size: 12px; line-height: 18px; font-family: 'Roboto Condensed', DejaVu Sans; color: #666666;"
                                                class="padding-copy">{!! $mandate->address !!}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left"
                                                style="font-size: 12px; line-height: 18px; font-family: 'Roboto Condensed', DejaVu Sans; color:#666666;">
                                                Танд амжилт хүсэе.
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
    </tr>
    {{--<tr>--}}
    {{--<td bgcolor="#ffffff" align="center" style="padding: 20px 0px;">--}}
    {{--<!--[if (gte mso 9)|(IE)]>--}}
    {{--<table align="center" border="0" cellspacing="0" cellpadding="0" width="500">--}}
    {{--<tr>--}}
    {{--<td align="center" valign="top" width="500">--}}
    {{--<![endif]-->--}}
    {{--<!-- UNSUBSCRIBE COPY -->--}}
    {{--<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="max-width: 500px;"--}}
    {{--class="responsive-table">--}}
    {{--<tr>--}}
    {{--<td align="left"--}}
    {{--style="font-size: 12px; line-height: 18px; font-family: Helvetica, Arial, sans-serif; color:#666666;">--}}
    {{--1234 Main Street, Anywhere, MA 01234, USA--}}
    {{--<br>--}}
    {{--<a href="http://litmus.com" target="_blank" style="color: #666666; text-decoration: none;">Unsubscribe</a>--}}
    {{--<span style="font-family: Arial, sans-serif; font-size: 12px; color: #444444;">&nbsp;&nbsp;|&nbsp;&nbsp;</span>--}}
    {{--<a href="http://litmus.com" target="_blank" style="color: #666666; text-decoration: none;">View--}}
    {{--this email in your browser</a>--}}
    {{--</td>--}}
    {{--</tr>--}}
    {{--</table>--}}
    {{--<!--[if (gte mso 9)|(IE)]>--}}
    {{--</td>--}}
    {{--</tr>--}}
    {{--</table>--}}
    {{--<![endif]-->--}}
    {{--</td>--}}
    {{--</tr>--}}
</table>
</body>
</html>


{{--<div>--}}
{{--<div>--}}
{{--<br>--}}
{{--<div style="float: left">--}}
{{--<img style="margin-left: 25px;"--}}
{{--src="data:image/png;base64,{!! base64_encode(QrCode::format('png')->errorCorrection('Q')->mergeString(Storage::get($olympiad->logo), .22)->size(300)->margin(1)->generate($mandate->id)) !!} ">--}}
{{--</div>--}}
{{--<div style="float: right">--}}
{{--<img style="margin-right: 25px;"--}}
{{--src="data:image/png;base64,{!! base64_encode(QrCode::format('png')->errorCorrection('Q')->mergeString(Storage::get($olympiad->logo), .22)->size(300)->margin(1)->generate($mandate->id)) !!} ">--}}
{{--</div>--}}
{{--</div>--}}
{{--ыйоблрөхйлоырбхөролйыбхөролйыбхөшзугцзнгфцуүүүөө--}}
{{--</div>--}}