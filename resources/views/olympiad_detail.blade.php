<!doctype html>
<html lang="{{ app()->getLocale() }}" xmlns:v-on="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,400,700&amp;subset=cyrillic,cyrillic-ext"
          rel="stylesheet">
    <link rel="stylesheet" href="{{asset('css/theme.css')}}" type="text/css"/>
    <link rel="stylesheet" href="{{asset('css/uikit.css')}}" type="text/css"/>
    {{--    <link rel="stylesheet" href="{{asset('css/ionicons.min.css')}}">--}}
    <link href="https://unpkg.com/ionicons@4.1.0/dist/css/ionicons.min.css" rel="stylesheet">
    <link rel="stylesheet" href="../../public/css/uikit.min.css" type="text/css"/>
    <link rel="stylesheet" href="../../public/css/theme.css" type="text/css"/>
    <title>Olympiad</title>
</head>
<body>
<div id="app">
    @include('.layout.navbar')

    <div class="sc-slide-show">
        {{--@if($olympiad->cover != null)--}}
        <div class="uk-position-relative uk-visible-toggle uk-light"
             uk-slideshow="min-height: 300; max-height: 700; animation: push">
            <ul class="uk-slideshow-items">
                <li>
                    @if($olympiad->cover != null)
                        <div class="uk-position-cover uk-animation-kenburns uk-animation-reverse uk-transform-origin-center-left">
                            <img src="/storage/{{$olympiad->cover}}" alt="" uk-cover>
                        </div>
                    @else
                        <div class="uk-position-cover ">
                            <img src="{{asset('avdar_cover.png')}}" alt="" uk-cover>
                        </div>
                    @endif
                    <div class="uk-overlay uk-overlay-primary uk-position-bottom uk-text-center uk-transition-slide-bottom">
                        <h4 class="uk-margin-remove">Олимпиад эхлэхэд</h4>
                        {{--<h3 class="uk-margin-remove">Bottom</h3>--}}
                        <div class="uk-flex uk-flex-center uk-grid-small uk-child-width-auto" uk-grid
                             uk-countdown="date: {{date('Y-m-d', strtotime($olympiad->start_date))}}">
                            <div>
                                <div class="uk-countdown-number uk-countdown-days"></div>
                                <div class="uk-countdown-label uk-margin-small uk-text-center">Хоног</div>
                            </div>
                            {{--<div class="uk-countdown-separator">:</div>--}}
                            <div>
                                <div class="uk-countdown-number uk-countdown-hours"></div>
                                <div class="uk-countdown-label uk-margin-small uk-text-center">Цаг</div>
                            </div>
                            {{--<div class="uk-countdown-separator">:</div>--}}
                            <div>
                                <div class="uk-countdown-number uk-countdown-minutes"></div>
                                <div class="uk-countdown-label uk-margin-small uk-text-center">Минут</div>
                            </div>
                            {{--<div class="uk-countdown-separator">:</div>--}}
                            <div>
                                <div class="uk-countdown-number uk-countdown-seconds"></div>
                                <div class="uk-countdown-label uk-margin-small uk-text-center">Секунд</div>
                            </div>
                        </div>
                    </div>
                </li>
                @foreach($images as $image)
                    <li>
                        <div class="uk-position-cover uk-animation-kenburns uk-animation-reverse uk-transform-origin-center-left">
                            <img src="/storage/{{$image->path}}" alt="" uk-cover>
                        </div>
                    </li>
                @endforeach
            </ul>
            <a class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous
               uk-slideshow-item="previous"></a>
            <a class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next
               uk-slideshow-item="next"></a>
        </div>
        {{--@else--}}
        {{--<div class="uk-text-center uk-background-contain">--}}
        {{--<img src="{{asset('avdar_cover.png')}}">--}}
        {{--</div>--}}
        {{--@endif--}}
    </div>

    <div class="uk-margin-medium-top uk-margin-bottom">
        <div class="uk-container">
            <div class="uk-text-center" uk-grid>
                <div class="uk-width-expand@m">
                    <div class="uk-grid-divider uk-text-center uk-flex uk-flex-middle uk-visible@m" uk-grid>
                        <div class="uk-width-auto@m">
                            <a href="/organization/{{$olympiad->user_id}}">
                                @if($olympiad->logo != null)
                                    <img src="/storage/{{$olympiad->logo}}" height="100px"
                                         width="100px"></a>
                            @else
                                <img src="/storage/{{$organizer->image_path}}" height="100px"
                                     width="100px"></a>
                            @endif
                        </div>
                        <div class="uk-width-expand@m uk-text-left">
                            <h4 class="sc-text-default">{{$olympiad->title}}</h4>
                        </div>
                    </div>
                    <div class="uk-grid-divider uk-child-width-1-2 uk-margin-remove-top uk-text-left uk-flex uk-flex-middle uk-hidden@m"
                         uk-grid>
                        <div class="uk-width-auto">
                            <a href="/organization/{{$olympiad->user_id}}">
                                @if($olympiad->logo != null)
                                    <img src="/storage/{{$olympiad->logo}}"
                                         height="75px" width="75px"></a>
                            @else
                                <img src="/storage/{{$organizer->image_path}}"
                                     height="75px" width="75px"></a>
                            @endif
                        </div>
                        <div class="uk-text-left uk-width-expand">
                            <h4>{{$olympiad->title}}</h4>
                        </div>
                    </div>
                    <div class="uk-text-left uk-margin-top">
                        <h4 class="sc-text-default sc-text-400 uk-margin-remove-bottom">Тайлбар</h4>
                        <div class="sc-text-default sc-text-200 uk-margin-remove-top">
                            {!! $olympiad->description !!}
                        </div>
                        <h4 class="sc-text-default sc-text-400 uk-margin-remove-bottom">Зорилго</h4>
                        <div class="sc-text-default sc-text-200 uk-margin-remove-top">
                            {!! $olympiad->goal !!}
                        </div>
                        <h4 class="sc-text-default sc-text-400 uk-margin-remove-bottom">Шагнал урамшуулал</h4>
                        <div class="sc-text-default sc-text-200 uk-margin-remove-top">
                            {!! $olympiad->reward !!}
                        </div>
                        <h4 class="sc-text-default sc-text-400 uk-margin-remove-bottom">Шаардлага</h4>
                        <div class="sc-text-default sc-text-200 uk-margin-remove-top uk-margin-bottom">
                            {!! $olympiad->requirement !!}
                        </div>
                        <h4 class="sc-text-default sc-text-400 uk-margin-remove-bottom">Хаяг</h4>
                        <div class="sc-text-default sc-text-200 uk-margin-remove-top">
                            {!! $olympiad->address !!}
                        </div>
                        <h4 class="sc-text-default sc-text-400 uk-margin-remove-bottom">Зоон</h4>
                        <div class="uk-margin-remove-top uk-margin-bottom">
                            <div class="uk-overflow-auto">
                                {{--<table class="uk-table uk-table-small uk-table-divider">--}}
                                {{--<thead>--}}
                                {{--<tr>--}}
                                {{--<th class="sc-text-default sc-text-400">#</th>--}}
                                {{--<th class="sc-text-default sc-text-400">Хамрах хүрээ</th>--}}
                                {{--<th class="sc-text-default sc-text-400">Хүйс</th>--}}
                                {{--<th class="sc-text-default sc-text-400">Огноо</th>--}}
                                {{--<th class="sc-text-default sc-text-400">Эхлэх цаг</th>--}}
                                {{--<th class="sc-text-default sc-text-400">Оролцогчийн тоо</th>--}}
                                {{--<th class="sc-text-default sc-text-400">Төлөв</th>--}}
                                {{--</tr>--}}
                                {{--</thead>--}}
                                {{--<tbody>--}}
                                {{--@foreach($zones as $key=>$zone)--}}
                                {{--@if($mandate)--}}
                                {{--@if($zone->id == $currentZone->id)--}}
                                {{--<tr bgcolor="#c4dff6">--}}
                                {{--@else--}}
                                {{--<tr>--}}
                                {{--@endif--}}
                                {{--@endif--}}
                                {{--<td class="sc-text-default sc-text-200">{{$key + 1}}</td>--}}
                                {{--@if($zone->min == $zone->max)--}}
                                {{--<td class="sc-text-default sc-text-200">{{$zone->min . ' '.$zone->type}}</td>--}}
                                {{--@else--}}
                                {{--<td class="sc-text-default sc-text-200">{{$zone->min . '-'. $zone->max. ' '.$zone->type}}</td>--}}
                                {{--@endif--}}
                                {{--<td class="sc-text-default sc-text-200">{{$zone->gender}}</td>--}}
                                {{--<td class="sc-text-default sc-text-200">{!! substr($zone->start_date, 0, 10) !!}</td>--}}
                                {{--<td class="sc-text-default sc-text-200">{!! substr($zone->start_date, 10, 15) !!}</td>--}}
                                {{--<td class="sc-text-default sc-text-200">{{$zone->count . '/' . $zone->limit}}</td>--}}
                                {{--@if($zone->status_id == 3)--}}
                                {{--<td><a href="/score/{{$zone->id}}" target="_blank"--}}
                                {{--class="sc-text-200 sc-link-default">Дүн харах</a></td>--}}
                                {{--@elseif($zone->status_id == 2)--}}
                                {{--<td class="uk-text-primary sc-text-200">Ирц баталгаажсан</td>--}}
                                {{--@elseif($zone->status_id == 1)--}}
                                {{--<td class="uk-text-danger sc-text-200">Эхлээгүй</td>--}}
                                {{--@endif--}}
                                {{--</tr>--}}
                                {{--@endforeach--}}
                                {{--</tbody>--}}
                                {{--</table>--}}
                                <table class="uk-table uk-table-small uk-table-divider">
                                    <thead>
                                    <tr>
                                        <th class="sc-text-default sc-text-400">#</th>
                                        <th class="sc-text-default sc-text-400">Хамрах хүрээ</th>
                                        <th class="sc-text-default sc-text-400">Хүйс</th>
                                        <th class="sc-text-default sc-text-400">Огноо</th>
                                        <th class="sc-text-default sc-text-400">Эхлэх цаг</th>
                                        <th class="sc-text-default sc-text-400">Оролцогчийн тоо</th>
                                        <th class="sc-text-default sc-text-400">Төлөв</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {{--<tr bgcolor="#c4dff6">--}}
                                    <tr :bgcolor="checkIsRegistered(zone.id)" v-for="(zone, index) in zones">
                                        <td class="sc-text-default sc-text-200">@{{ index + 1 }}</td>
                                        <td v-if="zone.min == zone.max" class="sc-text-default sc-text-200">
                                            @{{ zone.min + ' ' + zone.type }}
                                        </td>
                                        <td v-else class="sc-text-default sc-text-200">
                                            @{{ zone.min + ' ' + zone.max + ' ' + zone.type}}
                                        </td>
                                        <td class="sc-text-default sc-text-200">
                                            @{{ zone.gender }}
                                        </td>
                                        <td class="sc-text-default sc-text-200">
                                            @{{ zone.start_date.substring(0, 10) }}
                                        </td>
                                        <td class="sc-text-default sc-text-200">
                                            @{{ zone.start_date.substring(10, 19) }}
                                        </td>
                                        <td class="sc-text-default sc-text-200">
                                            @{{ zone.count + ' / ' + zone.limit }}
                                        </td>
                                        <td v-if="zone.status_id == 3" class="sc-text-default sc-text-200">
                                            <a :href="'/score/' + zone.id" target="_blank"
                                               class="sc-text-200 sc-link-default">Дүн харах</a></td>
                                        </td>
                                        <td v-else-if="zone.status_id == 2" class="uk-text-primary sc-text-200">
                                            Ирц баталгаажсан
                                        </td>
                                        <td v-else-if="zone.status_id == 1" class="uk-text-danger sc-text-200">
                                            Эхлээгүй
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="uk-text-center uk-child-width-1-3@m" uk-grid uk-lightbox="animation: slide">
                            @foreach($guidances as $key=>$guidance)
                                <div>
                                    <a class="uk-inline sc-text-200" href="/storage/{{$guidance->path}}"
                                       data-caption="{{'Удирдамж ' . ($key+1)}}">
                                        <img src="/storage/{{$guidance->path}}" alt="">
                                    </a>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="uk-width-1-3@m">
                    <div style="border-style: solid solid solid solid; border-width: 1px; border-color: grey"
                         class="uk-card uk-card-default uk-card-body">
                        <div class="uk-flex uk-flex-middle uk-margin-remove-bottom">
                            <i class="sc-text-default icon ion-md-compass uk-margin-right"></i>
                            <span class="sc-text-default sc-text-200 sc-text-size-18 uk-text-meta">Улаанбаатар/Монгол</span><br>
                        </div>
                        <hr class="uk-margin-remove-top">
                        <div class="uk-flex uk-flex-middle uk-margin-remove-bottom">
                            &nbsp;<i class="sc-text-default icon ion-md-bookmark uk-margin-right"></i>
                            <span class="sc-text-default sc-text-200 sc-text-size-18 uk-text-meta">{{$olympiad->lesson_name}}</span><br>
                        </div>
                        <hr class="uk-margin-remove-top">
                        <div class="uk-flex uk-flex-middle uk-margin-remove-bottom">
                            <i class="sc-text-default icon ion-md-pricetag uk-margin-right"></i>
                            <span class="sc-text-default sc-text-200 sc-text-size-18 uk-text-meta"><b>{{$olympiad->mandate_cost. ' ₮'}}</b></span><br>
                        </div>
                        <hr class="uk-margin-remove-top">
                        <div class="uk-flex uk-flex-middle uk-margin-remove-bottom">
                            <i class="sc-text-default icon ion-md-calendar uk-margin-right"></i>
                            <span class="sc-text-default sc-text-200 sc-text-size-18 uk-text-meta">Бүртгэл дуусах: {{date('Y-m-d', strtotime($olympiad->register_end_date))}}</span>
                            <br>
                        </div>
                        <hr class="uk-margin-remove-top">
                        <div class="uk-flex uk-flex-middle uk-margin-remove-bottom">
                            <i class="sc-text-default icon ion-ios-calendar uk-margin-right"></i>
                            <span class="sc-text-default sc-text-200 sc-text-size-18 uk-text-meta">Эхлэх: {{date('Y-m-d', strtotime($olympiad->start_date))}}</span><br>
                        </div>
                        <hr class="uk-margin-remove-top">
                        @if($olympiad->show_mobile)
                            <div class="uk-flex uk-flex-middle uk-margin-remove-bottom">
                                <i class="sc-text-default icon ion-ios-call uk-margin-right"></i>
                                <span class="sc-text-default sc-text-200 sc-text-size-18 uk-text-meta">+976 - {{$organizer->mobile_phone}}</span><br>
                            </div>
                            <hr class="uk-margin-remove-top">
                        @endif
                        <div>
                            @if(auth()->guest())
                                <a href="#login-modal" uk-toggle style="border-radius: 5px"
                                   class="sc-text-200 uk-width-1-1 uk-button-score uk-button">
                                    Бүртгүүлэх
                                </a>
                            @elseif(auth()->user())
                                @if(auth()->user()->role_id != 1 && auth()->user()->role_id != 2)
                                    <button href="#mandate-register-modal" uk-toggle style="border-radius: 5px"
                                            class="sc-text-200 uk-width-1-1 uk-button-score uk-button">
                                        Бүртгүүлэх
                                    </button>
                                    @if (session('error'))
                                        <div class="uk-alert-danger uk-margin-remove-bottom" uk-alert>
                                            <a class="uk-alert-close" uk-close></a>
                                            <p>{{ session('error') }}</p>
                                        </div>
                                    @endif
                                    @if (session('success'))
                                        <div class="uk-alert-success uk-margin-remove-bottom" uk-alert>
                                            <a class="uk-alert-close" uk-close></a>
                                            <p class="sc-text-200">{{ session('success') }}</p>
                                        </div>
                                    @endif
                                @endif
                            @endif
                        </div>
                        <div v-if="mandates" class="uk-margin-small-top">
                            <button v-if="mandates.length > 0" href="#modal-mandates" uk-toggle style="border-radius: 5px"
                                    class="sc-text-200 uk-width-1-1 uk-button-default uk-button">
                                Миний мандатууд
                            </button>
                        </div>
                    </div>
                    <div style="border-style: solid solid solid solid; border-width: 1px; border-color: grey"
                         class="olympiadContentSection uk-card uk-card-default uk-card-body uk-margin-top uk-text-left">
                        <h3 class="sc-text-default sc-text-400">{{$organizer->first_name}}</h3>
                        {{--<h3><b>{{auth()->user()->dob}}</b></h3>--}}
                        <a class="sc-text-default sc-link-default"
                           href="/organization/{{$olympiad->user_id}}">Зохион
                            байгуулагчын хуудас харах</a>
                    </div>
                    {{--@if($mandate)--}}
                    {{--<div style="border-style: solid solid solid solid; border-width: 1px; border-color: grey"--}}
                    {{--class="olympiadContentSection uk-card uk-card-default uk-card-body uk-margin-top uk-text-left uk-text-center">--}}
                    {{--<div class="uk-flex-center" uk-grid>--}}
                    {{--<div>--}}
                    {{--<p class="sc-text-default sc-text-200">Бодлого</p>--}}
                    {{--@if($currentZone->problem_pdf)--}}
                    {{--<a href="/{{$currentZone->problem_pdf}}"--}}
                    {{--target="_blank"><img--}}
                    {{--src="{{asset('file_pdf.svg')}}"--}}
                    {{--width="100px"></a>--}}
                    {{--@else--}}
                    {{--<img src="{{asset('file_unknown.svg')}}" width="100px">--}}
                    {{--@endif--}}
                    {{--</div>--}}
                    {{--<div>--}}
                    {{--<p class="sc-text-default sc-text-200">Бодолт</p>--}}
                    {{--@if($currentZone->solution_pdf)--}}
                    {{--<a href="/{{$currentZone->solution_pdf}}"--}}
                    {{--target="_blank"><img--}}
                    {{--src="{{asset('file_pdf.svg')}}"--}}
                    {{--width="100px"></a>--}}
                    {{--@else--}}
                    {{--<img src="{{asset('file_unknown.svg')}}" width="100px">--}}
                    {{--@endif--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--@endif--}}
                </div>
            </div>
        </div>
    </div>

    @include('.layout.footer')
    @include('.layout.login_modal')
    @include('.layout.offcanvas')

    @include('.layout.mandate_register_modal')
    @include('.layout.mandates')
</div>
</body>
<script src="{{asset('js/uikit.min.js')}}"></script>
<script src="{{asset('js/uikit-icons.min.js')}}"></script>
<script src="{{asset('js/vuejs.js')}}"></script>
<script src="https://cdn.jsdelivr.net/npm/vue-resource@1.3.6"></script>
<script>
    var app = new Vue({
        el: '#app',
        data: {
            email: null,
            password: null,
            loginError: null,
            mandates: null,
            zones: null,
            user: null,
        },
        created: function () {
            this.getZones();
            @if (auth()->check())
                this.getData();
            @endif
        },
        methods: {
            login: function () {
                this.loginError = null;
                var jsonObject = JSON.stringify({email: this.email, password: this.password});
                this.$http.post('/login', jsonObject).then(function (data) {
                    location.reload();
                }, function (error) {
                    this.loginError = error.body.responseMessage;
                });
            },
            logout: function () {
                this.$http.get('/logout').then(function () {
                    localStorage.removeItem('user');
                    location.reload();
                })
            },
            check: function () {
                if (document.getElementById('checkBox').checked)
                    document.getElementById('confirm').disabled = false;
                else document.getElementById('confirm').disabled = true;
            },
            getZones: function () {
                var pathArray = window.location.pathname.split('/');
                this.$http.get('/rest/olympiad/zones/' + pathArray[pathArray.length - 1]).then(function (data) {
                    this.zones = data.body;
                })
            },
            getData: function () {
                var pathArray = window.location.pathname.split('/');
                this.$http.get('/rest/olympiad/' + pathArray[pathArray.length - 1] + '/data').then(function (data) {
                    this.mandates = data.body.mandates;
                    console.log(this.mandates);
                    this.user = data.body.user;
                })
            },
            checkIsRegistered: function (id) {
                var color = '#ffffff';
                if (this.mandates != null) {
                    this.mandates.forEach(function (mandate) {
                        if (mandate.olympiad_zone_id == id) {
                            color = '#c4dff6';
                            return color;
                        }
                    });
                }
                return color;
            },
            copy: function (event) {
                console.log(document.getElementById('val' + event.target.id).innerText);
            }
        }
    })
</script>
</html>