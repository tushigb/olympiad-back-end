<!doctype html>
<html lang="{{ app()->getLocale() }}" xmlns:v-on="http://www.w3.org/1999/xhtml"
      xmlns:v-bind="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,400,700&amp;subset=cyrillic,cyrillic-ext"
          rel="stylesheet">
    <link rel="stylesheet" href="{{asset('css/theme.css')}}" type="text/css"/>
    <link rel="stylesheet" href="{{asset('css/uikit.css')}}" type="text/css"/>
    <link rel="stylesheet" href="{{asset('css/ionicons.min.css')}}">
    <link rel="stylesheet" href="../../public/css/uikit.min.css" type="text/css"/>
    <link rel="stylesheet" href="../../public/css/theme.css" type="text/css"/>
    <title>Olympiad</title>
</head>
<body>
<div id="app">
    @include('.layout.navbar')
    <div class="uk-section">
        <div class="uk-container">
            <div class="uk-overflow-auto">
                <div>
                    <h3 class="sc-text-brand sc-text-400">Оролцогчдын дүнгийн жагсаалт</h3>
                    <ul class="uk-pagination" uk-margin>
                        <li><a class="sc-text-default sc-text-200" v-on:click="getPagination(pagination.prev_page_url)"
                            ><span uk-pagination-previous></span></a></li>
                        <li><a class="sc-text-200"
                               v-on:click="getPagination('http://localhost:8000/mandates/zone/6/scores?page=' + 1)"
                               v-bind:class="{'sc-text-brand': pagination.current_page == 1,
                           'sc-text-default': pagination.current_page != 1}"><span>1</span></a>
                        </li>
                        <template>
                            <li v-if="pagination.last_page > 10">
                                <span class="sc-text-default sc-text-200 uk-disabled">...</span></li>
                            <template v-for="n in pagination.last_page">
                                <li v-if="n > 1 && n < pagination.last_page"><a
                                            class="sc-text-200"
                                            v-on:click="getPagination('http://localhost:8000/mandates/zone/6/scores?page=' + n)"
                                            v-bind:class="{'sc-text-brand': pagination.current_page == n,
                           'sc-text-default': pagination.current_page != n}">@{{ n }}</a></li>
                            </template>
                            <li v-if="pagination.last_page > 10">
                                <span class="sc-text-default sc-text-200 uk-disabled">...</span></li>
                        </template>
                        <li v-if="pagination.last_page > 1"><a class="sc-text-200"
                                                               v-on:click="getPagination('http://localhost:8000/mandates/zone/6/scores?page=' + pagination.last_page)"
                                                               v-bind:class="{'sc-text-brand': pagination.current_page == pagination.last_page,
                                          'sc-text-default': pagination.current_page != pagination.last_page}">@{{
                                pagination.last_page }}</a></li>
                        <li><a class="sc-text-default sc-text-200"
                               v-on:click="getPagination(pagination.next_page_url)"><span
                                        uk-pagination-next></span></a></li>
                    </ul>
                    <table class="uk-table uk-table-small uk-table-divider">
                        <thead>
                        <tr>
                            <th class="sc-text-default sc-text-400">#</th>
                            {{--<th class="sc-text-default sc-text-400">Овог</th>--}}
                            <th class="sc-text-default sc-text-400">Нэр</th>
                            <th class="sc-text-default sc-text-400">Сургууль</th>
                            <th class="sc-text-default sc-text-400">Анги</th>
                            @foreach($problems as $problem)
                                <th class="sc-text-default sc-text-400">{{$problem->name}}</th>
                            @endforeach
                            <th class="sc-text-default sc-text-400">Нийт</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr v-for="(mandate, i) in mandates">
                            <td class="sc-text-default sc-text-200">@{{ i + 1 }}</td>
                            <td v-if="mandate.last_name.substring(0,2) != 'ал' && mandate.last_name.substring(0,2) != 'al'"
                                class="sc-text-default sc-text-200"><span class="uk-text-capitalize">@{{ mandate.last_name.substring(0,2) }}<span
                                            class="uk-text-uppercase">@{{ '. ' + mandate.first_name }}</span></span>
                            </td>
                            <td v-if="mandate.last_name.substring(0,2) == 'ал' || mandate.last_name.substring(0,2) == 'al'"
                                class="sc-text-default sc-text-200"><span class="uk-text-capitalize">@{{ mandate.last_name.substring(0,3) }}<span
                                            class="uk-text-uppercase">@{{ '. ' + mandate.first_name }}</span></span>
                            </td>
                            <td class="sc-text-default sc-text-200">@{{ mandate.school }}</td>
                            <td class="sc-text-default sc-text-200">@{{ mandate.class + mandate.class_group }}</td>
                            <td v-for="mark in mandate.marks" class="sc-text-default sc-text-200">@{{ mark.score }}</td>
                            <td class="sc-text-default sc-text-200">@{{ mandate.total }}</td>
                        </tr>
                        </tbody>
                    </table>
                    <div style="display: block;" id="spinner" class="uk-text-center">
                        <div style="color: #00AB8E" uk-spinner="ratio: 1.5"></div>
                    </div>
                    <ul class="uk-pagination" uk-margin>
                        <li><a class="sc-text-default sc-text-200" v-on:click="getPagination(pagination.prev_page_url)"
                            ><span uk-pagination-previous></span></a></li>
                        <li><a class="sc-text-200"
                               v-on:click="getPagination('http://localhost:8000/mandates/zone/6/scores?page=' + 1)"
                               v-bind:class="{'sc-text-brand': pagination.current_page == 1,
                           'sc-text-default': pagination.current_page != 1}"><span>1</span></a>
                        </li>
                        <template>
                            <li v-if="pagination.last_page > 10">
                                <span class="sc-text-default sc-text-200 uk-disabled">...</span></li>
                            <template v-for="n in pagination.last_page">
                                <li v-if="n > 1 && n < pagination.last_page"><a
                                            class="sc-text-200"
                                            v-on:click="getPagination('http://localhost:8000/mandates/zone/6/scores?page=' + n)"
                                            v-bind:class="{'sc-text-brand': pagination.current_page == n,
                           'sc-text-default': pagination.current_page != n}">@{{ n }}</a></li>
                            </template>
                            <li v-if="pagination.last_page > 10">
                                <span class="sc-text-default sc-text-200 uk-disabled">...</span></li>
                        </template>
                        <li v-if="pagination.last_page > 1"><a class="sc-text-200"
                                                               v-on:click="getPagination('http://localhost:8000/mandates/zone/6/scores?page=' + pagination.last_page)"
                                                               v-bind:class="{'sc-text-brand': pagination.current_page == pagination.last_page,
                                          'sc-text-default': pagination.current_page != pagination.last_page}">@{{
                                pagination.last_page }}</a></li>
                        <li><a class="sc-text-default sc-text-200"
                               v-on:click="getPagination(pagination.next_page_url)"><span
                                        uk-pagination-next></span></a></li>
                    </ul>
                </div>
                <hr class="uk-divider-icon">
                <div>
                    <h3 class="sc-text-brand sc-text-400">Сургуулийн дундаж оноо</h3>
                    <table class="uk-table uk-table-small uk-table-divider">
                        <thead>
                        <tr>
                            <th class="sc-text-default sc-text-400">#</th>
                            {{--<th class="sc-text-default sc-text-400">Овог</th>--}}
                            <th class="sc-text-default sc-text-400">Сургууль</th>
                            <th class="sc-text-default sc-text-400">Оролцогчийн тоо</th>
                            <th class="sc-text-default sc-text-400">Дундаж оноо</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr v-for="(school, i) in schools">
                            <td class="sc-text-default sc-text-200">@{{ i + 1 }}</td>
                            <td class="sc-text-default sc-text-200">@{{ school.school_name }}</td>
                            <td class="sc-text-default sc-text-200">@{{ school.participant_count }}</td>
                            <td class="sc-text-default sc-text-200">@{{ school.avg_score.substring(0,4) }}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    @include('.layout.footer')
    @include('.layout.login_modal')
    @include('.layout.offcanvas')
</div>
</body>
<script src="{{asset('js/uikit.min.js')}}"></script>
<script src="{{asset('js/uikit-icons.min.js')}}"></script>
<script src="{{asset('js/vuejs.js')}}"></script>
<script src="https://cdn.jsdelivr.net/npm/vue-resource@1.3.6"></script>
<script>
    var app = new Vue({
        el: '#app',
        data: {
            message: 'Hello Vue!',
            email: null,
            password: null,
            loginError: null,
            mandates: [],
            schools: [],
            pagination: {}
        },
        created: function () {
            var pathArray = window.location.pathname.split('/');
            this.getMarks('/mandates/zone/' + pathArray[2] + '/scores');
            this.getSchoolAvgMarks('/mandates/zone/' + pathArray[2] + '/school/avg');
        },
        methods: {
            login: function () {
                this.loginError = null;
                var jsonObject = JSON.stringify({email: this.email, password: this.password});
                this.$http.post('/login', jsonObject).then(function (data) {
                    location.reload();
                }, function (error) {
                    this.loginError = error.body.responseMessage;
                });
            },
            logout: function () {
                this.$http.get('/logout').then(function () {
                    localStorage.removeItem('user');
                    location.reload();
                })
            },
            getMarks: function (url) {
                this.$http.get(url).then(function (data) {
                    document.getElementById('spinner').style.display = 'none';
                    this.mandates = [];
                    this.mandates = data.body.data;
                    this.pagination.path = data.body.path;
                    this.pagination.next_page_url = data.body.next_page_url;
                    this.pagination.prev_page_url = data.body.prev_page_url;
                    this.pagination.last_page = data.body.last_page;
                    this.pagination.current_page = data.body.current_page;
                })
            },
            getPagination: function (url) {
                var current_page;
                if (url != null) {
                    current_page = url.substring(url.length, url.length - 1);
                    if (current_page != this.pagination.current_page) {
                        document.getElementById('spinner').style.display = 'block';
                        this.getMarks(url);
                    }
                }
            },
            getSchoolAvgMarks(url) {
                this.$http.get(url).then(function (data) {
                    this.schools = data.body;
                    console.log(this.schools);
                })
            }
        }
    })
</script>
</html>