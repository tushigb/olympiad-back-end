<div id="modal-contact" class="uk-modal-container" uk-modal>
    <div class="uk-modal-dialog">
        <div>
            <div style="border-style: none none solid none; border-width: 1px; border-color: #f3f4f7;"
                 class="uk-grid-collapse uk-child-width-1-2@s uk-flex-middle uk-grid" uk-grid>
                <div uk-scrollspy="cls: uk-animation-fade;" class="uk-background-cover uk-first-column"
                     style="background-image: url({{asset('images/contact_image.png')}}); box-sizing: border-box; min-height: 50vh; height: 50vh;"
                ></div>
                <div class="uk-padding-large">
                    <div uk-scrollspy="cls: uk-animation-slide-right-medium; repeat: true; delay: 300"
                         class="uk-visible@l">
                        <h1 class="uk-text-uppercase sc-text-default sc-text-400">Холбоо
                            барих</h1>
                        <h3 class="sc-text-default sc-text-200 uk-margin-remove-top uk-margin-remove-bottom">
                            <span class="ion-ios-email-outline"></span>&nbsp;contact@techavdar.com
                        </h3>
                        <h3 class="sc-text-default sc-text-200 uk-margin-remove-top">
                            <span class="ion-ios-telephone-outline"></span>&nbsp;8888-6009
                        </h3>
                    </div>
                    <div uk-scrollspy="cls: uk-animation-slide-right-medium;" class="uk-hidden@l">
                        <h3 class="sc-text-default sc-text-400 uk-margin-remove-bottom">Холбоо барих</h3>
                        <h4 class="sc-text-default sc-text-200 uk-margin-remove-top uk-margin-remove-bottom">
                            <span class="ion-ios-email-outline sc-text-brand"></span>&nbsp;contact@techavdar.com
                        </h4>
                        <h4 class="sc-text-default sc-text-200 uk-margin-remove-top">
                            <span class="ion-ios-telephone-outline"></span>&nbsp;contact@techavdar.com
                        </h4>
                    </div>
                </div>
            </div>
        </div>
        <div class="uk-modal-body">
            <div class="uk-container uk-section-small">
                <div class="uk-child-width-1-2@m" uk-grid>
                    <div uk-scrollspy="cls: uk-animation-slide-left-medium;"
                         class="uk-text-right uk-margin-small-bottom">
                        <h3 class="sc-text-brand sc-text-200 uk-text-left">Бидэнд сэтгэгдэл илгээхийг хүсвэл</h3>
                        <form class="uk-form-stacked">
                            <div class="uk-margin">
                                <label class="sc-text-default sc-text-200 uk-form-label uk-text-left">Нэр *</label>
                                <div class="uk-form-controls">
                                    <input class="sc-text-200 uk-input" id="form-stacked-text" type="text"
                                           placeholder="Таны нэр">
                                </div>
                            </div>
                            <div class="uk-margin">
                                <label class="sc-text-default sc-text-200 uk-form-label uk-text-left">Байгууллага</label>
                                <div class="uk-form-controls">
                                    <input class="sc-text-200 uk-input" id="form-stacked-text" type="text"
                                           placeholder="Байгууллага">
                                </div>
                            </div>
                            <div class="uk-margin">
                                <label class="sc-text-default sc-text-200 uk-form-label uk-text-left">Имэйл *</label>
                                <div class="uk-form-controls">
                                    <input class="sc-text-200 uk-input" id="form-stacked-text" type="text"
                                           placeholder="Имэйл хаяг">
                                </div>
                            </div>
                            <div class="uk-margin">
                                <label class="sc-text-default sc-text-200 uk-form-label uk-text-left">Утас *</label>
                                <div class="uk-form-controls">
                                    <input class="sc-text-200 uk-input" id="form-stacked-text" type="text"
                                           placeholder="Утасны дугаар">
                                </div>
                            </div>
                            <div class="uk-margin">
                                <label class="sc-text-default sc-text-200 uk-form-label uk-text-left">Сэтгэгдэл</label>
                                <div class="uk-form-controls">
                                    <div class="uk-margin">
                                            <textarea class="sc-text-200 uk-textarea" rows="3"
                                                      placeholder="Таны сэтгэгдэл"></textarea>
                                    </div>
                                </div>
                            </div>
                            <button style="border-radius: 5px;"
                                    class="sc-text-200 uk-button-score uk-button">
                                Сэтгэгдэл үлдээх
                            </button>
                        </form>
                    </div>
                    <hr class="uk-hidden@m">
                    <div uk-scrollspy="cls: uk-animation-slide-right-medium;">
                        <h3 class="sc-text-brand sc-text-200">Хамтран ажиллах</h3>
                        <p class="sc-text-default sc-text-200">Олимпиад зохион байгуулагчдын хувьд түгээмэл тулгардаг
                            олон асуудлыг olympiad.edu.mn сайт
                            технологийн дэвшлийг ашиглан шийдэж байна. Хэрвээ та олимпиад зохион байгуулдаг эсвэл зохион
                            байгуулах хүсэлтэй хувь хүн, албан байгууллага бол бид танд хамтран ажиллах, дэлхийн
                            стандартад нийцсэн олимпиад зохион байгууллахад шаардлагатай технологиийг санал болгож
                            байна. </p>
                        <p class="sc-text-default sc-text-200 uk-margin-remove-bottom">
                            Бидний танд санал болгож үйлчилгээ, Үүнд:
                        </p>
                        <ul class="">
                            <li class="sc-text-default ">
                                <p class="sc-text-default sc-text-200 uk-margin-remove-bottom">Олимпиадын мэдээлэл</p>
                            </li>
                            <li class="sc-text-default">
                                <p class="sc-text-default sc-text-200 uk-margin-remove-top uk-margin-remove-bottom">
                                    Цахим бүртгэлийн систем</p>
                            </li>
                            <li class="sc-text-default">
                                <p class="sc-text-default sc-text-200 uk-margin-remove-top uk-margin-remove-bottom">
                                    Цахим төлбөр, тооцоо </p>
                            </li>
                            <li class="sc-text-default">
                                <p class="sc-text-default sc-text-200 uk-margin-remove-top uk-margin-remove-bottom">
                                    Цахим мандат</p>
                            </li>
                            <li class="sc-text-default">
                                <p class="sc-text-default sc-text-200 uk-margin-remove-top uk-margin-remove-bottom">Ирц
                                    бүртгэл </p>
                            </li>
                            <li class="sc-text-default">
                                <p class="sc-text-default sc-text-200 uk-margin-remove-top uk-margin-remove-bottom">
                                    Автомат дүнгийн илгээлт</p>
                            </li>
                            <li class="sc-text-default">
                                <p class="sc-text-default sc-text-200 uk-margin-remove-top uk-margin-remove-bottom">
                                    Цахим дүнгийн мэдээлэл</p>
                            </li>
                        </ul>
                        <hp class="sc-text-200 uk-text-left"><a href="http://techavdar.com/" target="_blank"
                                                                class="sc-link-brand">ТЕК АВДАР ХХК</a></hp>
                    </div>
                </div>
            </div>
        </div>
        <div class="mapouter">
            <div class="gmap_canvas">
                <iframe width="100%" height="100%" id="gmap_canvas"
                        src="https://maps.google.com/maps?q=Тусгаар тогтнолын ордон&t=&z=15&ie=UTF8&iwloc=&output=embed"
                        frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
            </div>
            {{--<a href="https://www.crocothemes.net"></a>--}}
        </div>
    </div>
</div>