<div id="modal-service" class="uk-modal-container" uk-modal>
    <div class="uk-modal-dialog uk-modal-body tm-background uk-cover-background" style="background-image: url({{asset('images/bg.svg')}}); background-repeat: no-repeat;">
        <div>
            <div class="uk-container uk-section">
                <div uk-scrollspy="cls: uk-animation-slide-left-medium; repeat: true; delay: 300"
                     class="uk-width-1-1@m uk-margin-large-bottom">
                    <h1 class="sc-text-brand sc-text-200">
                        ҮЙЛЧИЛГЭЭ
                    </h1>
                    <hr class="uk-divider-small">
                    <p class="sc-text-default sc-text-200 uk-width-1-2@l uk-width-2-3@m uk-text-justify">
                        Та олимпиад зохион байгуулах гэж байна уу? Тэгвэл та хамгийн зөв газараа олж ирсэн байна. Та бидэнд
                        бүх
                        ажилаа даатга учир нь бид танд тулгардаг зохион байгуулалтын хамгийн хүндрэлтэй бүх асуудлыг шийдэж
                        өгөх
                        болно. Үүнд:
                    </p>
                </div>
                <div uk-scrollspy="cls: uk-animation-slide-left-medium; repeat: true; delay: 300" class="uk-grid" uk-grid>
                    <div class="uk-width-3-4@m">
                        <div class="uk-card uk-card-default uk-grid-collapse uk-child-width-1-2@s uk-margin" uk-grid>
                            <div class="uk-card-media-left uk-cover-container">
                                <img src="{{asset('light.jpg')}}" alt="" uk-cover>
                                <canvas width="600" height="400"></canvas>
                            </div>
                            <div>
                                <div class="uk-card-body uk-margin-xlarge-bottom">
                                    <h3 class="uk-card-title sc-text-brand sc-text-200">
                                        Олимпиадын мэдээлэл түгээх
                                    </h3>
                                    <p class="sc-text-default sc-text-200">
                                        Албан ёсны ebs.edu.mn сайт дээр өөрийн зохион байгуулах олимпиадын мэдээллийг
                                        байршуулах, олон нийтэд түгээх боломжтой.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="uk-width-expand@m">

                    </div>
                </div>
                <div uk-scrollspy="cls: uk-animation-slide-right-medium; repeat: true; delay: 300" class="uk-grid" uk-grid>
                    <div class="uk-width-expand@m">

                    </div>
                    <div class="uk-width-3-4@m">
                        <div class="uk-card uk-card-default uk-grid-collapse uk-child-width-1-2@s uk-margin" uk-grid>
                            <div class="uk-card-media-left uk-cover-container">
                                <img src="{{asset('light.jpg')}}" alt="" uk-cover>
                                <canvas width="600" height="400"></canvas>
                            </div>
                            <div>
                                <div class="uk-card-body uk-margin-xlarge-bottom">
                                    <h3 class="uk-card-title sc-text-brand sc-text-200">
                                        Бүртгэлийн систем
                                    </h3>
                                    <p class="sc-text-default sc-text-200">
                                        Сайт дээр бүртгэгдсэн олимпиадын мэдээллээр ороцох хүсэлтэй хэрэглэчид цахимаар
                                        бүртгүүлэх болмжтой.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div uk-scrollspy="cls: uk-animation-slide-left-medium; repeat: true; delay: 300" class="uk-grid" uk-grid>
                    <div class="uk-width-3-4@m">
                        <div class="uk-card uk-card-default uk-grid-collapse uk-child-width-1-2@s uk-margin" uk-grid>
                            <div class="uk-card-media-left uk-cover-container">
                                <img src="{{asset('light.jpg')}}" alt="" uk-cover>
                                <canvas width="600" height="400"></canvas>
                            </div>
                            <div>
                                <div class="uk-card-body uk-margin-xlarge-bottom">
                                    <h3 class="uk-card-title sc-text-brand sc-text-200">
                                        Төлбөр тооцоо
                                    </h3>
                                    <p class="sc-text-default sc-text-200">
                                        Олимпиадад бүртгэгдсэн хэрэглэгчид онлайнаар Qpay(Khan pay, TDB pay) эсвэл Мобайл
                                        шилжүүлгээр төлбөр, тооцоогоо хийх боломжтой.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="uk-width-expand@m">

                    </div>
                </div>
                <div uk-scrollspy="cls: uk-animation-slide-right-medium; repeat: true; delay: 300" class="uk-grid" uk-grid>
                    <div class="uk-width-expand@m">

                    </div>
                    <div class="uk-width-3-4@m">
                        <div class="uk-card uk-card-default uk-grid-collapse uk-child-width-1-2@s uk-margin" uk-grid>
                            <div class="uk-card-media-left uk-cover-container">
                                <img src="{{asset('light.jpg')}}" alt="" uk-cover>
                                <canvas width="600" height="400"></canvas>
                            </div>
                            <div>
                                <div class="uk-card-body uk-margin-xlarge-bottom">
                                    <h3 class="uk-card-title sc-text-brand sc-text-200">Мандат</h3>
                                    <p class="sc-text-default sc-text-200">Мандатын төлбөрөө төлсөн хэрэглэгчдэд Олимпиадын
                                        систем автомаатаар тус хэрэглэгчийн
                                        мэдээлэлтэй QR code-той цахим мандат илгээх болно.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div uk-scrollspy="cls: uk-animation-slide-left-medium; repeat: true; delay: 300" class="uk-grid" uk-grid>
                    <div class="uk-width-3-4@m">
                        <div class="uk-card uk-card-default uk-grid-collapse uk-child-width-1-2@s uk-margin" uk-grid>
                            <div class="uk-card-media-left uk-cover-container">
                                <img src="{{asset('light.jpg')}}" alt="" uk-cover>
                                <canvas width="600" height="400"></canvas>
                            </div>
                            <div>
                                <div class="uk-card-body uk-margin-xlarge-bottom">
                                    <h3 class="uk-card-title sc-text-brand sc-text-200">
                                        Ирцийн бүртгэл
                                    </h3>
                                    <p class="sc-text-default sc-text-200">
                                        Ухаалаг гар утасны апп ашиглан оролцогчдын ирцийг мандатныхан QR code-ийг ашиглан
                                        бүртгэх боломжтой.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="uk-width-expand@m">

                    </div>
                </div>
                <div uk-scrollspy="cls: uk-animation-slide-right-medium; repeat: true; delay: 300" class="uk-grid" uk-grid>
                    <div class="uk-width-expand@m">

                    </div>
                    <div class="uk-width-3-4@m">
                        <div class="uk-card uk-card-default uk-grid-collapse uk-child-width-1-2@s uk-margin" uk-grid>
                            <div class="uk-card-media-left uk-cover-container">
                                <img src="{{asset('light.jpg')}}" alt="" uk-cover>
                                <canvas width="600" height="400"></canvas>
                            </div>
                            <div>
                                <div class="uk-card-body uk-margin-xlarge-bottom">
                                    <h3 class="uk-card-title sc-text-brand sc-text-200">
                                        Дүнгийн мэдээлэл илгээх
                                    </h3>
                                    <ul>
                                        <li class="sc-text-default sc-text-200">
                                            Дүн бүртгэл дуусхад оролцогчийн имэйл хаягруу дүнгийн мэдээлэл нь автоматаар
                                            илгээгдэх
                                            болно
                                        </li>
                                        <li class="sc-text-default sc-text-200">
                                            Olympiad.edu.mn сайт дээр дүнгийн мэдээлэл тавигдах болно
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div uk-scrollspy="cls: uk-animation-slide-left-medium; repeat: true; delay: 300" class="uk-grid" uk-grid>
                    <div class="uk-width-3-4@m">
                        <div class="uk-card uk-card-default uk-grid-collapse uk-child-width-1-2@s uk-margin" uk-grid>
                            <div class="uk-card-media-left uk-cover-container">
                                <img src="{{asset('light.jpg')}}" alt="" uk-cover>
                                <canvas width="600" height="400"></canvas>
                            </div>
                            <div>
                                <div class="uk-card-body uk-margin-xlarge-bottom">
                                    <h3 class="uk-card-title sc-text-brand sc-text-200">
                                        Хэвлэлийн үйлчилгээ
                                    </h3>
                                    <ul>
                                        <li class="sc-text-default sc-text-200">
                                            Шалгалтын материал
                                        </li>
                                        <li class="sc-text-default sc-text-200">
                                            Зохион байгуулагчдын мандат
                                        </li>
                                        <li class="sc-text-default sc-text-200">
                                            Оролцогчдын мандат
                                        </li>
                                        <li class="sc-text-default sc-text-200">
                                            Плакат
                                        </li>
                                        <li class="sc-text-default sc-text-200">
                                            Аннос
                                        </li>
                                    </ul>
                                    {{--<p class="sc-text-default sc-text-200">--}}
                                    {{--Шалгалтын материал--}}
                                    {{--</p>--}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="uk-width-expand@m">

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>