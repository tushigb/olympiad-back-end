<div id="modal-mandates" class="uk-modal-container" uk-modal>
    <div class="uk-modal-dialog uk-modal-body">
        <ul uk-accordion>
            <li v-for="(mandate, i) in mandates" class="uk-open">
                <a v-if="mandate.min == mandate.max" class="sc-text-200 sc-text-default uk-accordion-title" href="#">
                    @{{ mandate.min + ' ' + mandate.type}}
                </a>
                <a v-else class="sc-text-200 sc-text-default uk-accordion-title" href="#">
                    @{{ mandate.min + '-' + mandate.max + ' ' + mandate.type}}
                </a>
                <div class="uk-accordion-content">
                    <div v-if="!mandate.is_paid" class="uk-grid-collapse uk-child-width-1-2@s uk-flex-middle uk-grid"
                         uk-grid>
                        <div class="uk-padding-small uk-background-cover uk-first-column">
                            <div class="uk-text-center">
                                <img :src="'data:image/png;base64, ' + mandate.qr_image">
                            </div>
                        </div>
                        <div class="uk-padding-large">
                            <div class="uk-visible@l">
                                <div style="border-radius: 5px" class="uk-alert-danger uk-margin-remove-bottom"
                                     uk-alert>
                                    <a class="uk-alert-close" uk-close></a>
                                    <p class="sc-text-200">Үлдэгдэл: <b>@{{ mandate.payment }}₮</b></p>
                                </div>
                                <form method="post" :action="'/mandate/delete/'">
                                    <input name="mandate_id" :value="mandate.id" hidden>
                                    <button style="border-radius: 5px"
                                            class="sc-text-200 uk-width-1-1 uk-button-danger uk-button uk-margin-small-top">
                                        Цуцлах
                                    </button>
                                </form>
                            </div>
                            <div class="uk-hidden@l">
                                <div class="uk-alert-danger uk-margin-remove-bottom" uk-alert>
                                    <a class="uk-alert-close" uk-close></a>
                                    <p class="sc-text-200">Үлдэгдэл: <b>@{{ mandate.payment }}₮</b></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div v-else class="uk-text-center">
                        <iframe frameborder="0" class="uk-width-1-2" height="600px"
                                :src="'/participant/2/olympiad/1/mandate/' + mandate.id">
                        </iframe>
                    </div>
                </div>
            </li>
        </ul>
    </div>
</div>