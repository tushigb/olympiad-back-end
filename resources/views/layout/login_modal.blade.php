<div id="login-modal" uk-modal>
    <div class="uk-modal-dialog">
        <form v-on:submit.prevent="login">
            {{--<button class="uk-modal-close-default" type="button" uk-close></button>--}}
            <div class="uk-modal-header">
                <h2 class="sc-text-default sc-text-200 uk-modal-title">Нэвтрэх</h2>
            </div>
            <div class="uk-modal-body">
                <div class="uk-margin">
                    <label class="sc-text-default sc-text-200 uk-form-label" for="form-stacked-text">Имэйл</label>
                    <div class="uk-form-controls">
                        <input v-model="email" class="sc-text-default sc-text-200 uk-input" type="email"
                               placeholder="Имэйл">
                    </div>
                </div>
                <div class="uk-margin">
                    <label class="sc-text-default sc-text-200 uk-form-label" for="form-stacked-text">Нууц үг</label>
                    <div class="uk-form-controls">
                        <input v-model="password" class="sc-text-default sc-text-200 uk-input" type="password"
                               placeholder="Нууц үг">
                    </div>
                </div>
                <div v-if="loginError" class="uk-margin">
                    <div class="uk-form-controls">
                        <div class="uk-alert-danger" uk-alert>
                            <a class="uk-alert-close" uk-close></a>
                            <p class="sc-text-200">@{{ loginError }}</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="uk-modal-footer uk-text-right">
                <button style="border-radius: 5px" class="sc-text-200 uk-button uk-button-default uk-modal-close"
                        type="button">
                    Болих
                </button>
                <button style="border-radius: 5px" class="sc-text-200 uk-button uk-button-score" type="submit">Нэвтрэх
                </button>
            </div>
        </form>
    </div>
</div>