<div uk-sticky="" media="768" show-on-up="" animation="uk-animation-slide-top"
     cls-active="uk-navbar-sticky" sel-target=".uk-navbar-container"
     class="uk-sticky uk-sticky-below uk-sticky-fixed"
     style="position: fixed; top: 0px; width: 1654px; border-style: none none solid none; border-width: 0.15mm; border-color: #f3f4f7; background-color: #FFF"
     xmlns:v-on="http://www.w3.org/1999/xhtml">
    <div class="uk-container">
        <div uk-grid>
            <div class="uk-navbar-left">
                <div class="uk-flex uk-flex-middle">
                    <a href="/"><img src="{{asset('images/logo_avdar.png')}}" width="220px" class="uk-margin-right"></a>
                    {{--<p class="sc-text-brand uk-margin-remove-left uk-visible@s">OLYMPIAD</p>--}}
                </div>
            </div>
            <div class="uk-navbar-right uk-visible@s" uk-grid>
                <a href="/shop" class="sc-link-brand sc-text-200"><span class="icon ion-md-basket"></span>&nbsp;&nbsp;Дэлгүүр</a>
                @if(auth()->guest())
                    <a class="sc-link-brand sc-text-200" href="/dist/signup">Бүртгүүлэх</a>
                    <a class="sc-link-brand sc-text-200" href="#login-modal" uk-toggle>Нэвтрэх</a>
                @elseif(auth()->user())
                    <a class="sc-link-brand sc-text-200">{{auth()->user()->first_name}}</a>
                    <a class="sc-link-brand sc-text-200"><span class="menu ion-md-menu"></span></a>
                    <div uk-dropdown="animation: uk-animation-slide-top-small; duration: 500">
                        <ul class="uk-nav uk-dropdown-nav">
                            <a href="/dist/index" class="sc-link-brand sc-text-300" href="#">
                                <span class="icon ion-md-clipboard"></span>&nbsp;&nbsp;Удирдлагын самбар</a><br>
                            @if(auth()->user()->role_id == 2)
                                <a class="sc-link-brand sc-text-300" href="/organization/{{auth()->id()}}">
                                    <span class="icon ion-md-people"></span>&nbsp;&nbsp;Миний хуудас</a><br>
                            @else
                                <a class="sc-link-brand sc-text-300" href="/participant/{{auth()->id()}}">
                                    <span class="icon ion-md-people"></span>&nbsp;&nbsp;Миний хуудас</a><br>
                            @endif
                            <a href="/dist/user/profile" class="sc-link-brand sc-text-300" href="#">
                                <span class="icon ion-md-settings"></span>&nbsp;&nbsp;Профайл засах</a><br>
                            <a class="sc-link-brand sc-text-300" href="#">
                                <span class="icon ion-md-paper"></span>&nbsp;&nbsp;Файл</a><br>
                            <li class="uk-nav-divider"></li>
                            <a class="sc-link-brand sc-text-300" v-on:click="logout" href="#">
                                <span class="icon ion-md-log-out"></span>&nbsp;&nbsp;Гарах</a><br>
                        </ul>
                    </div>
                @endif
            </div>
            <div class="uk-navbar-right uk-hidden@s">
                <li><a class="sc-link-brand sc-text-200" uk-toggle="target: #offcanvas-overlay"
                       uk-icon="icon: menu"></a>
                </li>
            </div>
        </div>
    </div>
</div>