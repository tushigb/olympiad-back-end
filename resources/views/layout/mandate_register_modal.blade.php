<div id="mandate-register-modal" class="uk-modal-container" uk-modal>
    <div class="uk-modal-dialog">
        <form method="post" action="/olympiad/{{$olympiad->id}}">
            <div>
                <div style="border-style: none none solid none; border-width: 1px; border-color: #f3f4f7;"
                     class="uk-grid-collapse uk-child-width-1-2@m uk-flex-middle uk-grid" uk-grid>
                    <div class="uk-padding-large uk-first-column">
                        <div class="">
                            <a href="/organization/{{$olympiad->user_id}}">
                                @if($olympiad->logo != null)
                                    <img src="/storage/{{$olympiad->logo}}"
                                         height="100px" width="100px"></a>
                            @else
                                <img src="/storage/{{$organizer->image_path}}"
                                     height="100px" width="100px"></a>
                            @endif
                        </div>
                        <div uk-scrollspy="cls: uk-animation-slide-right-medium; repeat: true; delay: 300">
                            <h4 class="sc-text-brand sc-text-400">
                                {{$olympiad->title}}
                            </h4>
                            <div class="uk-margin-top uk-margin-bottom uk-grid-small" uk-grid>
                                <div>
                                    <p class="sc-text-default sc-text-200 uk-margin-small-right">
                                        <span class="icon ion-md-bookmark"></span>&nbsp;&nbsp;{{$olympiad->lesson_name}}
                                    </p>
                                </div>
                                <div>
                                    <p class="sc-text-default sc-text-200 uk-margin-small-right">
                                        <span class="icon ion-md-pricetag"></span>&nbsp;&nbsp;<b>{{$olympiad->mandate_cost. '₮'}}</b>
                                    </p>
                                </div>
                                <div>
                                    <p class="sc-text-default sc-text-200 uk-margin-small-right">
                                        <span class="icon ion-md-calendar"></span>&nbsp;&nbsp;Бүртгэл
                                        дуусах: {{date('Y-m-d', strtotime($olympiad->register_end_date))}}</p>
                                </div>
                                <div>
                                    <p class="sc-text-default sc-text-200 uk-margin-small-right">
                                        <span class="icon ion-ios-calendar"></span>&nbsp;&nbsp;Эхлэх: {{date('Y-m-d', strtotime($olympiad->start_date))}}
                                    </p>
                                </div>
                            </div>
                            <div class="sc-text-default sc-text-200 uk-margin-remove-top">
                                {!! $olympiad->address !!}
                            </div>
                        </div>
                    </div>
                    <div class="uk-padding-large">
                        <div uk-scrollspy="cls: uk-animation-slide-right-medium; repeat: true; delay: 300">
                            <p class="sc-text-default ">
                                <span class="ion-information-circled"></span>
                                Хэрэв та <b>анги/нас</b> ахиж олимпиадад оролцох бол доорх сонголтоос сонгоно уу!
                            </p>
                            @if(auth()->check())
                                <select class="sc-text-default sc-text-200 uk-select" name="zone_id">
                                    <template v-for="(zone, i) in zones">
                                        <template v-if="zone.zone_type_id == 1">
                                            <template v-if="zone.min != zone.max">
                                                <option v-if="zone.min <= user.class && zone.max >= user.class"
                                                        :id="zone.id" :value="zone.id">
                                                    @{{ zone.min + '-' + zone.max + ' ' + zone.type }}
                                                </option>
                                            </template>
                                            <template v-else>
                                                <option v-if="zone.max >= user.class"
                                                        :id="zone.id" :value="zone.id">@{{ zone.min + ' ' + zone.type }}
                                                </option>
                                            </template>
                                        </template>
                                        <template v-if="zone.zone_type_id == 2">
                                            <template v-if="zone.min != zone.max">
                                                <option v-if="zone.min <= user.age && zone.max >= user.age"
                                                        :id="zone.id" :value="zone.id">
                                                    @{{ zone.min + '-' + zone.max + ' ' + zone.type }}
                                                </option>
                                            </template>
                                            <template v-else>
                                                <option v-if="zone.max >= user.age"
                                                        :id="zone.id" :value="zone.id">
                                                    @{{ zone.min + ' ' + zone.type }}
                                                </option>
                                            </template>
                                        </template>
                                    </template>
                                </select>

                                {{--<select class="sc-text-default sc-text-200 uk-select" name="zone_id">--}}
                                {{--@foreach($zones as $key=>$zone)--}}
                                {{--@if($zone->zone_type_id == 1)--}}
                                {{--@if(intval($zone->min) != intval($zone->max))--}}
                                {{--@if(intval($zone->min) <= auth()->user()->class && intval($zone->max) >= auth()->user()->class)--}}
                                {{--<option id="{{$zone->id}}"--}}
                                {{--value="{{$zone->id}}">{{$zone->min . '-'. $zone->max. ' '.$zone->type}}</option>--}}
                                {{--@endif--}}
                                {{--@else--}}
                                {{--@if(intval($zone->max) >= auth()->user()->class)--}}
                                {{--<option id="{{$zone->id}}"--}}
                                {{--name="zone_id"--}}
                                {{--value="{{$zone->id}}">{{$zone->min . ' '.$zone->type}}</option>--}}
                                {{--@endif--}}
                                {{--@endif--}}
                                {{--@elseif($zone->zone_type_id == 2)--}}
                                {{--@if(intval($zone->min) != intval($zone->max))--}}
                                {{--@if(intval($zone->min) <= $age && intval($zone->max) >= $age)--}}
                                {{--<option id="{{$zone->id}}"--}}
                                {{--value="{{$zone->id}}">{{$zone->min . '-'. $zone->max. ' '.$zone->type}}</option>--}}
                                {{--@endif--}}
                                {{--@else--}}
                                {{--@if(intval($zone->max) >= $age)--}}
                                {{--<option id="{{$zone->id}}"--}}
                                {{--name="zone_id"--}}
                                {{--value="{{$zone->id}}">{{$zone->min . ' '.$zone->type}}</option>--}}
                                {{--@endif--}}
                                {{--@endif--}}
                                {{--@endif--}}
                                {{--@endforeach--}}
                                {{--</select>--}}
                            @endif
                        </div>
                        <p class="sc-text-default">
                            <span class="ion-information-circled"></span>
                            Төлбөрийн мэдээлэл баталгаажсаны дараа бид таны цахим мандатыг имэйл хаягаар илгээх болно.
                        </p>
                        <div class="uk-margin-top uk-flex uk-flex-middle" uk-grid>
                            <div class="uk-width-expand">
                                <label class="sc-text-default sc-text-200">
                                    <input id="checkBox" v-on:change="check()" class="uk-checkbox" type="checkbox">
                                    Үйлчилгээний нөхцөлийг зөвшөөрч байна
                                </label>
                            </div>
                            <div class="uk-width-1-2">
                                <button id="confirm" style="border-radius: 5px"
                                        class="sc-text-200 uk-width-1-1 uk-button-score uk-button" disabled>
                                    Бүртгүүлэх
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{--<div class="uk-modal-body">--}}
            {{--@if($mandate)--}}
            {{--<div class="uk-card uk-card-default uk-grid-collapse uk-child-width-1-2@s uk-margin"--}}
            {{--uk-grid>--}}
            {{--<div class="uk-card-media-left">--}}
            {{--<div class="uk-card-body">--}}
            {{--<h3 class="sc-text-default uk-card-title">Q-Pay</h3>--}}
            {{--<i class="sc-text-default ion-information-circled"></i>--}}
            {{--<span class="sc-text-default sc-text-size-14 uk-text-meta uk-margin-right uk-text-left">--}}
            {{--Та банкныхаа мобайл аппликейшнээр орж QR кодыг уншуулна уу.--}}
            {{--</span>--}}
            {{--<img class="uk-align-center" src="data:image/png;base64, {{$qr_image}}"--}}
            {{--width="175px">--}}
            {{--<img class="uk-align-center"--}}
            {{--src="data:image/png;base64,{!! base64_encode(QrCode::format('png')->errorCorrection('Q')->mergeString(Storage::get('olympiad_content/Xb2Nsm93DUdQkwUvdkh4wHZU4oUyGclrAlyP3tvy.jpeg'), .22)->size(300)->margin(1)->generate('sms:555-555-5555')) !!} ">--}}
            {{--olympiad_content/Xb2Nsm93DUdQkwUvdkh4wHZU4oUyGclrAlyP3tvy.jpeg--}}
            {{--77290102794489780767382802080669737119365015155449--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--<div>--}}
            {{--<div class="uk-card-body">--}}
            {{--<h3 class="sc-text-default uk-card-title">Шилжүүлгээр төлөх</h3>--}}
            {{--<i class="sc-text-default ion-information-circled"></i>--}}
            {{--<span class="sc-text-default sc-text-size-14 uk-text-meta uk-margin-right uk-text-left uk-margin-small-bottom">--}}
            {{--Гүйлгээ хийхдээ регистрийн дугаарын эхний 2 үсгийг <b>КИРИЛЛЭЭР</b> бичихииг анхаарна уу--}}
            {{--</span>--}}
            {{--<div class="uk-margin-small-top">--}}
            {{--<p class="sc-text-default uk-margin-remove-bottom uk-text-meta">Төлөлт хийх--}}
            {{--банк: <b>Голомт--}}
            {{--банк</b>--}}
            {{--</p>--}}
            {{--<p class="uk-margin-remove-top uk-margin-remove-bottom"><b>Голомт банк</b></p>--}}
            {{--<p class="uk-margin-remove-bottom uk-margin-small-top uk-text-meta">Төлөх дүн:</p>--}}
            {{--<div class="uk-child-width-1-2" uk-grid>--}}
            {{--<div>--}}
            {{--<p class="sc-text-default uk-margin-remove-top uk-margin-remove-bottom">--}}
            {{--Төлөх дүн: <b id="val1">{{$olympiad->mandate_cost}}</b><b>₮</b>--}}
            {{--</p>--}}
            {{--</div>--}}
            {{--<div class="uk-text-right">--}}
            {{--<button type="button" id="1" v-on:click="copy($event)"--}}
            {{--class="sc-text-default uk-button uk-button-text">--}}
            {{--Хуулах--}}
            {{--</button>--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--<p class="uk-margin-remove-bottom uk-margin-small-top uk-text-meta">Дансны--}}
            {{--дугаар:</p>--}}
            {{--<div class="uk-child-width-1-2 uk-margin-remove-bottom uk-margin-small-top uk-flex uk-flex-middle"--}}
            {{--uk-grid>--}}
            {{--<div>--}}
            {{--<p class="sc-text-default uk-margin-remove-top uk-margin-remove-bottom">--}}
            {{--Дансны дугаар: <b id="val2">9999999999</b>--}}
            {{--</p>--}}
            {{--</div>--}}
            {{--<div class="uk-text-right">--}}
            {{--<button type="button" id="2" v-on:click="copy($event)"--}}
            {{--class="sc-text-default uk-button uk-button-text">--}}
            {{--Хуулах--}}
            {{--</button>--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--<p class="uk-margin-remove-bottom uk-margin-small-top uk-text-meta">Хүлээн--}}
            {{--авагч:</p>--}}
            {{--<div class="uk-child-width-1-2 uk-margin-remove-bottom uk-margin-small-top uk-flex uk-flex-middle"--}}
            {{--uk-grid>--}}
            {{--<div>--}}
            {{--<p class="sc-text-default uk-margin-remove-top uk-margin-remove-bottom">--}}
            {{--Хүлээн--}}
            {{--авагч: <b id="val3">Тек Авдар</b></p>--}}
            {{--</div>--}}
            {{--<div class="uk-text-right">--}}
            {{--<button type="button" id="3" v-on:click="copy($event)"--}}
            {{--class="sc-text-default uk-button uk-button-text">--}}
            {{--Хуулах--}}
            {{--</button>--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--<p class="uk-margin-remove-bottom uk-margin-small-top uk-text-meta">Гүйлгээний--}}
            {{--утга:</p>--}}
            {{--<div class="uk-child-width-1-2 uk-margin-remove-bottom uk-margin-small-top uk-flex uk-flex-middle"--}}
            {{--uk-grid>--}}
            {{--<div>--}}
            {{--<p class="sc-text-default uk-margin-remove-top uk-margin-remove-bottom">--}}
            {{--Гүйлгээний утга: <b id="val4">{{$mandate->register_number}}</b>--}}
            {{--</p>--}}
            {{--</div>--}}
            {{--<div class="uk-text-right">--}}
            {{--<button type="button" id="4" v-on:click="copy($event)"--}}
            {{--class="sc-text-default uk-button uk-button-text">--}}
            {{--Хуулах--}}
            {{--</button>--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--@endif--}}
            {{--</div>--}}
        </form>
    </div>
</div>