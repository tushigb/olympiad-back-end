<div v-if="currentContent" id="content-detail" class="sc-content-modal uk-modal-container" uk-modal>
    <div class="uk-modal-dialog uk-modal-body">
        <button class="uk-modal-close-default" type="button" uk-close></button>
        <article class="uk-article">
            <h1 class="uk-article-title">
                <p class="uk-link-reset">
                    @{{ currentContent.title }}
                </p>
            </h1>
            <hr>
            <div class="uk-margin-bottom">
                <i class="sc-color-grey sc-text-200 uk-text-meta">
                    <time datetime="2016-04-01T19:00">
                        @{{ 'Нийтэлсэн: ' + currentContent.date}}
                        <span v-if="currentContent.is_content">@{{ 'Нийтэлсэн: ' }}</span>
                    </time>
                </i>
            </div>
            <div v-if="currentContent.is_content" class="sc-content-modal-slider uk-margin-bottom">
                <div class="uk-position-relative uk-visible-toggle uk-light" uk-slider>
                    <ul class="uk-slider-items uk-grid">
                        <li v-for="(image, i) in currentContent.images" class="uk-width-4-5">
                            <div class="uk-panel">
                                <img :src="'/storage/' + image.path" alt="">
                                <div class="uk-position-center uk-text-center">
                                    <h2 uk-slider-parallax="x: 100,-100">@{{ i + 1 }}</h2>
                                </div>
                            </div>
                        </li>
                    </ul>
                    <a class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous
                       uk-slider-item="previous"></a>
                    <a class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next
                       uk-slider-item="next"></a>
                </div>
            </div>
            <div class="uk-child-width-1-1@s uk-inline">
                <article class="uk-article" v-html="currentContent.content">
                </article>
            </div>
        </article>
    </div>
</div>