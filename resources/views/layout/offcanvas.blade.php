<div class="uk-offcanvas-content">
    <div id="offcanvas-overlay" uk-offcanvas="overlay: true">
        <div class="uk-offcanvas-bar">
            <button class="uk-offcanvas-close" type="button" uk-close></button>
            <h3 class="sc-text-200">ЦЭС</h3>
            <div class="uk-text-center">
                <a class="sc-link-brand sc-text-200">Дэлгүүр</a><br>
                <hr>
                @if(auth()->guest())
                    <a class="sc-link-brand sc-text-200" href="/dist/signup">Бүртгүүлэх</a><br>
                    <hr>
                    <a class="sc-link-brand sc-text-200" href="#login-modal" uk-toggle>Нэвтрэх</a><br>
                @elseif(auth()->user())
                    <a class="sc-link-brand sc-text-200"
                       href="/participant/{{auth()->id()}}">{{auth()->user()->first_name}}</a><br>
                    <hr>
                    <a class="sc-link-brand sc-text-200" v-on:click="logout">Гарах</a>
                @endif
            </div>
        </div>
    </div>
</div>