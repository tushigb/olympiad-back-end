<div id="footerSection" class="footerSection">
    <div class="uk-container">
        <div class="uk-child-width-1-4@s" uk-grid>
            <div>
                <p class="sc-text-white-grey sc-text-400 uk-margin-remove-bottom">Цэс</p>
                <p class="sc-text-white-grey sc-text-200 sc-text-size-14 uk-margin-remove-bottom">Олимпиад</p>
                <p class="sc-text-white-grey sc-text-200 sc-text-size-14 uk-margin-remove-top uk-margin-remove-bottom">
                    Мэдээлэл</p>
                <p class="sc-text-white-grey sc-text-200 sc-text-size-14 uk-margin-remove-top uk-margin-remove-bottom">
                    Ярилцлага</p>
                <p class="sc-text-white-grey sc-text-200 sc-text-size-14 uk-margin-remove-top uk-margin-remove-bottom">
                    Чансаа</p>
            </div>
            <div>
                <p class="sc-text-white-grey sc-text-400 sc-text-size-14 uk-margin-remove-bottom">Холбоос</p>
                <p class="sc-text-white-grey sc-text-200 sc-text-size-14 uk-margin-remove-bottom">
                    <a class="sc-link-footer" href="http://techavdar.com/">TECHAVDAR</a>
                </p>
                <p class="sc-text-white-grey sc-text-200 sc-text-size-14 uk-margin-remove-top uk-margin-remove-bottom">
                    <a class="sc-link-footer" href="https://www.ebs.mn/">EBS</a>
                </p>
                <p class="sc-text-white-grey sc-text-200 sc-text-size-14 uk-margin-remove-top uk-margin-remove-bottom">
                    <a class="sc-link-footer" href="http://avdar.mn/">AVDAR</a>
                </p>
                <p class="sc-text-white-grey sc-text-200 sc-text-size-14 uk-margin-remove-top uk-margin-remove-bottom">
                    <a class="sc-link-footer" target="_blank" href="https://mecss.gov.mn/">БСШУЯ</a>
                </p>
            </div>
            <div>
                <p class="sc-text-white-grey sc-text-400 sc-text-size-14 uk-margin-remove-bottom">Сайтын тухай</p>
                <p class="sc-text-white-grey sc-text-200 sc-text-size-14 uk-margin-remove-bottom">
                    <a class="sc-link-footer" href="#modal-service" uk-toggle>Үйлчилгээ</a>
                </p>
                <p class="sc-text-white-grey sc-text-200 sc-text-size-14 uk-margin-remove-top uk-margin-remove-bottom">
                    <a class="sc-link-footer" href="#modal-terms-condition" uk-toggle>Ерөнхий нөхцөл</a>
                </p>
                <p class="sc-text-white-grey sc-text-200 sc-text-size-14 uk-margin-remove-top uk-margin-remove-bottom">
                    <a class="sc-link-footer" href="#modal-security-condition" uk-toggle>Мэдээлэллийн нууцлал</a>
                </p>
                <p class="sc-text-white-grey sc-text-200 sc-text-size-14 uk-margin-remove-top uk-margin-remove-bottom">
                    <a class="sc-link-footer" href="#modal-contact" uk-toggle>Холбоо барих</a>
                </p>
                <p class="sc-text-white-grey sc-text-200 sc-text-size-14 uk-margin-remove-top uk-margin-remove-bottom">
                    <a class="sc-link-footer" href="/about">Бидний тухай</a>
                </p>
            </div>
            <div>
                <p class="sc-text-white-grey sc-text-200 sc-text-size-14 uk-margin-remove-top">
                    Хүрээ хотхон
                    3-р хороо, Чингэлтэй дүүрэг
                    Улаанбаатар хот, Монгол улс. <br>Утас : 9955-6989 <br> Имэйл : contact@techavdar.com</p>

                <p class="sc-text-white-grey sc-text-200 sc-text-size-14 uk-margin-remove-top uk-margin-remove-bottom">
                    © Copyright TECHAVDAR - 2018<br>
                    Made with <span class="sc-text-size-13 sc-color-red icon ion-md-heart"></span> in Ulaanbaatar,
                    Mongolia.
                </p>
            </div>
        </div>
    </div>
</div>