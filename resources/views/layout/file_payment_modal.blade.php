<div id="file-payment-modal" class="uk-modal-container" uk-modal>
    <div class="uk-modal-dialog uk-modal-body">
        <div class="uk-modal-body">

            {{--<h2 v-if="currentFile" class="sc-text-brand sc-text-200 uk-modal-title">--}}
            {{--@{{ currentFile.name }}--}}
            {{--</h2>--}}
            {{--<div id="qpay_image">--}}

            {{--</div>--}}
            {{--<div style="display: block;" id="spinner" class="uk-text-center">--}}
            {{--<div style="color: #00AB8E" uk-spinner="ratio: 1.5"></div>--}}
            {{--</div>--}}
            {{--<p class="uk-text-right">--}}
            {{--<button class="uk-button uk-button-default uk-modal-close" type="button">Cancel</button>--}}
            {{--<button class="uk-button uk-button-primary" type="button">Save</button>--}}
            {{--</p>--}}

            <div class="uk-card uk-card-default uk-grid-collapse uk-child-width-1-2@s uk-margin"
                 uk-grid>
                <div class="uk-card-media-left">
                    <div class="uk-card-body">
                        <h3 class="sc-text-default uk-card-title">Q-Pay</h3>
                        <i class="sc-text-default ion-information-circled"></i>
                        <span class="sc-text-default sc-text-size-14 uk-text-meta uk-margin-right uk-text-left">
                            Та банкныхаа мобайл аппликейшнээр орж QR кодыг уншуулна уу.
                        </span>
                        <div id="qpay_image">

                        </div>
                        <div style="display: block;" id="spinner" class="uk-text-center">
                            <div style="color: #00AB8E" uk-spinner="ratio: 1.5"></div>
                        </div>
                    </div>
                </div>
                <div>
                    <div class="uk-card-body">
                        <h3 class="sc-text-default uk-card-title">Шилжүүлгээр төлөх</h3>
                        <i class="sc-text-default ion-information-circled"></i>
                        <span class="sc-text-default sc-text-size-14 uk-text-meta uk-margin-right uk-text-left uk-margin-small-bottom">
                                    Гүйлгээ хийхдээ регистрийн дугаарын эхний 2 үсгийг <b>КИРИЛЛЭЭР</b> бичихииг анхаарна уу
                                </span>
                        <div class="uk-margin-small-top">
                            <p class="sc-text-default uk-margin-remove-bottom uk-text-meta">Төлөлт хийх
                                банк: <b>Голомт
                                    банк</b>
                            </p>
                            {{--<p class="uk-margin-remove-top uk-margin-remove-bottom"><b>Голомт банк</b></p>--}}
                            {{--<p class="uk-margin-remove-bottom uk-margin-small-top uk-text-meta">Төлөх дүн:</p>--}}
                            <div class="uk-child-width-1-2" uk-grid>
                                <div>
                                    <p class="sc-text-default uk-margin-remove-top uk-margin-remove-bottom">
                                        Төлөх дүн: <b id="val1">5000</b><b>₮</b>
                                    </p>
                                </div>
                                <div class="uk-text-right">
                                    <button type="button" id="1" v-on:click="copy($event)"
                                            class="sc-text-default uk-button uk-button-text">
                                        Хуулах
                                    </button>
                                </div>
                            </div>
                            {{--<p class="uk-margin-remove-bottom uk-margin-small-top uk-text-meta">Дансны--}}
                            {{--дугаар:</p>--}}
                            <div class="uk-child-width-1-2 uk-margin-remove-bottom uk-margin-small-top uk-flex uk-flex-middle"
                                 uk-grid>
                                <div>
                                    <p class="sc-text-default uk-margin-remove-top uk-margin-remove-bottom">
                                        Дансны дугаар: <b id="val2">9999999999</b>
                                    </p>
                                </div>
                                <div class="uk-text-right">
                                    <button type="button" id="2" v-on:click="copy($event)"
                                            class="sc-text-default uk-button uk-button-text">
                                        Хуулах
                                    </button>
                                </div>
                            </div>
                            {{--<p class="uk-margin-remove-bottom uk-margin-small-top uk-text-meta">Хүлээн--}}
                            {{--авагч:</p>--}}
                            <div class="uk-child-width-1-2 uk-margin-remove-bottom uk-margin-small-top uk-flex uk-flex-middle"
                                 uk-grid>
                                <div>
                                    <p class="sc-text-default uk-margin-remove-top uk-margin-remove-bottom">
                                        Хүлээн
                                        авагч: <b id="val3">Тек Авдар</b></p>
                                </div>
                                <div class="uk-text-right">
                                    <button type="button" id="3" v-on:click="copy($event)"
                                            class="sc-text-default uk-button uk-button-text">
                                        Хуулах
                                    </button>
                                </div>
                            </div>
                            {{--<p class="uk-margin-remove-bottom uk-margin-small-top uk-text-meta">Гүйлгээний--}}
                            {{--утга:</p>--}}
                            <div class="uk-child-width-1-2 uk-margin-remove-bottom uk-margin-small-top uk-flex uk-flex-middle"
                                 uk-grid>
                                <div>
                                    <p class="sc-text-default uk-margin-remove-top uk-margin-remove-bottom">
                                        Гүйлгээний утга: <b id="val4">5000</b>
                                    </p>
                                </div>
                                <div class="uk-text-right">
                                    <button type="button" id="4" v-on:click="copy($event)"
                                            class="sc-text-default uk-button uk-button-text">
                                        Хуулах
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>